class QuestController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def index
    logger.debug '> index @ quest con'
    @code = Code.new
  end
  
  def code
    unless request.post? and params[ :code ] and params[ :code ][ :input ]
      redirect_to quest_path
    else
      @code = Code.find_by input: params[ :code ][ :input ].downcase
    end
  end
  
end
