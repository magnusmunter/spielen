class SpielerMailer < ApplicationMailer
  helper ApplicationHelper
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.spieler_mailer.dubistdran.subject
  #
  def dubistdran
    @email = params[ :email ]
    @name = params[ :name ]
    
    @spiel_link = "https://spielen.io/plan/#{ params[ :plan_id ] }##{ @name }"
    
    mail to: @email,
         subject: "[1835] Du bist dran!"
  end

  def issue
    mail to: "incoming+magnusmunter-spielen-15365968-enaktznb01ndmf34wzbnq91io-issue@incoming.gitlab.com",
         subject: params[:titel],
         body: params[:beschreibung]
  end
end
