module RewardsHelper
  
  def h_reward_editable?( in_reward )
    h_is_admin? or
        in_reward.funding.blank? ?
            h_is_sales? :
            ( h_is_editor? and in_reward.funding.not_started? )
  end
  
end
