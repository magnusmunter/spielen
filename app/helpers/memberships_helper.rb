module MembershipsHelper
  
  def h_fee_mode_class
    @membership.fee_mode != 'direct_debit' ? 'initially_hidden' : nil
  end
  
end
