class CreateStands < ActiveRecord::Migration[5.2]
  def change
    create_table :stands do |t|
      t.text :content

      t.timestamps
    end
  end
end
