class CreateZuege < ActiveRecord::Migration[5.2]
  def change
    if !table_exists? :zuege
      create_table :zuege do |t|
        t.integer :spiel_id
        t.integer :nr
        t.string :spieler, limit: 2
        t.text :aktionen

        t.timestamps
      end
    end

    Stand.all.each do |stand|
      stand = JSON stand.content
      zuege = stand["zuege"].to_json
      zuege.gsub!(/"Spielleiter"/, "-1")
      i = 0
      stand["spieler"].each do |n, v|
        zuege.gsub!("\"" + n + "\"", i.to_s)
        i = i + 1
      end
      JSON(zuege).each do |zug|
        Zug.create spiel_id: stand["id"], nr: zug["nr"], spieler: zug["spieler"], aktionen: zug["aktionen"].to_json
      end
    end
  end
end
