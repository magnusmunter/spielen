json.extract! message, :id, :sender_id, :sender_type, :receiver_id, :receiver_type, :content, :read_at, :created_at, :updated_at
json.url message_url(message, format: :json)
