require "application_system_test_case"

class StreamsTest < ApplicationSystemTestCase
  setup do
    @stream = streams(:one)
  end

  test "visiting the index" do
    visit streams_url
    assert_selector "h1", text: "Streams"
  end

  test "creating a Stream" do
    visit streams_url
    click_on "New Stream"

    fill_in "Fade away days", with: @stream.fade_away_days
    fill_in "Max participants", with: @stream.max_participants
    check "Only invites" if @stream.only_invites
    fill_in "Owner", with: @stream.owner_id
    check "Public" if @stream.public
    fill_in "Status", with: @stream.status
    fill_in "Status required", with: @stream.status_required
    fill_in "Title", with: @stream.title
    fill_in "Token", with: @stream.token
    click_on "Create Stream"

    assert_text "Stream was successfully created"
    click_on "Back"
  end

  test "updating a Stream" do
    visit streams_url
    click_on "Edit", match: :first

    fill_in "Fade away days", with: @stream.fade_away_days
    fill_in "Max participants", with: @stream.max_participants
    check "Only invites" if @stream.only_invites
    fill_in "Owner", with: @stream.owner_id
    check "Public" if @stream.public
    fill_in "Status", with: @stream.status
    fill_in "Status required", with: @stream.status_required
    fill_in "Title", with: @stream.title
    fill_in "Token", with: @stream.token
    click_on "Update Stream"

    assert_text "Stream was successfully updated"
    click_on "Back"
  end

  test "destroying a Stream" do
    visit streams_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Stream was successfully destroyed"
  end
end
