const VERSION = "v1"
const CACHE_NAME = "spiel1835"

console.log("ServiceWorker " + VERSION)

self.addEventListener("install", (event) => {
	console.log("ServiceWorker " + VERSION + " install",
	            event.activeWorker || "no active worker")
})

self.addEventListener("activate", (event) => {
	console.log("ServiceWorker " + VERSION + " activate", event)
	// clear old cache
})

self.addEventListener("message", (event) => {
	console.log("ServiceWorker " + VERSION + " message", event)
})

self.addEventListener("fetch", (event) => {
	const request = event.request
	const url = request.url
	// console.log("ServiceWorker " + VERSION + " fetch", url)
	if (url.indexOf("stand.json") >= 0) {
		// console.log(" get from net unless network error:", url)
		event.respondWith(fetch(request).then(response => {
			// console.log(" fetched:", url, response)
			return caches.open(CACHE_NAME).then(cache => {
				// console.log(" response.ok:", response.ok)
				if (response.ok)
					cache.put(request, response.clone())
				else
					response = cache.match(request)
				return response
			})
		}).catch(error => caches.match(request)))
	} else if (url.indexOf("stand") >= 0)
		;// console.log(" shall not be cached:", url)
	else {
		const match = /\/\d+/.exec(url)
		const cacheUrl = (match && (match.index + match[0].length === url.length))
		                 ? url.substr(0, match.index) : url
		// console.log(" check cache for:", cacheUrl)
		event.respondWith(caches.match(cacheUrl).then(response => {
			if (response)
				;//console.log(" cache matched:", cacheUrl)
			else {
				console.log(" fetch from net:", url)
				response = fetch(request).then(response => {
					if (response.ok)
						caches.open(CACHE_NAME).then(cache => {
							// console.log(" put to cache:", cacheUrl)
							cache.put(cacheUrl, response)
						})
					else
						throw new Error("HTTP " + response.status + " for "+ url)
					return response.clone()
				})
			}
			return response
		// }).catch(error => {
		// 	console.log(" failed with:", error)
		// 	return new Response
		}))
	}
})

console.log(" check for update")
fetch("update.json").then(response => {
	if (response.ok)
		response.clone().json().then(fetchedUpdate => {
			// console.log("fetched update.json", response.url, fetchedUpdate)
			caches.open(CACHE_NAME).then(cache => {
				cache.match(response.url).then(cacheResponse => cacheResponse && cacheResponse.json()).then(cachedUpdate => {
					// console.log("cached update.json", cachedUpdate)
					if (!cachedUpdate || (fetchedUpdate.version != cachedUpdate.version)) {
						console.log("update")
						const path = response.url.substr(0, response.url.indexOf("/", response.url.search(/\/\//) + 2) + 1)
						fetchedUpdate.changed.forEach(file => cache.delete(path + file))
						fetchedUpdate.moved.forEach(files =>
							cache.match(path + files[0])
								.then(response => cache.delete(path + files[0])
								.then(cache.put(path + files[1], response))))
						fetchedUpdate.removed.forEach(file => cache.delete(path + file))
						cache.put(response.url, response)
					} else
						console.log("version is:", fetchedUpdate.version)
				})
			})
		}).catch(error => console.log(error))
	else
		throw new Error("HTTP " + response.status + " for "+ response)
})