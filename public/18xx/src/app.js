import { createApp } from "../../lib/vendor/vue.esm-browser.js"
import { createPinia } from "../../lib/vendor/pinia.esm-browser.js"
import "../../lib/extendJSON.js"
import { DragDirective } from "../../lib/vue_drag_directive.js"
import { importVue } from "../../lib/importVue.js"

const Spiel = await importVue("../components/Spiel.vue", import.meta.url)
const AgMarker = await importVue("../../1835/components/AgMarker.vue")
const Karte = await importVue("../../1835/components/Karte.vue")
const Aktie = await importVue("../../1835/components/Aktie.vue")
const Auswahl = await importVue("../../1835/components/Auswahl.vue")

console.log( "app > init" )

createApp(Spiel)
.use(createPinia())
.directive("drag", DragDirective)
.component("AgMarker", AgMarker)
.component("Karte", Karte)
.component("Aktie", Aktie)
.component("Auswahl", Auswahl)
.mount("#app")