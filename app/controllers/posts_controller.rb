class PostsController < ApplicationController
  skip_before_action :require_team, only: %i[ create ]
  before_action :set_post, only: %i[ show edit update destroy ]
  
  # GET /posts or /posts.json
  def index
    @posts = Post.all
  end
  
  # GET /posts/1 or /posts/1.json
  def show
  end
  
  # GET /posts/new
  def new
    @post = Post.new
  end
  
  # GET /posts/1/edit
  def edit
  end
  
  # POST /posts or /posts.json
  def create
    redirect_to( login_path, alert: 'Aktion nicht erlaubt' ) and return if params[ :stream_id ].blank? or params[ :token ].blank? or params[ :text ].blank?
    
    # hole zugehörigen Stream mit derselben
    # Nummer, wie das Spiel
    @stream = Stream.find params[ :stream_id ]
    redirect_to( root_path, alert: 'Nicht erlaubt' ) and return unless @stream
    
    # hole Spieler Contact via dem Token
    @contact = Contact.where( token: params[ :token ] ).first
    redirect_to( root_path, alert: 'Nicht erlaubt' ) and return unless @contact
    
    # setze neuen Post auf mit Spieler
    @post = @stream.posts.build status: 'new',
        contact: @contact,
        contentpart: Contentpart.new( 
            kind: 'p',
            position: 0,
            content: params[ :text ] )
    
    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: "Post was successfully created." }
        format.json { render :show, status: :created, location: @post }
        format.js { render status: :ok }
        
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
        format.js { render status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /posts/1 or /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: "Post was successfully updated." }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /posts/1 or /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: "Post was successfully destroyed." }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find(params[:id])
  end
  
  # Only allow a list of trusted parameters through.
  def post_params
    params.require(:post).permit(:token, :status, :contact_id, :stream_id)
  end
  
end
