class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :token
      t.string :status
      t.references :contact, foreign_key: true
      t.references :stream, foreign_key: true
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
