class StandsController < ApplicationController
  skip_before_action :require_team, only: %i( show index update delete )
  before_action :require_admin, only: %i( index delete )
  
  layout 'admin_vue32'
  
  def show
    @stand = Stand.find_or_create_by id: params[ :id ].to_i
    @stand.content ||= '{}'
    
    @stream = Stream.where( id: @stand.id ).first
    zuege = @stand.zuege.ab params[ :nr ]
    @posts = @stream ? @stream.posts : []
    @zuege_hashes = zuege.collect { |z|
          { wer: z.spieler, was: JSON( z.aktionen ), wann: z.updated_at } }
    
    
    respond_to do |format|
      format.html do
        redirect_to login_path and return unless current_account and current_account.is_admin?
        stand_text = gibMitZuege( @stand.content, params[:id], params[:nr] )
        @text = stringify(JSON(stand_text), "\t", {"bank" => {"*" => {"-" => 1}}, "dran" => {"-" => 1}, "*" => {"*" => {"-" => 1, "was" => {"*" => {"-" => 1, "fährt" => {"*" => {"-" => 1}}, "*" => {"-" => 1}}}, "fährt" => {"*" => {"-" => 1}}, "*" => {"-" => 1}}}})
      end
      
      format.json
    end
  end
  
  def index
    @spiele = Stand.all
  end
  
  def update
    @stand = Stand.find params[ :id ]
    redirect_to( login_path, alert: 'Kein Spielstand vorhanden!' ) and return unless @stand
    
    if params[:stand].is_a? String 
      # von Stand editieren aktualisieren
      @stand.update content: params[:stand]
      
      if params[:zuege]
        JSON("[" + params[:zuege] + "]").each do |zug|
          if zug.is_a? Integer
            if (zug >= 0)
              Zug.find(zug).destroy
            else
              Zug.where(spiel_id: params[:id]).destroy_all
            end
          else
            zugEintrag = Zug.find_by(spiel_id: params[:id], id: zug["id"])
            if zugEintrag.nil?
              Zug.create spiel_id: params[:id], nr: zug["nr"], spieler: zug["wer"], aktionen: zug["was"].to_json, dann: zug["dann"]
            else
              zugEintrag.update nr: zug["nr"], spieler: zug["wer"], aktionen: zug["was"].to_json, dann: zug["dann"]
            end
          end
        end
      end
      redirect_to @stand
      
    elsif params[ :titel ]
      # prüfen ob issue gesendet werden soll
      puts "Sende issue an GitLab: " + params[:titel] + " : " + params[:beschreibung]
      SpielerMailer.with( titel: params[ :titel ],
          beschreibung: params[ :beschreibung ] + "\n\n" + params[:stand].to_json ).
          issue.
          deliver_later
      
    else
      # gesendeten neuen Stand speichern
      letzterZug = @stand.zuege.last
      if letzterZug.nil? || (letzterZug.dann == params[ :stand ][ :zuege ].first[ :wer ].to_s)
        aktionen = letzterZug ? JSON(letzterZug.aktionen) : nil
        if aktionen.is_a?(Hash) && aktionen["extra"] # letzter Zug war Extrazug
          zug = params[ :stand ][ :zuege ][0]
          letzterZug.update spieler: zug[ :wer ],
            aktionen: zug[ :was ].to_json,
            dann: zug[ :dann ]
          head :ok
        else
          if aktionen
            id = aktionen.last.keys[0]
            if id == id.to_i.to_s # letzte Aktion war Zugrücknahme
              aktionen.pop
              letzterZug.update aktionen: aktionen.to_json
            end
          end
          i = params[ :nr ].to_i
          params[ :stand ][ :zuege ].each do |zug|
            @stand.zuege.create nr: i,
                spieler: zug[ :wer ],
                aktionen: zug[ :was ].to_json,
                dann: zug[ :dann ]
            i += 1
          end
          params[ :stand ].delete "zuege"
          
          if @stand.update content: params[ :stand ].to_json
            # if params[ :to ] # prüfen ob Benachrichtigung gesendet werden soll
            #   puts "Sende Benachrichtigung an: " + params[:name] + " (" + params[:to] + ") für Spiel " + params[:id]
            #   SpielerMailer.with( email: params[ :to ],
            #       name: params[ :name ],
            #       plan_id: params[ :id ] ).
            #       dubistdran.
            #       deliver_later
            # end
            head :ok
          
          else
            head :unprocessable_entity
          end
        end
      else
        head :unprocessable_entity
      end
    end
  end
  
  def destroy
    puts "destroy " + params[:id]
    Stand.find(params[:id]).destroy
    Zug.where(spiel_id: params[:id]).destroy_all
    redirect_to action: :index
  end
  
private
  
  def gibMitZuege( text, id, nr )
    zuege = ""
    Zug.where("spiel_id = ? AND nr > ?", id, nr || 0).each do |zug|
      zuege += "," unless zuege.empty?
      spieler = zug.spieler.to_i.to_s
      spieler = spieler == zug.spieler ? spieler : "\"" + (zug.spieler.nil? ? "null" : zug.spieler) + "\""
      dann = zug.dann.to_i.to_s
      dann = dann == zug.dann ? dann : "\"" + (zug.dann.nil? ? "null" : zug.dann) + "\""
      zuege += "{\"id\": " + zug.id.to_s + ", " + "\"nr\": " + zug.nr.to_s + ", "
      zuege += "\"wer\":" + spieler + ",\"was\":" + zug.aktionen + ",\"dann\":" + dann + "}"
    end
    text.insert(-2, (text[-2] == "{" ? "" : ",") + "\"zuege\":[" + zuege + "]")
  end
  
  def stringify(object, einzug = "\t", option = nil)
    if object.is_a? String
      text = "\"" + object + "\""
    elsif object.is_a? Hash
      if option.is_a?(Hash) && option["-"]
        text = "{ "
        object.each do |key, value|
          text += ", " if text.length > 2
          text += "\"" + key + "\": "
          text += stringify(value, einzug, option.is_a?(Hash) ? option.has_key?(key) ? option[key] : option["*"] : nil)
        end
        text += " }"
      else
        text = "{\n"
        object.each do |key, value|
          text += ",\n" if text.length > 2
          text += einzug + "\"" + key + "\": "
          text += stringify(value, einzug + "\t", option.is_a?(Hash) ? option.has_key?(key) ? option[key] : option["*"] : nil)
        end
        text += "\n" + einzug[0...-1] + "}"
      end
    elsif object.is_a? Array
      if (option.is_a?(Hash) && option["-"]) || (object.length == 0)
        text = "[ "
        object.each_index do |i|
          text += ", " if text.length > 2
          text += stringify(object[i], einzug, option.is_a?(Hash) ? option.has_key?(i) ? option[i] : option["*"] : nil)
        end
        text += " ]"
      else
        text = "[\n"
        object.each_index do |i|
          text += ",\n" if text.length > 2
          text += einzug + stringify(object[i], einzug + "\t",
                                     option.is_a?(Hash) ? option.has_key?(i) ? option[i] : option["*"] : nil)
        end
        text += "\n" + einzug[0...-1] + "]"
      end
    else
      text = object.to_s
    end
  end

end
