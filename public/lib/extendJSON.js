function createRequest(method, url, onResponded) {
	const request = new XMLHttpRequest()
	request.onreadystatechange = function() {
		if (onResponded && (this.readyState === XMLHttpRequest.DONE))
			onResponded(this.status === 200 ? JSON.parse(this.responseText || "null") : { status: this.status })
	}
	request.open(method, url)
	request.setRequestHeader("Content-Type", "application/json")
	request.overrideMimeType("text/plain")
	return request
}
 
JSON.load = function(url, onLoaded) {
	createRequest("GET", url, onLoaded).send()
}
 
JSON.post = function(sendObject, url, onResponded) {
	createRequest("POST", url, onResponded).send(JSON.stringify(sendObject))
}
 
JSON.send = function(sendObject, url, onResponded) {
	createRequest("PATCH", url, onResponded).send(JSON.stringify(sendObject))
}