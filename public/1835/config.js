export const SCHLESIEN = document.location.search.indexOf("schlesien") > 0

export const BAHNEN = {
	p1: "Nürnberg-Fürther Bahn",
	p2: "Leipzig-Dresdner Bahn",
	p3: "Braunschweigische Bahn",
	p4: "Hannoversche Bahn",
	p5: "Ostbayrische Bahn",
	p6: "Pfalzbahnen",
	v1: "Bergisch-Märkische Bahn",
	v2: "Berlin-Potsdamer Bahn",
	v3: "Magdeburger Bahn",
	v4: SCHLESIEN ? "Oberschlesische Bahn" : "Köln-Mindener Bahn",
	v5: "Berlin-Stettiner Bahn",
	v6: "Hamburg-Altonaer Bahn",
	PR: "Preußische Eisenbahn",
	BY: "Bayrische Eisenbahn",
	SX: "Sächsische Eisenbahn",
	BA: "Badische Eisenbahn",
	WT: "Württembergische Eisenbahn",
	HE: "Hessische Eisenbahn",
	MS: "Eisenbahn Mecklenburg-Schwerin",
	OL: "Oldenburger Eisenbahn"
}

export const LOKS = {
	"2": { zahl: SCHLESIEN ? 11 : 9, geld: 80 },
	"2+": { zahl: 4, geld: 120 },
	"3": { zahl: 4, geld: 180 },
	"3+": { zahl: 3, geld: 270 },
	"4": { zahl: 3, geld: 360 },
	"4+": { zahl: 1, geld: 440 },
	"5": { zahl: 2, geld: 500 },
	"5+": { zahl: 1, geld: 600 },
	"6": { zahl: 2, geld: 600 },
	"6+": { zahl: 4, geld: 720 }
}

export const FERNVERBINDUNGEN = Object.assign({
	"2,12": "Elsaß/Lothringen",
	"1,13": "Elsaß/Lothringen"
}, SCHLESIEN ? {
	"12,2": "Ostpreußen",
	"14,8": "Kattowitz",
} : {
	"10,2": "Ostpreußen",
	"10,7": "Oberschlesien",
})

// Reihen der baubaren Gleise für Feldtypen
export const GLEIS_REIHEN = [
	[[7, 1], [8, 2], [9, 0], [8, 0], [7, 0]], // normales Feld
	[[3, 0], [58, 0], [4, 0], [58, 4], [3, 5]], // Feld mit Eizelhalt
	[[2, 1], [56, 2], [69, 1], [56, 1], [1, 2], [69, 3], [2, 3], [55, 0],
	 [69, 0], [55, 5], [2, 0], [69, 3], [1, 0], [56, 0], [69, 5], [56, 5], [2, 2]], // Feld mit Doppelhalt
	[[5, 0], [6, 2], [57, 0], [6, 0], [5, 5]], // Feld mit Stadt
	[[201, 0], [202, 2], [202, 0], [201, 5]], // Feld mit Großstadt
	[[212, 1], [211, 0], [213, 4], [210, 2], [214, 3], [211, 4], [214, 0], [215, 5], [213, 2], [210, 0], // Feld mit Doppelstadt
	 [210, 3], [214, 4], [215, 0], [213, 0], [211, 5], [213, 3], [210, 1], [214, 2], [211, 3], [212, 0]]
]

// Vorlage für alle Gleisarten
export const GLEIS_VORLAGE = {
	// gelb
	1: { bild: 0, x: 83, y: 0, zahl: 1, strecken: [[0, 4], [1, 3]], reihe: [[88, 1]], streckenBhf: true, geld: true, gx: -21, gy: 36 }, // Doppelhalt
	2: { bild: 0, x: 1, y: 48, zahl: 1, strecken: [[0, 3], [1, 2]], reihe: [[87, 3]], streckenBhf: true, geld: true, gx: -21, gy: 36 }, // Doppelhalt
	3: { bild: 0, x: 81, y: 96, zahl: 2, strecken: [[3, 4]], // Kurve mit Halt
	     reihe: [[204, 4], [87, 5], [88, 1], [204, 3], [87, 0], [87, 4], [204, 4], [88, 1], [87, 5], [204, 3], [87, 4], [87, 0]], geld: true,
	     gx: -30, gy: -15 },
	4: { bild: 0, x: 165, y: 143, zahl: 3, strecken: [[0, 3]], // Gerade mit Halt
	     reihe: [[87, 3], [88, 4], [204, 0], [204, 3], [88, 0], [87, 3], [87, 0], [88, 4], [204, 3], [204, 0], [88, 0], [87, 0]], geld: true,
	     gx: -21, gy: 36 },
	5: { bild: 0, x: 249, y: 189, zahl: 3, strecken: [[3, 4]], // Stadt spitz
	     reihe: [[12, 1], [205, 0], [14, 0], [15, 4], [15, 5], [15, 0], [14, 0], [206, 1], [12, 0]], bhfe: [[0, 0]], geld: 20, gx: -21, gy: 36 },
	6: { bild: 0, x: 331, y: 140, zahl: 3, strecken: [[1, 3]], // Stadt stumpf
	     reihe: [[12, 5], [15, 4], [15, 3], [206, 1], [14, 0], [15, 4], [13, 0], [205, 3], [15, 3], [14, 0]], bhfe: [[0, 0]], geld: 20, gx: 22, gy: -26 },
	7: { bild: 0, x: 247, y: 96, zahl: 8, strecken: [[2, 3]], reihe: [[29, 5], [27, 5], [26, 0], [28, 0], [18, 4]] }, // Kurve
	8: { bild: 0, x: 249, y: 0, zahl: 16, strecken: [[1, 3]], // Bogen
	     reihe: [[29, 4], [24, 4], [25, 4], [28, 0], [16, 0], [19, 5], [16, 5], [29, 4], [25, 0], [23, 0],
	             [28, 0], [24, 4], [16, 0], [25, 4], [19, 5], [25, 0], [16, 5], [23, 0]] },
	9: { bild: 0, x: 330, y: 45, zahl: 12, strecken: [[0, 3]], // Gerade
	     reihe: [[23, 3], [26, 3], [18, 3], [23, 0], [20, 0], [19, 3], [27, 3], [26, 0], [19, 0], [20, 5], [24, 3], [18, 3], [27, 0], [24, 0],
	             [23, 0], [26, 0], [18, 0], [23, 3], [20, 0], [19, 0], [27, 0], [26, 3], [19, 3], [20, 5], [24, 0], [18, 0], [27, 3], [24, 3]] },
	55: { bild: 0, x: 0, y: 143, zahl: 1, strecken: [[0, 3], [1, 4]], reihe: [[88, 1]], streckenBhf: true, geld: true, gx: -21, gy: 36 }, // Doppelhalt
	56: { bild: 0, x: 2, y: 237, zahl: 1, strecken: [[1, 3], [2, 4]], reihe: [[87, 4]], streckenBhf: true, geld: true, gx: -21, gy: 36 }, // Doppelhalt
	57: { bild: 0, x: 330, y: 236, zahl: 2, strecken: [[0, 3]], // Stadt gerade
	      reihe: [[205, 0], [15, 0], [14, 0], [206, 3], [14, 5], [15, 0], [205, 3], [15, 3], [14, 0], [206, 0], [14, 5], [15, 3]],
	      bhfe: [[0, 0]], geld: 20, gx: -14, gy: -35 },
	58: { bild: 0, x: 164, y: 47, zahl: 4, strecken: [[3, 5]], geld: true, gx: -21, gy: 36, // Bogen mit Halt
	      reihe: [[87, 0], [204, 2], [203, 0], [204, 4], [204, 0], [87, 5], [88, 0], [204, 2], [87, 0], [204, 4], [87, 5], [204, 0], [88, 0]] },
	69: { bild: 0, x: 85, y: 286, zahl: 2, strecken: [[0, 3], [2, 4]], reihe: [[204, 3]], streckenBhf: true, geld: true, gx: 38, gy: 5 }, // Doppelhalt
	201: { bild: 0, x: 165, y: 237, zahl: 2, strecken: [[3, 4]], // Großstadt spitz
		    reihe: [[208, 0], [207, 4], [207, 5], [207, 0]], bhfe: [[0, 0]], geld: 30, gx: -13, gy: 35 },
	202: { bild: 0, x: 83, y: 191, zahl: 2, strecken: [[1, 3]], reihe: [[207, 3], [208, 0], [207, 4]], bhfe: [[0, 0]], geld: 30,
		    gx: 1, gy: -32 }, // Großstadt stumpf
	// grün
	12: { bild: 1, x: 0, y: 192, zahl: 2, strecken: [[2, 3], [2, 4], [3, 4]], reihe: [[63, 0]], bhfe: [[0, 0]], geld: 30, gx: 18, gy: -34 },
	13: { bild: 1, x: 83, y: 144, zahl: 2, strecken: [[1, 3], [1, 5], [3, 5]], reihe: [[63, 0]], bhfe: [[0, 0]], geld: 30, gx: -22, gy: -34 },
	14: { bild: 1, x: 249, y: 240, zahl: 2, strecken: [[0, 1], [0, 3], [0, 4], [1, 3], [1, 4], [3, 4]], bhfe: [[2, 18], [8, 18]], geld: 30,
		   gx: 11, gy: -37 },
	15: { bild: 1, x: 332, y: 192, zahl: 2, strecken: [[0, 3], [0, 4], [0, 5], [3, 4], [3, 5], [4, 5]], bhfe: [[1, 18], [7, 18]], geld: 30,
		   gx: 12, gy: -39 },
	16: { bild: 1, x: 332, y: 0, zahl: 2, strecken: [[1, 3], [2, 4]], reihe: [[70, 4], [43, 4]] },
	18: { bild: 1, x: 415, y: 144, zahl: 1, strecken: [[0, 3], [4, 5]], reihe: [[43, 0]] },
	19: { bild: 1, x: 332, y: 96, zahl: 2, strecken: [[0, 3], [2, 4]], reihe: [[46, 3], [45, 3]] },
	20: { bild: 1, x: 415, y: 48, zahl: 2, strecken: [[0, 3], [1, 4]], reihe: [[47, 1], [44, 0]] },
	23: { bild: 1, x: 0, y: 0, zahl: 3, strecken: [[0, 3], [1, 3]], reihe: [[47, 1], [45, 0], [41, 0], [43, 3]] },
	24: { bild: 1, x: 83, y: 48, zahl: 3, strecken: [[0, 3], [3, 5]], reihe: [[47, 0], [43, 0], [42, 0], [46, 0]] },
	25: { bild: 1, x: 0, y: 96, zahl: 3, strecken: [[1, 3], [3, 5]], reihe: [[46, 4], [40, 0], [45, 2]] },
	26: { bild: 1, x: 166, y: 0, zahl: 2, strecken: [[0, 3], [2, 3]], reihe: [[44, 2], [42, 3], [45, 3]] },
	27: { bild: 1, x: 166, y: 96, zahl: 2, strecken: [[0, 3], [3, 4]], reihe: [[46, 3], [41, 3], [44, 0]] },
	28: { bild: 1, x: 249, y: 48, zahl: 2, strecken: [[1, 3], [2, 3]], reihe: [[46, 2], [70, 3], [39, 5], [43, 4]] },
	29: { bild: 1, x: 249, y: 144, zahl: 2, strecken: [[3, 4], [3, 5]], reihe: [[45, 4], [43, 5], [39, 1], [70, 0]] },
	87: { bild: 1, x: 498, y: 0, zahl: 2, strecken: [[0, 3], [0, 4], [0, 5], [3, 4], [3, 5], [4, 5]], geld: true, gx: -21, gy: 36 },
	88: { bild: 1, x: 498, y: 96, zahl: 2, strecken: [[0, 2], [0, 3], [0, 5], [2, 3], [2, 5], [3, 5]], geld: true, gx: -21, gy: 36 },
	203: { bild: 1, x: 498, y: 192, zahl: 2, strecken: [[1, 3], [1, 5], [3, 5]], geld: true, gx: -21, gy: 36 },
	204: { bild: 1, x: 415, y: 240, zahl: 2, strecken: [[0, 1], [0, 3], [0, 5], [1, 3], [1, 5], [3, 5]], geld: true, gx: -21, gy: 36 },
	205: { bild: 1, x: 83, y: 240, zahl: 1, strecken: [[0, 3], [0, 4], [3, 4]], reihe: [[63, 0]], bhfe: [[0, 0]], geld: 30, gx: -18, gy: -33 },
	206: { bild: 1, x: 166, y: 192, zahl: 1, strecken: [[0, 2], [0, 3], [2, 3]], reihe: [[63, 0]], bhfe: [[0, 0]], geld: 30, gx: 12, gy: -37 },
	207: { bild: 1, x: 0, y: 288, zahl: 2, strecken: [[0, 3], [0, 4], [0, 5], [3, 4], [3, 5], [4, 5]],
	       reihe: [[216, 3], [216, 2]], bhfe: [[10, 18], [4, 18]], geld: 40, gx: 16, gy: -39 },
	208: { bild: 1, x: 83, y: 336, zahl: 2, strecken: [[0, 1], [0, 3], [0, 4], [1, 3], [1, 4], [3, 4]],
	       reihe: [[216, 3], [216, 0]], bhfe: [[0, 18], [6, 18]], geld: 40, gx: 14, gy: -40 },
	209: { bild: 1, x: 332, y: 384, zahl: 1,
	       strecken: [[0, 1], [0, 2], [0, 3], [0, 5], [1, 2], [1, 3], [1, 5], [2, 3], [2, 5], [3, 5]],
	       reihe: [[220, 0]], bhfe: [[0, 20], [4, 20], [8, 20]], geld: 40, gx: -32, gy: 18 },
	210: { bild: 1, x: 166, y: 288, zahl: 1, strecken: [[0, 3], [1, 2]], reihe: [[217, 0]], bhfe: [[0, 0], [2, 30]], streckenBhf: true, geld: 30,
		    gx: -22, gy: -30 },
	211: { bild: 1, x: 249, y: 336, zahl: 1, strecken: [[0, 5], [3, 4]], reihe: [[217, 3]], bhfe: [[10, 20], [4, 20]], streckenBhf: true, geld: 30,
		    gx: 25, gy: -25 },
	212: { bild: 1, x: 332, y: 288, zahl: 1, strecken: [[0, 5], [2, 3]], reihe: [[218, 2]], bhfe: [[10, 20], [4, 20]], streckenBhf: true, geld: 30,
		    gx: -25, gy: 24 },
	213: { bild: 1, x: 415, y: 336, zahl: 1, strecken: [[0, 5], [1, 3]], reihe: [[219, 3]], bhfe: [[10, 20], [4, 20]], streckenBhf: true, geld: 30,
		    gx: -23, gy: 29 },
	214: { bild: 1, x: 498, y: 288, zahl: 1, strecken: [[0, 1], [3, 5]], reihe: [[219, 3]], bhfe: [[10, 20], [4, 20]], streckenBhf: true, geld: 30,
		    gx: 26, gy: 21 },
	215: { bild: 1, x: 498, y: 384, zahl: 1, strecken: [[0, 4], [1, 3]], reihe: [[218, 0]], bhfe: [[10, 20], [4, 20]], streckenBhf: true, geld: 30,
		    gx: 23, gy: 22 },
	// braun
	39: { bild: 2, x: 83, y: 48, zahl: 1, strecken: [[2, 3], [2, 4], [3, 4]] },
	40: { bild: 2, x: 83, y: 144, zahl: 1, strecken: [[1, 3], [1, 5], [3, 5]] },
	41: { bild: 2, x: 0, y: 0, zahl: 2, strecken: [[0, 1], [0, 3], [1, 3]] },
	42: { bild: 2, x: 0, y: 96, zahl: 2, strecken: [[0, 3], [0, 5], [3, 5]] },
	43: { bild: 2, x: 332, y: 0, zahl: 1, strecken: [[0, 3], [0, 4], [3, 5], [4, 5]] },
	44: { bild: 2, x: 166, y: 0, zahl: 2, strecken: [[0, 1], [0, 3], [1, 4], [3, 4]] },
	45: { bild: 2, x: 166, y: 96, zahl: 2, strecken: [[0, 3], [0, 5], [1, 3], [1, 5]] },
	46: { bild: 2, x: 249, y: 48, zahl: 2, strecken: [[0, 1], [0, 3], [1, 5], [3, 5]] },
	47: { bild: 2, x: 249, y: 144, zahl: 2, strecken: [[0, 2], [0, 3], [2, 5], [3, 5]] },
	63: { bild: 2, x: 0, y: 192, zahl: 3,
	      strecken: [[0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [1, 2], [1, 3], [1, 4], [1, 5],
	                 [2, 3], [2, 4], [2, 5], [3, 4], [3, 5], [4, 5]], bhfe: [[0, 18], [6, 18]], geld: 40, gx: 19, gy: -34 },
	70: { bild: 2, x: 332, y: 96, zahl: 1, strecken: [[0, 4], [0, 5], [3, 4], [3, 5]] },
	216: { bild: 2, x: 83, y: 240, zahl: 4,
	       strecken: [[0, 1], [0, 2], [0, 3], [0, 4], [1, 2], [1, 3], [1, 4], [2, 3], [2, 4], [3, 4]],
	       bhfe: [[0, 18], [6, 18]], geld: 50, gx: 15, gy: -39 },
	217: { bild: 2, x: 166, y: 192, zahl: 2,
	       strecken: [[0, 1], [0, 2], [0, 3], [1, 2], [1, 3], [2, 3]], bhfe: [[10, 18], [4, 18]], geld: 40, gx: 18, gy: -34 },
	218: { bild: 2, x: 249, y: 240, zahl: 2,
	       strecken: [[0, 1], [0, 3], [0, 4], [1, 3], [1, 4], [3, 4]], bhfe: [[0, 18], [6, 18]], geld: 40, gx: 18, gy: -34 },
	219: { bild: 2, x: 332, y: 192, zahl: 2,
	       strecken: [[0, 2], [0, 3], [0, 4], [2, 3], [2, 4], [3, 4]], bhfe: [[10, 18], [4, 18]], geld: 40, gx: 16, gy: -38 },
	220: { bild: 2, x: 415, y: 144, zahl: 1,
	       strecken: [[0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [1, 2], [1, 3], [1, 4], [1, 5],
	                  [2, 3], [2, 4], [2, 5], [3, 4], [3, 5], [4, 5]], bhfe: [[0, 20], [4, 20], [8, 20]], geld: 60, gx: 20, gy: -40 },
	221: { bild: 2, x: 415, y: 240, zahl: 1,
	       strecken: [[0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [1, 2], [1, 3], [1, 4], [1, 5],
	                  [2, 3], [2, 4], [2, 5], [3, 4], [3, 5], [4, 5]], bhfe: [[2.5, 23], [5.5, 23], [10, 23]], geld: 60, gx: -37, gy: -1 }
}

// Vorlage für alle Bahnhöfe
export const BHF_VORLAGE = {
	v1: { x: 402, y: 331, zahl: 1, start: [0, 7, 0] },
	v2: { x: 363, y: 331, zahl: 1, start: [9, 4, 0] },
	v3: { x: 382, y: 367, zahl: 1, start: [6, 5, 0] },
	v4: { x: 344, y: 366, zahl: 1, start: SCHLESIEN ? [12, 7, 0] : [2, 6, 0] },
	v5: { x: 400, y: 400, zahl: 1, start: SCHLESIEN ? [10, 2, 0] : [9, 4, 1] },
	v6: { x: 363, y: 400, zahl: 1, start: [5, 2, 0] },
	PR: { x: 325, y: 333, zahl: 7, start: [9, 4] },
	BY: { x: 305, y: 367, zahl: 5, start: [7, 14, 0] },
	SX: { x: 324, y: 401, zahl: 3, start: [7, 7, 0] },
	BA: { x: 285, y: 333, zahl: 2, start: [2, 11] },
	WT: { x: 266, y: 369, zahl: 2, start: [4, 12, 0] },
	HE: { x: 287, y: 400, zahl: 2, start: [3, 9, 0] },
	MS: { x: 246, y: 335, zahl: 2, start: [6, 2, 0] },
	OL: { x: 250, y: 402, zahl: 2, start: [2, 3, 0] }
}

// Anzahl der Felder des Spielplans in der Breite
const BREIT = 11
const feldData = []

/**
 * @returns Die Startdaten aller Spielfelder
 */
export function gibFeldData() {
	const breiter = SCHLESIEN ? 4 : 0
	for (let j = 1, anzahl = SCHLESIEN ? 141 : 126; j <= anzahl; ++j) {
		let i = j + 3
		if (i > 5)
			i += 2
		if (i > 8)
			i += 5 + breiter // Zeile 2
		if (i > 19 + breiter)
			i += 4 + breiter // Zeile 3
		if (i > 32 + (SCHLESIEN ? 2 * breiter + 2 : 0))
			i += 1 + (SCHLESIEN ? breiter - 2 : 0) // Zeile 4
		if (i > 42 + (SCHLESIEN ? 3 * breiter + 2 : 0))
			i += 3 + (SCHLESIEN ? breiter - 2 : 0) // Zeile 5
		if (i > 54 + (SCHLESIEN ? 4 * breiter + 1 : 0))
			i += 1 + (SCHLESIEN ? breiter - 1 : 0) // Zeile 6
		if (i > 64 + (SCHLESIEN ? 5 * breiter + 2 : 0))
			i += 1 + (SCHLESIEN ? breiter - 2 : 0) // Zeile 7
		if (SCHLESIEN) {
			if (i > 76 + 6 * breiter + 3)
				i += breiter - 3 // Zeile 8
			if (i > 87 + 7 * breiter + 3)
				i += breiter - 3 // Zeile 9
			if (i > 97 + 8 * breiter)
				i += 2
			if (i > 97 + 8 * breiter + 3)
				i += 1
		}
		if (i > 97 + (SCHLESIEN ? 8 * breiter + 5 : 0))
			i += 1 + (SCHLESIEN ? breiter - 5 : 0) // Zeile 10
		if (i > 106 + 9 * breiter)
			i += 4 + breiter // Zeile 11
		if (i > 117 + 10 * breiter)
			i += 3 + breiter // Zeile 12
		if (i > 128 + 11 * breiter)
			i += 5 + breiter // Zeile 13
		if (i > 141 + 12 * breiter)
			i += 2 + breiter // Zeile 14
		if (i > 151 + 13 * breiter)
			i += 4 + breiter // Zeile 15
		if (i > 162 + 14 * breiter)
			i += 4 + breiter // Zeile 16
		if (i > 167 + 15 * breiter)
			i += 1
		if (i > 169 + 15 * breiter)
			i += 1

		const x = i % (BREIT + breiter)
		const y = Math.floor(i / (BREIT + breiter))
		const data = { x: x, y: y }
		if (y === 0) {
			if (x === 4)
				data.gleis = {w: 0, phase: 2, strecken: [[1, 2]]}
			else if (x === 5) {
				data.typ = 3
				data.name = "Kiel"
			} else if (x === 8)
				data.gleis = {w: 0, phase: 2, strecken: [[-1, 2]], geld: true}
		} else if (y === 1) {
			if (x === 3)
				data.gleis = {w: 0, phase: 2, strecken: [[2, 3]]}
			else if ((x === 5) || (x === 7))
				data.typ = 1
			else if (x === 6)
				data.typ = 1
		} else if (y === 2) {
			if (x === 2)
				data.gleis = {w: 0, phase: 2, strecken: [[2, 3]], geld: true}
			else if (x === 3)
				data.gleis = {w: 0, phase: 2, strecken: [[-1, 3], [0, 2]], geld: true}
			else if (x === 5) {
				data.name = "Hamburg"
				data.geld = 50
				data.gleis = {w: 0, phase: 1, strecken: [[-1, 5], [0, 1], [-1, 3]], reihe: [[221, 3]],
				              bhfe: [[8, 25], [-1.1, 20], [5.1, 25]], streckenBhf: true, geld: 40}
			} else if (x === 6) {
				data.name = "Schwerin"
				data.gleis = {w: 0, phase: 2, strecken: [[0, 2], [0, 4], [2, 4]], bhfe: [[-1.4, 30]], geld: 10}
			} else if (x === 10) {
				if (SCHLESIEN) {
					data.name = "Stettin"
					data.gleis = {w: 0, reihe: GLEIS_REIHEN[3], bhfe: [[-4, 17]]}
				} else // Ostpreußen
					data.gleis = {w: 0, phase: 2, strecken: [[-1, 4]], geld: [20, 20, 40]}
			} else if (x === 12) // Ostpreußen (SCHLESIEN)
				data.gleis = {w: 0, phase: 2, strecken: [[-1, 3], [-1, 4]], geld: [20, 20, 40]}
		} else if (y === 3) {
			if (x === 1)
				data.gleis = {w: 0, phase: 2, strecken: [[0, 2]]}
			else if (x === 2) {
				data.name = "Oldenburg"
				data.gleis = {w: 0, phase: 2, strecken: [[0, 1], [0, 2], [0, 5], [1, 2], [1, 5], [2, 5]], bhfe: [[-.9, 27]], geld: 10}
			} else if (x === 3) {
				data.typ = 3
				data.name = "Bremen"
				data.geld = 50
			} else if (x === 6)
				data.geld = 50
			else if (x === 10) {
				data.typ = 2
				data.geld = 50
			}
		} else if (y === 4) {
			if (x === 7)
				data.geld = 50
			else if (x === 9) {
				data.name = "Berlin"
				data.gleis = {w: 0, phase: 0, strecken: [[-1, 4], [-1, SCHLESIEN ? 0 : 5]],
				              reihe: [[209, 5], [209, SCHLESIEN ? 1: 2], [209, 3], [209, 4]],
				              bhfe: [[5, 30], [6, 5]], streckenBhf: true, geld: 30}
			} else if (x === 10) {
				if (SCHLESIEN) {
					data.typ = 1
					data.geld = 50
				} else
					data.gleis = {w: 0, phase: 2, strecken: [[3, 4], [3, 5], [4, 5]], geld: true}
			} else if (x === 11)
				data.typ = 1
		} else if (y === 5) {
			if ((x === 1) || (x === 2))
				data.typ = 1
			else if (x === 4) {
				data.typ = 3
				data.name = "Hannover"
			} else if (x === 5) {
				data.name = "Braunschweig"
				data.gleis = {w: 0, phase: 2, strecken: [[1, 3], [1, 4], [3, 4]], bhfe: [[-3.6, 28]], geld: 20}
			} else if (x === 6) {
				data.name = "Magdeburg"
				data.gleis = {w: 0, reihe: GLEIS_REIHEN[3], bhfe: [[.7, 20]]}
			} else if (x > 10)
				data.geld = 50
		} else if (y === 6) {
			if (x === 0)
				data.gleis = {w: 0, phase: 2, strecken: [[1, 2]]}
			else if (x === 1) {
				data.phase = 0
				data.typ = 5
				data.name = "Essen/Duisburg"
				data.geld = 50
			} else if (x === 2) {
				data.name = "Dortmund"
				if (SCHLESIEN)
					data.typ = 3
				else
					data.gleis = {w: 0, reihe: GLEIS_REIHEN[3], bhfe: [[-5.7, 28]]}
			} else if ((x === 5) || (x === 7))
				data.typ = 1
			else if (x === 6)
				data.geld = 70
			else if (!SCHLESIEN && (x === 10)) {
				data.gleis = {w: 0, phase: 2, strecken: [[3, 5]]}
			} else if (x === 11)
				data.typ = 2
			else if (x === 12)
				data.geld = 50
		} else if (y === 7) {
			if (x === 0) {
				data.name = "Düsseldorf"
				data.gleis = {w: 0, reihe: GLEIS_REIHEN[4], bhfe: [[-1, 32]]}
			} else if (x === 1)
				data.typ = 2
			else if ((x === 2) || (x === 3) || (x === 5))
				data.geld = 70
			else if (x === 4)
				data.typ = 1
			else if (x === 7) {
				data.name = "Leipzig"
				data.gleis = {w: 0, reihe: GLEIS_REIHEN[3], bhfe: [[-2, 26]]}
			} else if (x === 9) {
				data.typ = 4
				data.name = "Dresden"
			} else if (!SCHLESIEN && (x === 10)) // Oberschlesien
				data.gleis = {w: 0, phase: 2, strecken: [[-1, 4]], geld: [20, 30, 40]}
			else if (x === 11)
				data.typ = 1
			else if (x === 12) {
				data.name = "Breslau"
				data.gleis = {w: 0, reihe: GLEIS_REIHEN[4], bhfe: [[-1, 30]]}
			} else if (x === 13)
				data.typ = 1
		} else if (y === 8) {
			if (x === 0)
				data.gleis = {w: 0, phase: 2, strecken: [[0, 1], [0, 2], [1, 2]], geld: true}
			else if (x === 1) {
				data.typ = 4
				data.name = "Köln"
				data.geld = 50
			} else if ((x === 3) || (x === 4))
				data.geld = 70
			else if (x === 6) {
				data.typ = 1
				data.geld = 70
			} else if (x === 7)
				data.typ = 1
			else if (x === 8)
				data.typ = 2
			else if (x === 9)
				data.gleis = {w: 0, phase: 2, strecken: [[0, 4], [0, 5], [4, 5]]}
			else if (x === 12)
				data.gleis = {w: 0, phase: 2, strecken: [[0, 5]]}
			else if (x === 14) // Kattowitz
				data.gleis = {w: 0, phase: 2, strecken: [[-1, 5]], geld: [20, 30, 40]}
		} else if (y === 9) {
			if (x === 1) // Koblenz
				data.gleis = {w: 0, phase: 2, strecken: [[0, 1], [2, 5]], streckenBhf: true, geld: [false, true]}
			else if (x === 2) {
				data.phase = 0
				data.typ = 5
				data.name = "Mainz/Wiesbaden"
				data.geld = 50
			} else if (x === 3) {
				data.name = "Frankfurt"
 				data.geld = 50
				data.gleis = {i: 208, w: 0, bhfe: [[2.2, 26], [4.5, 28]]}
			} else if (x === 6)
				data.geld = 70
			else if (x === 7)
				data.gleis = {w: 0, phase: 2, strecken: [[0, 3], [0, 4], [3, 4]]}
		} else if (y === 10) {
			if ((x === 1) || (x === 5))
				data.typ = 1
			else if ((x === 3) || (x === 4))
				data.geld = 50
			if (x === 7)
				data.geld = 70
		} else if (y === 11) {
			if (x === 0)
				data.typ = 1
			else if (x === 2) {
				data.name = "Ludwigshafen/Mannheim"
				data.geld = 50
				data.gleis = {w: 0, phase: 0, reihe: GLEIS_REIHEN[5], bhfe: [[11.2, 28]]}
			} else if (x === 3)
				data.typ = 2
			else if (x === 6) {
				data.name = "Fürth/Nürnberg"
				data.gleis = {i: 212, w: 0, bhfe: [[-3.7, 30], [-1.6, 26]]}
			}
		} else if (y === 12) {
			if (x === 2) // Elsaß/Lothringen oben
				data.gleis = {w: 0, phase: 2, strecken: [[-1, 0]], geld: [0, 50, 0]}
			else if (x === 3)
				data.typ = 2
			else if (x === 4) {
				data.name = "Stuttgart"
				data.gleis = {w: 0, reihe: GLEIS_REIHEN[3], bhfe: [[3.6, 30]]}
			} else if (x === 6)
				data.geld = 50
			else if (x === 7) {
				data.typ = 1
				data.geld = 50
			} else if (x === 8)
				data.geld = 50
			else if (x === 9)
				data.gleis = {w: 0, phase: 2, strecken: [[3, 4]], geld: true}
		} else if (y === 13) {
			if (x === 1) // Elsaß/Lothringen unten
				data.gleis = {w: 0, phase: 2, strecken: [[-1, 1]], geld: [0, 50, 0]}
			else if (x === 2)
				data.gleis = {w: 0, phase: 2, strecken: [[0, 3], [0, 4], [3, 4]]}
			else if (x === 3)
				data.geld = 70
			else if (x === 4)
				data.typ = 2
			else if (x === 5) {
				data.typ = 3
				data.name = "Augsburg"
			}
		} else if (y === 14) {
			if (x === 2) {
				data.typ = 3
				data.name = "Freiburg"
			} else if (x === 3)
				data.geld = 70
			else if (x === 7) {
				data.name = "München"
				data.gleis = {w: 0, reihe: GLEIS_REIHEN[4], bhfe: [[-4.9, 30]]}
			}
		} else if (y === 15) {
			if (x === 2)
				data.gleis = {w: 0, phase: 2, strecken: [[0, 5]]}
			else if (x === 4)
				data.gleis = {w: 0, phase: 2, strecken: [[0, 5]], geld: true}
			else if (x === 6)
				data.gleis = {w: 0, phase: 2, strecken: [[0, 5]]}
		}
		feldData.push(data)
	}
	return feldData
}

/**
 * @param {int} x Die x Position
 * @param {int} y Die y Position..
 * @param {int} i Die Richtung des Nachbarns
 * @returns Ob der Nachar der Felds übersprungen werden soll
 */
export function ueberspringtNachbar(x, y, i) {
	return ((x === 4) && (y === 1) && (i === 3)) // Spezialfall Elbe
	        || ((x === 4) && (y === 2) && (i === 0))
}

export function gibStadtBei(x, y) {
	return feldData.find( d => (d.x === x) && (d.y === y) ).name
}

export const STARTSTAND = {
	bank: {
		pool: [],
		loks: []
	},
	privat: [0, 0, 0, 0, 0, 0],
	vorPR: [
		{ geld: 0, loks: [], wert: 0 },
		{ geld: 0, loks: [], wert: 0 },
		{ geld: 0, loks: [], wert: 0 },
		{ geld: 0, loks: [], wert: 0 },
		{ geld: 0, loks: [], wert: 0 },
		{ geld: 0, loks: [], wert: 0 }
	],
	ags: {
		PR: { kurs: [8, 3], geld: 0, loks: [], wert: 0 },
		BY: { kurs: [2, 4], geld: 0, loks: [], wert: 0 },
		SX: { kurs: [2, 3], geld: 0, loks: [], wert: 0 },
		BA: { kurs: [1, 3], geld: 0, loks: [], wert: 0 },
		WT: { kurs: [1, 3], geld: 0, loks: [], wert: 0 },
		HE: { kurs: [1, 3], geld: 0, loks: [], wert: 0 },
		MS: { kurs: [1, 2], geld: 0, loks: [], wert: 0 },
		OL: { kurs: [1, 2], geld: 0, loks: [], wert: 0 }
	},
	verlauf: {},
	dran: [],
	gleise: [],
	bhfe: [],
	log: []
}