import { Stand } from "./Stand.js"

const ZUG_NACHRICHT_TYP = "a0"
const GLEIS_NEU_NACHRICHT_TYP = "a1"
const GLEIS_ANDERS_NACHRICHT_TYP = "a2"
const GLEIS_WEG_NACHRICHT_TYP = "a3"
const BHF_NEU_NACHRICHT_TYP = "a4"
const BHF_WEG_NACHRICHT_TYP = "a5"
const FAHRSTRECKE_NACHRICHT_TYP = "a6"
const BESTÄTIG_LOKKAUF_NACHRICHT_TYP = "a7"
const LOKKAUF_BESTÄTIGT_NACHRICHT_TYP = "a8"
const EXTRA_ZUG_NACHRICHT_TYP = "a9"
const TEXT_NACHRICHT_TYP = "aa"
const ZUG_ZURÜCK_NACHRICHT_TYP = "ab"

function verbinden(typ, funktion) {
	TogetherJS.hub.on(typ, function (msg) {
		funktion(msg.data)
	})
}

function sende(typ, data) {
	if (TogetherJS.running) {
		const nachricht = { type: typ }
		if (data)
			nachricht.data = data
		TogetherJS.send(nachricht)
	}
}

export class Verteiler
{
	static set beiSpielerWechsel(funktion) {
		console.log("TJS geladen", TogetherJS)
		const session = TogetherJS.require("session")
		const self = TogetherJS.require("peers").Self
		if (session && self) {
			console.log("beiSpielerWechsel setzen")
			session.on("self-updated", function () { funktion(self.name) })
			funktion(self.name)
		} else
			TogetherJS.on("ready", function () {
				const session = TogetherJS.require("session")
				const self = TogetherJS.require("peers").Self
				console.log("beiSpielerWechsel setzen nach TJS ready")
				session.on("self-updated", function () { funktion(self.name) })
			})
	}

	static sende(typ, data) {
		if (TogetherJS.running) {
			const nachricht = { type: typ }
			if (data)
				nachricht.data = data
			TogetherJS.send(nachricht)
		}
	}
	
	static set beiZug(funktion) {
		verbinden(ZUG_NACHRICHT_TYP, funktion)
	}
	
	static sendeZug(data) {
		sende(ZUG_NACHRICHT_TYP, data)
	}
	
	static set beiGleisNeu(funktion) {
		verbinden(GLEIS_NEU_NACHRICHT_TYP, funktion)
	}
	
	static sendeGleisNeu(data) {
		sende(GLEIS_NEU_NACHRICHT_TYP, data)
	}
	
	static set beiGleisAnders(funktion) {
		verbinden(GLEIS_ANDERS_NACHRICHT_TYP, funktion)
	}
	
	static sendeGleisAnders(data) {
		sende(GLEIS_ANDERS_NACHRICHT_TYP, data)
	}
	
	static set beiGleisWeg(funktion) {
		verbinden(GLEIS_WEG_NACHRICHT_TYP, funktion)
	}
	
	static sendeGleisWeg() {
		sende(GLEIS_WEG_NACHRICHT_TYP)
	}
	
	static set beiBhfNeu(funktion) {
		verbinden(BHF_NEU_NACHRICHT_TYP, funktion)
	}
	
	static sendeBhfNeu(data) {
		sende(BHF_NEU_NACHRICHT_TYP, data)
	}
	
	static set beiBhfWeg(funktion) {
		verbinden(BHF_WEG_NACHRICHT_TYP, funktion)
	}
	
	static sendeBhfWeg() {
		sende(BHF_WEG_NACHRICHT_TYP)
	}
	
	static set beiFahrstrecke(funktion) {
		verbinden(FAHRSTRECKE_NACHRICHT_TYP, funktion)
	}
	
	static sendeFahrstrecke(data) {
		sende(FAHRSTRECKE_NACHRICHT_TYP, data)
	}
	
	static set beiBestätigLokkauf(funktion) {
		verbinden(BESTÄTIG_LOKKAUF_NACHRICHT_TYP, funktion)
	}
	
	static sendeBestätigLokkauf(data) {
		sende(BESTÄTIG_LOKKAUF_NACHRICHT_TYP, data)
	}
	
	static set beiLokkaufBestätigt(funktion) {
		verbinden(LOKKAUF_BESTÄTIGT_NACHRICHT_TYP, funktion)
	}
	
	static sendeLokkaufBestätigt(data) {
		sende(LOKKAUF_BESTÄTIGT_NACHRICHT_TYP, data)
	}
	
	static set beiExtraZug(funktion) {
		verbinden(EXTRA_ZUG_NACHRICHT_TYP, funktion)
	}
	
	static sendeExtraZug(data) {
		sende(EXTRA_ZUG_NACHRICHT_TYP, data)
	}

	static set beiText(funktion) {
		verbinden(TEXT_NACHRICHT_TYP, funktion)
	}
	
	static sendeText(data) {
		sende(TEXT_NACHRICHT_TYP, data)
	}

	static set beiZugZurück(funktion) {
		verbinden(ZUG_ZURÜCK_NACHRICHT_TYP, funktion)
	}
	
	static sendeZugZurück(data) {
		const rück = {}
		let senden = false
		TogetherJS.require("peers").getAllPeers(true).forEach( p => {
			const nr = Stand.gibSpielerNr(p.name)
			const r = data[nr]
			if (r) {
				rück[nr] = r
				delete data[nr]
				senden = true
			}
		} )
		if (senden)
			sende(ZUG_ZURÜCK_NACHRICHT_TYP, rück)
	}
}