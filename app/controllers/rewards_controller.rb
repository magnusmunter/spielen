class RewardsController < ApplicationController
  before_action :set_reward, only: [ :show, :edit, :update, :destroy ]
  before_action :set_funding, only: [ :new, :create ]
  
  skip_before_action :require_team
  before_action :require_editor, except: [ :index, :show ]
  
  # --------------------------------------------
  # CRUD methods
  
  # GET /rewards
  # GET /rewards.json
  def index
    render 'pages/no_access' and return unless h_page_accessible? 'rewards', current_account
    
    if h_is_editor?
      @rewards = Reward.all
      
    else
      @rewards = Reward.published
    end
    
    write_log 'index_rewards'
  end
  
  # GET /rewards/new
  def new
    if defined? @funding
      @reward = @funding.rewards.build status: 'new'
    else
      @reward = Reward.new
    end
  end
  
  # GET /rewards/1/edit
  def edit
  end
  
  # POST /rewards
  # POST /rewards.json
  def create
    if defined? @funding
    @reward = @funding.rewards.build( reward_params  )
    else
      @reward = Reward.new( reward_params )
    end
    
    respond_to do |format|
      if @reward.save
        format.html { redirect_to @reward.funding ? @reward.funding : rewards_path, notice: 'Produkt wurde erfolgreich gespeichert.' }
        format.json { render :show, status: :created, location: @reward }
      else
        format.html { render :new }
        format.json { render json: @reward.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /rewards/1
  # PATCH/PUT /rewards/1.json
  def update
    respond_to do |format|
      if @reward.update( reward_params )
        format.html { redirect_to @reward.funding ? @reward.funding : rewards_path, notice: 'Produkt wurde erfolgreich geändert.' }
        format.json { render :show, status: :ok, location: @reward }
      else
        format.html { render :edit }
        format.json { render json: @reward.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /rewards/1
  # DELETE /rewards/1.json
  def destroy
    @funding = @reward.funding
    @reward.destroy
    
    respond_to do |format|
      format.html { redirect_to @funding, notice: 'Produkt wurde gelöscht.' }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_reward
    @reward = Reward.find_by token: params[ :id ].split( '-' ).first
    render_404 and return if @reward.blank?
  end
  
  def set_funding
    if numeric? params[ :funding_id ]
      @funding = Funding.find params[ :funding_id ].to_i
    elsif params[ :funding_id ]
      @funding = Funding.find_by token: params[ :funding_id ].split( '-' ).first
    end
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def reward_params
    params.require( :reward ).permit( :status, :picture, :price, :title, :category, :available, :description, :delivery )
  end
  
end
