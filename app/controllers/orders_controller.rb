class OrdersController < ApplicationController
  skip_before_action :require_team
  before_action :require_sales, except: [ :activate, :show, :new, :edit, :create, :update ]
  
  before_action :set_order, only: [ :activate, :show, :edit, :update, :destroy ]
  
  # --- CUSTOM METHODS -------------------------
  
  # GET /orders/1/activate
  def activate
    @successful = false
    
    if @order.secret == params[ :ring ]
      @order.status = 'open'
      @order.contact.status = 'confirmed'
      
      @successful = @order.contact.save and @order.save
      
      if @successful
        session[ :order_token ] = @order.token
        
        # send notification if necessary
        if @order.first_order_of_day?
          OrdersMailer.with( order: @order ).
              new_order_notification_mail.
              deliver_later
        end
      end
    end
  end
  
  # --- STANDARD CRUD METHODS ------------------
  
  # GET /orders or /orders.json
  def index
    @orders = Order.order created_at: :desc
  end
  
  # GET /orders/1 or /orders/1.json
  def show
    require_sales_or_owner
    render :show_sales if h_is_sales?
  end
  
  # GET /orders/new
  def new
    # fetch category and products
    @category = params[ :category ] || nil
    if @category.blank?
      @products = Reward.where funding_id: nil
    else
      @products = Reward.where category: @order.category
    end
    
    # build new order with orderquant stubs
    @order = Order.new status: 'new', step: 1, category: @category
    @products.each do |product|
      @order.orderquants.build reward: product
    end
  end
  
  # GET /orders/1/edit
  def edit
    require_sales_or_owner
    
    if h_is_sales?
      render :edit_sales
      
    else
      # ensure order has contact
      unless @order.contact
        if current_account
          @order.contact = current_account.contact
        else
          @order.build_contact
          @order.contact.seed_checker_question
        end
      end
      
      # increase order step
      @order.step += 1
    end
  end
  
  # POST /orders or /orders.json
  def create
    @order = Order.new( order_params )
    @order.status = 'preliminary'
    
    respond_to do |format|
      if @order.save
        session[ :order_token ] = @order.token
        
        format.html { redirect_to edit_order_path( @order ) }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /orders/1 or /orders/1.json
  def update
    require_sales_or_owner
    
    order_done = params[ :order ][ :step ].to_i >= 5
    redirect_target = ( order_done or h_is_admin? ) ? @order : edit_order_path( @order )
    
    @order.assign_attributes order_params
    if @order.contact and @order.contact.new_record?
      @order.contact.status = 'order'
    end
    
    respond_to do |format|
      if @order.save
        if order_done
          @order.update_attribute( :status, 'new' ) unless h_is_admin?
          
          if session[ :order_token ] == @order.token
            OrdersMailer.with( order: @order ).
            confirmation_mail.
            deliver_later
          end
        end
        
        format.html { redirect_to redirect_target }
        format.json { render :show, status: :ok, location: @order }
      else
        @order.contact.seed_checker_question
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /orders/1 or /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: "Order was successfully destroyed." }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find( params[ :id ] )
  end
  
  # Only allow a list of trusted parameters through.
  def order_params
    params.require( :order ).permit( :token, :status, :contact_id, :customer_kind, :customer_location, :delivery_org, :delivery_name, :delivery_street, :delivery_housenr, :delivery_plz, :delivery_city, :delivery_country, :invoice_differs, :invoice_org, :invoice_name, :invoice_street, :invoice_housenr, :invoice_plz, :invoice_city, :invoice_country, :read_privacy, :read_terms, :delivery_status, :delivery_date_at, :delivery_reference, :invoice_status, :invoice_date_at, :invoice_reference, :step, contact_attributes: [ :id, :email, :first_name, :last_name, :street, :housenr, :plz, :city, :phone_nr, :op_a, :op_b, :solved ], orderquants_attributes: [ :id, :reward_id, :quantity ] )
  end
  
  def require_sales_or_owner
    redirect_to root_path, alert: 'Keine Berechtigung für diesen Zugriff' and return unless session[ :order_token ] == @order.token or h_is_sales?
  end
  
end
