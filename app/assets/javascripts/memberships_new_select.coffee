# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# ----------------------------------------------

($ document).on 'turbolinks:load', ->
  console.log '> script @ memberships_new_select.coffee'
  
  select_switch = ->
    $ich = ($ this)
    console.log "-- select_switch - ##{$ich.attr 'id'} @ memberships_new_select.coffee"
    sel = $ich.data 'sel'
    console.log "-- sel:#{sel}, val:#{$ich.val()}"
    if $ich.val() is 'direct_debit'
      ($ sel).fadeIn 200
    else
      ($ sel).fadeOut 200
    
    return false # hat_ziel
  
  
  # --- Initialisierung ------------------------
  
  $select = ($ 'select#membership_fee_mode')
  unless $select.data 'init_membership_fee_mode'
    console.log '-- init membership_fee_mode'
    $select.on 'input.membership_fee_mode',
        '',
        select_switch
    $select.data 'init_membership_fee_mode', true
  

