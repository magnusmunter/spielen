# Be sure to restart your server when you modify this file.

# Naming of your organisation and her domain
Rails.application.config.orga_name = 'Spielen'
Rails.application.config.orga_domain = 'spielen.io'
Rails.application.config.orga_slogan = 'Gerne mal ne Runde zocken? - spielen.io'
Rails.application.config.orga_color = '#18a328'
