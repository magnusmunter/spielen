class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :token
      t.string :status
      t.string :titel
      t.datetime :published_at
      t.string :description

      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
