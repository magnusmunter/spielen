class AddOffersHelpToContacts < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :offers_help, :boolean
  end
end
