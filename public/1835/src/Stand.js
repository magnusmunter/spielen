import { ref, computed, watch } from "../../lib/vendor/vue.esm-browser.js"
import { defineStore } from "../../lib/vendor/pinia.esm-browser.js"
import { SCHLESIEN, BAHNEN, LOKS, GLEIS_VORLAGE, BHF_VORLAGE, STARTSTAND } from "../config.js"
import { standPfad, chatPfad, gibAktienWert, gibPapierWert, gibAktienpackete, gibPapierlimit, spielNr } from "./global.js"
import { Verteiler } from "./Verteiler.js"

/**
 * stand.json Schnittstelle
{
	"bank": {  Info über die Bank
		"geld": int  Bankvermögen
		"aktien": array<"BY"|"BY2"..>  aktuell kaufbare Aktien (BY 10%, BY2 20% Bayern, PR 5%, PR2 10% Preußen)
		"pool": array<"BY"|"BY2"..>  aktuelle Aktien im Pool
		"loks": array<"2"|"2+"..>  aktuell kaufbare Loks
	},
	"spieler": {  Info über die Spieler
		"<name>": {
			"geld": int  Bargeld
			"papiere": array<"p1"|.."v1"|.."BY"|"BY2"..>  Papiere des Spielers (p1-6 Privat-, v1-6 Vorpreußische Bahn )
			"to": string email Adresse für Benachrichtigung
			"sperre": array<AGname> Aktiensperre für diese Runde verkaufte Atkien
			"passt": bool Automatisch weiterpassen
		}
	},
	"vorPR": [{  Info über die Vorpreußische Bahnen (nach Nummer)
		"geld": int  Bargeld
		"loks": array<"2"|"2+"..>  Loks
	}],
	"ags": {  Info über die AGs (PR, BY, SX. BA, WT. HE, MS, OL)
		"<name>": {
			"direktor": int  Index des Spielers
			"betrieb": bool  ist in Betrieb
			"kurs": array<int>  Position auf der Kurstabelle x, y von links unten
			"geld": int  Bargeld
			"loks": array<"2"|"2+"..>  Loks
		}
	},
	"clemens" 0|1 ob Startpaket nach Clemens versteigert werden soll
	"runde": -1|0|1|2|3  -1 Start-, 0 Aktien-, 1-3 Betriebsrunde
	"phase": 0|1|2  Spielphase (gelb, grün, braun)
	"vierPhase": bool markiert Zwischenphase nach Verkauf der ersten 4er
	"dran": Array<int|"v1"..|"BY"..>  Index des aktuellen Spielers/ handelnde Bahn, 
	                                  bei AGs in der Reihenfolge der noch folgenden AGs nach Kurstabelle,
									  bei Extrazug die zwischenreingeschobenen
	"lok": "2"|"2+".. nächste zukünftig kaufbare Lok
	"aktienStart": int  Index des die nächste Aktienrunde Beginnenden
	"wechsel": bool um zu markieren, dass die Zahl der Spielrunden noch wie in der alten Phase gilt
	"tausch": array<PRpapiere> die Papiere der Preußen, die diese Runde ausgetauscht wurden
	"extra": [ Sonderzug für zwangsweise Zwischenzüge
		int|String Index des eigentlich aktuellen Spielers/ eigentlich handelnde Bahn
		Object bis zum Extrazug gemachte Aktionen (siehe global.js gibAktionenText)
		..
		Object Extrazug: Lokaktion + von | Papieraktion + von
	]
	"ende": bool markiert Spielende
	"gleise": [{
		"i": int Nummer des Gleises
		"x": int x Position auf dem Plan
		"y": int y Position auf dem Plan
		"w": 1-6 Winkel des Gleises
	}],
	"bhfe": [{
		"bahn": int  Bahn
		"x": int x Position auf dem Plan
		"y": int y Position auf dem Plan
		"n": 0|1|2 Position auf dem Gleis
	}],
	"log": [
		{
			"wer": Index des Spielers/ handelnde Bahn
			"was": [ Aktion siehe global.js gibAktionenText ]
		}
	]
}
 **/

export const STARTGELD = 12000
export const STARTPAKET = [ "p1", "v1", "p2", "v2", "v3", "v4", "BY2", "p3", "p4", "v5", "v6", "p5", "p6" ]
const STAND_ID = "stand_" + spielNr

export const nutzStand = defineStore("stand", () => {
	const bank = ref({})
	const spieler = ref({})
	const vorPR = ref([])
	const ags = ref({})
	const clemens = ref(undefined)
	const standRunde = ref(-1)
	const phase = ref(0)
	const vierPhase = ref(false)
	const runde = ref(0)
	const dran = ref("")
	const aktuellerSpieler = ref("")
	const aktienStart = ref(-1)
	const passt = ref(false)
	const lok = ref(null)
	const admin = ref(false)
	const aktionen = ref([])
	const extra = ref(null)
	const gleise = ref([])
	const bhfe = ref([])
	const auswählen = ref(null)
	const fahrbareStrecken = ref([])
	const letzteFahrt = ref(null)
	const neueSpur = ref([])
	const tausch = ref(null)
	const tauschtPreußen = ref(false)
	const preußenDir = ref(undefined)
	const log = ref([])
	const letzteLogNr = ref(undefined)
	const tabelle = ref(null)

	const kursloseAGs = computed( () => {
		const agsV = ags.value
		return Object.keys(agsV).filter( n => agsV[n].betrieb && !agsV[n].kurs )
	} )
	const dranAG = computed( () => ags.value[dran.value] )
	const kannPapierKaufen = computed( () => (dran.value === aktuellerSpieler.value) && aktionen.value.every( a => a.geld >= 0 ) )
	const fährt = computed( () => neueSpur.value.lok >= 0 )
	
	const istAktuellerSpielerDran = computed( () =>  {
		const aktuellerSpielerV = aktuellerSpieler.value
		const data = spieler.value[aktuellerSpielerV]
		if (!data)
			return
		const dranV = dran.value
		if (dranAG.value) {
			const aktion = aktionen.value.at(-1)
			if (aktion && (aktion.art === 17)) // ehemaliger Direktor ist bankrott
				return aktion.wer === Stand.gibSpielerNr(aktuellerSpielerV)
			const nr = Stand.gibSpielerNr(aktuellerSpielerV)
			return (dranAG.value.direktor === nr) || ((dranV === "PR") && (preußenDir.value === nr))
		}
		return data.papiere.includes(dranV)
	} )

	const spielerNameFürAdmin = computed( () => {
		const spielerV = spieler.value
		const dranV = dran.value
		if (spielerV[dranV])
			return dranV
		const agsV = ags.value
		if (agsV[dranV])
			return Object.keys(spielerV)[agsV[dranV].direktor]
		const eintrag = Object.entries(spielerV).find( s => s[1].papiere.includes(dranV) )
		return eintrag ? eintrag[0] : Object.keys(spielerV)[preußenDir.value]
	})
	
	/**
	 * Verteilt Papier an den entsprechenden Spieler für die Aufnahme eines Tischspielstands
	 * @param {} art Die Art des Papiers
	 * @param {String} wem Spielername, an den das Papier gehen soll
	 */
	function verteilPapier(art, wem) {
		const aktien = bank.value.aktien
		const i = STARTPAKET.indexOf(art)
		if (i >= 0)
			STARTPAKET.splice(i, 1)
		if (["p3", "p4", "v2", "v4"].includes(art))
			aktien.splice(aktien.indexOf("PR2"), 1)
		if (["v1", "v3", "v5", "v6"].includes(art))
			aktien.splice(aktien.indexOf("PR"), 1)
		aktien.splice(aktien.indexOf(art), 1)
		spieler.value[wem].papiere.push(art)
		const ag = art.slice(0, 2)
		const agsV = ags.value
		if (agsV[ag]) {
			const z = art.length - 1
			if (10 - gibAktienpackete(aktien, agsV)[ag] == (ag === "PR" ? z / 2 : z))
				agsV[ag].direktor = Object.keys(spieler.value).findIndex( s => s === wem )
			else
				Stand.prüfUndÄnderDirektor(agsV[ag], Object.values(spieler.value), ag, data)
		}
	}

	function reichtGeld(geld) {
		if (!geld)
			return true
		const dranV = dran.value
		const info = spieler.value[dranV] || dranAG.value || vorPR.value[dranV[1] - 1]
		if (geld > info.geld) {
			alert((BAHNEN[dranV] || dranV) + " hat nicht genug Geld")
			return false
		} else
			return true
	}

	function kaufPapier(typ, von, sendeZug) {
		const bankV = bank.value
		const agsV = ags.value
		const dranV = dran.value
		const info = this.spieler[dranV]
		const kauft = von !== dranV
		const bahn = typ.substr(0, 2)
		const zwangsweise = info.geld < 0
		const extraV = extra.value
		const aktionenV = aktionen.value
		const prüfBankrott = zwangsweise && (() => {
			if (!extraV.blockt)
				extraV.blockt = []
			extraV.blockt.push(typ)
			if (!extraV.blockt.reduce( (r, p) => {
				const i = r.indexOf(p)
				if (i >= 0)
					r.splice(i, 1)
				return r
			} , info.papiere.filter( p => agsV[p.substring(0,2)] )).length) {
				aktionenV.splice(0, aktionenV.length, ...extraV.slice(1), { art: 17, wer: Stand.gibSpielerNr(dranV) })
				alert(dranV + " ist Bankrott")
				sendeZug()
			}
		})
		if (kauft) {
			if (info.sperre && info.sperre.some( s => typ.startsWith(s) ))
				return alert(dranV + " kann diese Runde verkaufte Aktien nicht wieder kaufen")
			else if (info.papiere.length >= gibPapierlimit(dranV, this))
				return alert(dranV + " hat sein Papierlimit erreicht")
		} else { // verkauft
			Stand.sperrAGFürSpieler(bahn, info)
			let teil = typ.length > 2 ? 2 : 1
			if (typ.startsWith("PR"))
				teil /= 2
			if ((typ.length > 2) && (agsV[bahn].direktor === Stand.gibSpielerNr(dranV))
				&& Object.values(spieler.value).every( s => (s === info) || (gibAktienpackete(s.papiere, agsV)[bahn] || 0) < teil )) {
				alert(dranV + " darf Direktoraktie nicht verkaufen,\nda sie sonst niemand übernehmen kann")
				if (zwangsweise)
					prüfBankrott()
				return
			}
			const pool = gibAktienpackete(bankV.pool, agsV)[bahn]
			if (pool && (teil + pool > 5)) {
				alert(dranV + " darf diese Aktie nicht verkaufen,\nda höchstens die Hälfte der AG am Markt sein darf")
				if (zwangsweise)
					prüfBankrott()
				return
			}
			if (zwangsweise && extraV && typ.startsWith(extraV[0])) {
				const anteil = gibAktienpackete(info.papiere, agsV)[bahn]
				if (Object.values(spieler.value).some( s => (s !== info) && (anteil - teil < gibAktienpackete(s.papiere, agsV)[bahn]) )) {
					alert(dranV + " darf seinen Direktorposten nicht aufgeben")
					prüfBankrott()
					return
				}
			}
		}
		
		let wert
		if (von) {
			wert = gibAktienWert(agsV[bahn])
			if (typ.startsWith("PR"))
				wert /= 2
			if (typ.length > 2)
				wert *= 2
		} else
			wert = gibPapierWert(typ)
		if (von && kauft && (von !== "pool")) // verstaatlichen
			wert *= 1.5
		if (kauft)
			wert *= -1
		if (!kauft || reichtGeld(-wert)) {
			info.geld += wert
			if (wert > 0) { // verkaufen
				Stand.gibAbPapier(info.papiere, typ)
				Stand.nimmAufPapier(bankV.pool, typ)
			} else if (von && (von !== "pool")) { // verstaatlichen
				Stand.gibAbPapier(spieler.value[von].papiere, typ)
				Stand.nimmAufPapier(info.papiere, typ)
			} else { // kaufen
				const papiere = bankV[von || "aktien"]
				Stand.gibAbPapier(papiere, typ)
				Stand.nimmAufPapier(info.papiere, typ)
			}
			const aktion = { papier: typ, geld: wert }
			if (extraV) // Zwangsverkauf durch Limitüberschreitung oder Aktienzwangsverkauf / Bankrott?
				aktion.von = Stand.gibSpielerNr(dranV)
			else if (von && kauft)
				aktion.von = spieler.value[von] ? Stand.gibSpielerNr(von) : von
			aktionenV.push(aktion)
			if (["p1", "p5", "p6"].includes(typ)) {
				typ = "BY"
				Stand.nimmAufPapier(info.papiere, typ)
				aktionenV.push({ papier: typ })
			} else if (typ === "p2") {
				typ = "SX2"
				Stand.nimmAufPapier(info.papiere, typ)
				aktionenV.push({ papier: typ })
			}
			Stand.prüfUndÄnderDirektor(agsV[bahn], Object.values(spieler.value), bahn, this)
			if (info.geld < 0)
				prüfBankrott()
		}
	}

	function gibGleisRest(i) {
		return GLEIS_VORLAGE[i].zahl - gleise.value.filter(e => e.i === Number(i)).length
	}

	function gibBhfRest(bahn) {
		return BHF_VORLAGE[bahn].zahl - bhfe.value.filter( b => (b.bahn === bahn) && !b.extra ).length
	}

	function gibLokZahl(lok) {
		return LOKS[lok].zahl
	}

	function gibLoklimit(bahn) {
		let limit
		if (phase.value === 2)
			limit = bahn === "PR" ? 3 : 2
		else
			limit = (bahn === "PR") || !vierPhase.value ? 4 : 3
		return limit
	}

	function gibLokrest(bahn) {
		return gibLoklimit(bahn) - (bahn >= 0 ? vorPR.value[bahn - 1].loks.length + 2 : ags.value[bahn].loks.length)
	}

	const kannLokKaufen = computed( () => {
		const dranV = dran.value
		if (BAHNEN[dranV]) {
			const spielerV = spieler.value
			const aktuellerSpielerV = aktuellerSpieler.value
			if (dranAG.value)
				return gibLokrest(dranV) && (Object.keys(spielerV)[dranAG.value.direktor] === aktuellerSpielerV)
			else
				return gibLokrest(dranV[1]) && spielerV[aktuellerSpielerV]
				       && spielerV[aktuellerSpielerV].papiere.includes(dranV)
		} else
			return false
	} )

	const istLokAbgabe = computed( () => BAHNEN[dran.value] && !dran.value.startsWith("p")
	                                     && (gibLokrest(dranAG.value ? dran.value : dran.value[1]) < 0) )

	watch(dran, dran => {
		let fahrt = letzteFahrt.value
		if (fahrt) // alte letzte Fahrt löschen
			letzteFahrt.value = fahrt = null
		if (!extra.value && !tauschtPreußen.value) { // letzte Fahrt markieren
			const logV = log.value
			for (let i = logV.length - 1; !fahrt && (i >= 0); --i) {
				const zug = logV[i]
				fahrt = (zug.wer === dran) && zug.was.find( a => a.fährt )
			}
			if (fahrt)
				letzteFahrt.value = fahrt.fährt
		}
	})

	const fahrstrecken = computed( () => {
		const aktion = aktionen.value.find( a => a.fährt )
		return (aktion && aktion.fährt) || []
	} )
	
	return { bank, spieler, vorPR, ags, clemens, standRunde, phase, vierPhase, runde, dran, aktuellerSpieler, kursloseAGs, dranAG, aktienStart, passt, lok, admin,
	         aktionen, extra, gleise, bhfe, auswählen, fahrbareStrecken, letzteFahrt, log, letzteLogNr, tabelle, neueSpur, tausch, tauschtPreußen, preußenDir,
	         kannPapierKaufen, fährt, istAktuellerSpielerDran, spielerNameFürAdmin, verteilPapier, reichtGeld, kaufPapier, gibGleisRest, gibBhfRest, gibLokZahl, 
	         gibLoklimit, gibLokrest, kannLokKaufen, istLokAbgabe, fahrstrecken }
})

let data
let authenticity_token
let aktualisierStand

function gibWann() {
	return (new Date()).toISOString().replace("T", " ").substring(0, 19)
}

function starteNeuesSpiel(aktion) {
	const spieler = aktion.spieler
	if (aktion.clemens) {
		data.dran.push(spieler.length - 1)
		data.clemens = 1
	} else
		data.dran.push(0)
	const geld = Stand.gibStartGeld(spieler.length)
	data.bank.geld -= spieler.length * geld
	for (name of spieler)
		data.spieler[name] = { geld, papiere: [] }
	data.bank.aktien = JSON.parse(JSON.stringify(STARTPAKET))
	data.phase = 0
	data.runde = -1
	data.aktienStart = -1
	data.lok = "2"
	data.tabelle.runde = []
	for (let n in BAHNEN) // Verlaufstabelle initialisieren
		data.tabelle[n] = []
	for (let n of spieler)
		data.tabelle[n] = []
	fügZugEin(data.log, -1, [aktion])
}

function gibStartStand() {
	return JSON.parse(JSON.stringify(STARTSTAND))
}

function beiStandGeladen(json) {
	// console.log("JSON.load")
	data = json
	
	if (data.authenticity_token) {
		authenticity_token = data.authenticity_token

		let letzteLogNr
		if (Object.keys(data).toString() === "authenticity_token,zuege" ) {
			const züge = data.zuege
			data = JSON.parse(localStorage.getItem(STAND_ID))
			if (züge.length) {
				if (!data || (züge.length !== data.letzteZugNr)) { // neuere Züge vom server ziehen
					if (!data || data.offeneZüge || (züge.length < data.letzteZugNr)) {
						data = gibStartStand()
						const zug = züge[0].was[0]
						if (Array.isArray(zug.spieler)) {
							data.spieler = {}
							data.bank.geld = STARTGELD
							starteNeuesSpiel(zug)
							data.letzteZugNr = 1
						}
					} else
						letzteLogNr = data.log.length
					for (let zug of züge.slice(data.letzteZugNr)) {
						if (zug.text)
							data.log.push(zug)
						else if (zug.was.extra) {
							data.extra = zug.was.extra
							if (zug.was.dran) {
								data.dran = zug.was.dran
								if (data.dran[0] < 0) { // Preußentauschrunde beenden
									delete data.tauschtPreußen
									Stand.zieh(data.extra.slice(1), false, zug.wann)
								} else if ((data.extra[0] !== data.dran[0])
									&& (["4", "4+", "5"].includes(data.extra.at(-1).lok) || data.extra.some( z => z.ende ))) // Preußische können umgewandelt werden
									data.tauschtPreußen = true
							}
						} else {
							if (data.extra) {
								data.dran = [data.extra[0]]
								wählNächste()
								delete data.extra
								delete data.tauschtPreußen
							}
							const rück = zug.was.at(-1)
							if (rück && Object.keys(rück)[0] >= 0) {
								data.rück = rück
								zug.was.pop()
							}
							Stand.zieh(zug.was, false, zug.wann)
						}
					}
					data.letzteZugNr = züge.length
					const letzteAktionen = züge.at(-1).was
					if (letzteAktionen && letzteAktionen.extra)
						--data.letzteZugNr
					localStorage.setItem(STAND_ID, JSON.stringify(data))
				}
			} else if (!data && confirm("Neues Spiel beginnen?")) {
				data = gibStartStand()
				let nimmtAuf
				if (confirm("Spielstand aufnehmen?")) {
					nimmtAuf = true
					data.bank.aktien = ["p1", "p2", "p3", "p4", "p5", "p6", "v1", "v2", "v3", "v4", "v5", "v6",
										"PR2", "PR2", "PR2", "PR2", "PR2", "PR2", "PR2", "PR2", "PR", "PR", "PR", "PR",
										"BY2", "BY", "BY", "BY", "BY", "BY", "BY", "BY", "BY",
										"SX2", "SX", "SX", "SX", "SX", "SX", "SX", "SX", "SX",
										"BA2", "BA2", "BA", "BA", "BA", "BA", "BA", "BA",
										"HE2", "HE2", "HE", "HE", "HE", "HE", "HE", "HE",
										"WT2", "WT2", "WT", "WT", "WT", "WT", "WT", "WT",
										"MS2", "MS2", "MS2", "MS", "MS", "MS", "MS",
										"OL2", "OL2", "OL2", "OL", "OL", "OL", "OL"]
					data.runde = -2
				}
				data.spieler = {}
				const spieler = []
				let name
				do {
					name = prompt(`Name des neuen Spielers (noch ${spieler.length < 3 ? `mindestens ${3 - spieler.length}`
																					: `höchstens ${7 - spieler.length}`})`)
					if (name) {
						if (nimmtAuf)
							data.spieler[name] = { papiere: [] }
						spieler.push(name)
					}
				} while ((name && (spieler.length < 7) || (spieler.length < 3)))
				
				data.bank.geld = STARTGELD
				data.letzteZugNr = 0
				if (!nimmtAuf) {
					const namen = spieler.splice(0)
					while (namen.length)
						spieler.push(namen.splice(Math.floor(Math.random() * namen.length), 1)[0])
					const aktion = { spieler }
					if (confirm("Startversteigerung nach Clemens?"))
						aktion.clemens = 1
					starteNeuesSpiel(aktion)
					const zug = { was: [aktion], wann: gibWann() }
					Verteiler.sendeZug(zug)
					zug.dann = data.dran[0]
					sendeData(null, null, zug)
				}
			}
		} else {
			data.log = data.zuege
			delete data.zuege
			delete data.authenticity_token
		}
		
		aktualisierStand(data, letzteLogNr)
	} else
		aktualisierStand(data)
}

function gibAktienAus(ag, aktionen) {
	switch (ag) {
		case "BA":
		case "WT":
		case "HE":
			data.bank.aktien.push(ag + "2")
			for (let i = 0; i < 6; ++i)
				data.bank.aktien.push(ag)
			data.bank.aktien.push(ag + "2")
			break
		case "PR":
			for (let i = 0; i < 4; ++i)
				data.bank.aktien.push("PR2")
			break
		case "MS":
		case "OL":
			for (let i = 0; i < 3; ++i)
				data.bank.aktien.push(ag + "2")
			for (let i = 0; i < 4; ++i)
				data.bank.aktien.push(ag)
	}
	aktionen.push({ aktien: ag })
}

/**
 * @param {Array<int>} kurs Der Kurs 
 * @returns Ob der angegebene Kurs auf einem Feld mit Pfeil nach oben liegt
 */
function istAmPfeilHoch(kurs) {
	return ((kurs[1] === 0) && (kurs[0] > 3)) || ((kurs[1] === 1) && (kurs[0] > 5)) || ((kurs[1] === 2) && (kurs[0] > 7))
	       || ((kurs[1] === 3) && (kurs[0] > 10)) || ((kurs[1] === 4) && (kurs[0] > 13)) || (kurs[0] > 14)
}

/**
 * Sortiert AG anhand des Kurses in die Spielreihenfolge
 * @param {Object} ag Die AG
 * @param {String} name Der agName
 */
function sortierAGInSpielrunde(ag, name) {
	const wert = gibAktienWert(ag)
	const i = data.dran.findIndex( b => {
		b = data.ags[b]
		const bWert = gibAktienWert(b)
		return (bWert < wert) || ((bWert == wert) && ((b.kurs[0] < ag.kurs[0])
		                                              || ((b.kurs[0] == ag.kurs[0]) && (b.kurs[2] > ag.kurs[2]))))
	} )
	if (i < 0)
		data.dran.push(name)
	else
		data.dran.splice(i, 0, name)
}

/**
 * Erzeugt die Reihenfolge der AGs für eine neue Bertiebsrunde
 */
function erzeugAGReihenfolge() {
	for (let n in data.ags) {
		const ag = data.ags[n]
		if (ag.betrieb)
			sortierAGInSpielrunde(ag, n)
	}
}

/**
 * Ermittelt die nächste spielende Bahn / Person
 * @param {String} dran Die aktuell spielende Bahn
 * @returns ob es eine neue Betriebsrunde gibt
 */
function wählNächste(dran) {
	let neueRunde
	data.dran.shift() // nächste aus Reihenfolge
	if (data.dran[0] || (data.dran[0] === 0))
		return false
	do {
		if ((data.phase < 2) && (neueRunde || !dran || dran.startsWith("v")))
			for (let i = dran && Number(dran[1]) + 1 || 1; i <= data.vorPR.length; ++i) { // nächste Vorpreußen
				dran = "v" + i
				if (Object.values(data.spieler).some( s => s.papiere.includes(dran) )) {
					data.dran.push(dran)
					break
				}
			}
		if (!data.dran[0] && (neueRunde || !data.ags[dran])) // AG Reihenfolge ermitteln
			erzeugAGReihenfolge()
		if (!data.dran[0]) { // neue Runde?
			if (Math.abs(data.runde) < data.phase + (data.wechsel ? 0 : 1))
				neueRunde = true
			else
				data.dran.push(data.aktienStart) // Startspieler ist nächste
		}
	} while (neueRunde && !data.dran[0])
	return neueRunde
}

function prüfUndGibLoksAus(typ, aktionen) {
	const lok = this.lok
	if (lok && ((typ === "2") || (typ === (lok.length > 1 ? lok[0] : (Number(lok[0]) - 1) + "+")))
	    && this.bank.loks.reduce( (r, l) => l === typ ? ++r : r , 0) === 0) {
		for (let i = 0; i < LOKS[lok].zahl; ++i)
			this.bank.loks.push(lok)
		if (lok !== "6+")
			this.lok = lok.length > 1 ? (Number(lok[0]) + 1).toString() : lok + "+"
		else
			delete this.lok
		if (aktionen)
			aktionen.push({ loks: lok })
	}
}

function verschrottLoks(typ, aktionen) {
	const filter = l => l !== typ
	const löschen = b => b.loks = b.loks.filter(filter)
	this.vorPR.forEach(löschen)
	Object.values(this.ags).forEach(löschen)
	this.bank.loks = this.bank.loks.filter(filter)
	if (aktionen)
		aktionen.push({ art: 10, typ: typ })
}

function gibPRAustausch(was) {
	return was.startsWith("p") || (was === "v2") || (was === "v4") ? "PR2" : "PR"
}

function fügZugEin(log, wer, was) {
	const zugVomSelben = log.find( z => z.wer === wer )
	if (zugVomSelben)
		zugVomSelben.was.push(...was)
	else
		log.push({ wer, was, wann: gibWann() })
}

/**
 * Beginnt neue Runde indem bereits ausgeschüttete vorpreußische Gesellschaften vollends umgewandelt
 * und vorpreußische Gesellschaften zum Wandeln gefragt werden.
 * @param {Array<Spieler>} spieler Die Spieler
 * @param {Array<Züge>} log Die Züge
 */
function beginnNeueRunde(spieler, log) {
	if (data.phase >= 1) {
		if (data.ags.PR.betrieb) { // bereits ausgeschüttete vorpreußische Gesellschaften vollends umwandeln
			if (data.tausch) {
				data.tausch.forEach( p => Stand.wandelUmVorPR(p, spieler.findIndex( s => s.papiere.includes(p) ), true, data, log) )
				delete data.tausch
			}
			const wandelbareVorPR = Stand.gibWandelbareVorPR(spieler, data.runde < 0 ? data.dran[0] : data.aktienStart)
			if (wandelbareVorPR.length) {
				if (data.dran[0] >= 0)
					data.aktienStart = data.dran[0]
				data.dran = wandelbareVorPR.concat([-1])
				data.extra = [-1]
				data.tauschtPreußen = true
				data.preußenDir = data.ags.PR.direktor
			}
		} else if (data.vierPhase) {
			if (data.dran[0] >= 0)
				data.aktienStart = data.dran[0]
			data.dran = ["v2", -1]
			data.extra = [-1]
			data.tauschtPreußen = true
		}
	}
}

/**
 * Bereitet den Spielstand auf und schickt ihn an den server
 * @param {String} titel Der Titel eines Problems
 * @param {String} beschreibung Die Beschreibung eines Problems
 */
function sendeData(titel, beschreibung, zug) {
	const sendData = {
		utf8: "✓",
		authenticity_token: authenticity_token,
		stand: data
	}
	if (titel) {
		sendData.titel = titel
		sendData.beschreibung = beschreibung
	} else if (data.dran && data.dran.length) {
		const dran = data.dran[0]
		if (dran !== -1)
			sendData.to = Object.values(data.spieler)[dran >= 0 ? dran : Stand.gibDirektorNrFürBahn(dran)].token
	}
	const offeneZüge = data.offeneZüge
	const züge = offeneZüge ? offeneZüge.at(-1).was.extra ? [...offeneZüge.slice(0, -1), zug] : [...offeneZüge, zug] : [zug]
	if (zug) {
		if (offeneZüge && Object.keys(offeneZüge.at(-1).was)[0] >= 0)
			offeneZüge.at(-1).was.pop()
		if (data.rück)
			zug.was.push(data.rück)
		sendData.nr = data.letzteZugNr + 1
		sendData.stand = { zuege: züge }
	}
	JSON.send(sendData, standPfad, e => {
		if (e && (e.status !== undefined))
			data.offeneZüge = züge
		else {
			data.letzteZugNr += züge.length
			if (züge.at(-1).was.extra)
				--data.letzteZugNr
			delete data.offeneZüge
		}
		localStorage.setItem(STAND_ID, JSON.stringify(data))
	} )
}

/**
 * Schüttet Erträge der Privatbahnen aus.
 * @param {Array<Spieler>} spieler Die Spieler
 * @param {Array<Züge>} log Die Züge
 */
function schüttPrivatbahnenAus(spieler, log) {
	[5, 20, 25, 30, 10, 15].forEach( (wert, i) => {
		const bahn = "p" + (i + 1)
		const an = spieler.findIndex( s => s.papiere.includes(bahn) )
		if (an >= 0) {
			data.bank.geld -= wert
			spieler[an].geld += wert
			data.privat[i].wert += gewinn // Wertsteigerung durch Ausschüttung für Verlauf
			fügZugEin(log, bahn, [{ rente: wert, an: an }])
		}
	} )
}

/**
 * returns ob der Kauf die erste Lok eines Typs ist
 * @param {String} typ Der Typ
 */
function istErsteLok(typ) {
	return data.bank.loks.reduce( (r, l) => r += l === typ ? 1 : 0 , 0) === LOKS[typ].zahl
}

function setzVerlauf() {
	data.tabelle.runde.push(data.runde)
	for (let bahn in BAHNEN) { // aktuellen Wert der Bahnen in Tabelle schreiben
		let wert = data.ags[bahn] ? data.ags[bahn].wert : bahn.startsWith("p") ? data.privat[bahn[1] - 1] : data.vorPR[bahn[1] - 1].wert
		if (!data.ags[bahn])
			wert += gibPapierWert(bahn)
		let ag = bahn.slice(0, 2)
		let teil = 1
		switch (bahn) {
			case "p1":
			case "p5":
			case "p6":
				ag = "BY"
				wert -= gibPapierWert(ag)
				break
			case "p2":
				ag = "SX"
				teil = 2
				wert -= gibPapierWert("SX2")
				break
			case "p3":
			case "p4":
			case "v1":
			case "v2":
			case "v3":
			case "v4":
			case "v5":
			case "v6":
				ag = "PR"
				if (bahn.startsWith("v") && (bahn !== "v2") && (bahn !== "v4"))
					teil = .5
				wert -= teil * gibPapierWert("PR2")
		}
		wert += teil * gibAktienWert(data.ags[ag])
		data.tabelle[bahn].push(wert)
	}
	for (let n in data.spieler) { // aktuelles Vermögen der Spieler in Tabelle schreiben
		const spieler = data.spieler[n]
		let wert = spieler.geld
		for (let bahn of spieler.papiere) {
			if (!data.ags[bahn.slice(0, 2)])
				wert += gibPapierWert(bahn)
			switch (bahn) {
				case "p1":
				case "p5":
				case "p6": wert -= gibPapierWert("BY"); break
				case "p2": wert -= gibPapierWert("SX2")
			}
		}
		const aktien = gibAktienpackete(spieler.papiere, data.ags)
		for (let ag in aktien)
			wert += aktien[ag] * gibAktienWert(data.ags[ag])
		data.tabelle[n].push(wert)
	}
}

export class Stand
{
	static get doppelPR() {
		return ["p3", "p4", "v2", "v4"]
	}

	static set aktualisierStand(funktion) {
		aktualisierStand = funktion
	}

	static gibSpielerNr(name) {
		return Object.keys(data.spieler).findIndex( s => s === name )
	}
	
	/**
	 * Ermittelt Nummer des Direktors für einen
	 * gegebenen Identifier einer Bahn
	 * @param {String} inBahnName: Identifier der Bahn
	 * @returns {Number} Nummer des Spielers
	 */
	static gibDirektorNrFürBahn( inBahnName ) {
		const spieler = Object.entries(data.spieler)
		const spielerNr = data.ags[ inBahnName ]
		                  ? (inBahnName === "PR") && (data.preußenDir >= 0) ? data.preußenDir : data.ags[ inBahnName ].direktor
		                  : spieler.findIndex( s => s[1].papiere.includes(inBahnName) )
		return spielerNr
	}

	static wandelUmVorPR(was, wer, darfKassieren, stand, log) {
		if (darfKassieren) {
			const papiere = Object.values(stand.spieler)[wer].papiere
			Stand.gibAbPapier(papiere, was)
			Stand.nimmAufPapier(papiere, gibPRAustausch(was))
		} else if (stand.tausch) {
			if (!stand.tausch.includes(was))
				stand.tausch.push(was)
		} else
			stand.tausch = [was]
		if (was.startsWith("v")) {
			const v = Number(was[1])
			const bahn = stand.vorPR[v - 1]
			const pr = stand.ags.PR
			pr.geld += bahn.geld
			bahn.geld = 0
			pr.loks.push(...bahn.loks)
			bahn.loks.splice(0)
			const i = stand.bhfe.findIndex( b => b.bahn === was )
			if (i >= 0) {
				if (!SCHLESIEN && (v === 5))
					stand.bhfe.splice(i, 1)
				else
					stand.bhfe[i].bahn = "PR"
			}
			if (!pr.betrieb && (v === 2)) {
				pr.direktor = wer
				pr.betrieb = true
				if (log && darfKassieren)
					fügZugEin(log, "PR", [{ art: 5, geld: pr.geld }])
			}
		}
	}
	
	/**
	 * @param {Object} stand Der Spielstand
	 * @param {string} dran Die Bahn die dran ist
	 * @param {Array<Spieler>} spieler Die Spieler
	 * @returns {Array<string>} Liefert die Liste der Bahnnamen (ohne dran), die überzählige Loks abgeben müssen.
	 */
	static gibBahnenMitÜberzähligenLoks(stand, dran, spieler) {
		let i = 0
		return stand.vorPR.reduce( (r, b) => {
			b = "v" + ++i
			return (b !== dran) && spieler.some( s => s.papiere.includes(b) ) && (stand.gibLokrest(i) < 0) ? r.concat([b]) : r
		} , []).concat(Object.keys(stand.ags).filter( b => (b !== dran) && (stand.gibLokrest(b) < 0) ))
	}
	
	/**
	 * @param {string} bahn Die Bahn
	 * @returns {Boolean} Entscheidet, ob angegebene Bahn eine vorpreußische Gesellschaft ist.
	 */
	static istVorpreußische(bahn) {
		return bahn.startsWith("v") || ["p3", "p4"].includes(bahn)
	}
	
	static kaufLok(typ, geld, von, stand, aktionen, log, extraDran) {
		const dran = extraDran || (stand === data ? data.dran[0] : stand.dran)
		const bahn = (stand.tausch && stand.tausch.includes(dran)) ? stand.ags.PR : (stand.ags[dran] || stand.vorPR[dran[1] - 1])
		bahn.geld -= geld
		bahn.loks.push(typ)
		const bank = stand.bank
		const spieler = Object.values(stand.spieler)
		if (bahn.geld < 0) {
			const direktor = spieler[bahn.direktor]
			direktor.geld += bahn.geld
			bahn.geld = 0
		}
		let kauftErsteLok
		if (von) {
			const vonBahn = von.startsWith("v") ? stand.vorPR[von[1] - 1] : stand.ags[von]
			vonBahn.loks.splice(vonBahn.loks.indexOf(typ), 1)
			vonBahn.geld += geld
		} else {
			if (istErsteLok(typ))
				kauftErsteLok = true
			bank.loks.splice(bank.loks.indexOf(typ), 1)
			bank.geld += geld
			prüfUndGibLoksAus.call(stand, typ, aktionen)
			if ((dran === "SX") && (bahn.loks.length === 1)) {
				const hatP2 = spieler[bahn.direktor]
				if (hatP2.papiere.includes("p2")) {
					Stand.gibAbPapier(hatP2.papiere, "p2")
					if (log)
						fügZugEin(log, "p2", [{ ende: 1 }])
				}
			}
		}
		if (kauftErsteLok)
			switch (typ) {
				case "3":
					stand.phase = 1
					stand.wechsel = true
					return
				case "4":
					stand.vierPhase = true // Zwischenphase für Vorpreußen
					verschrottLoks.call(stand, "2", aktionen)
					return true
				case "4+":
					verschrottLoks.call(stand, "2+", aktionen)
					const wer = Stand.gibDirektorNrFürBahn("v2")
					if (wer >= 0) {
						Stand.wandelUmVorPR("v2", wer, dran && (2 > dran[1]), stand, log)
						Stand.prüfUndÄnderDirektor(stand.ags.PR, spieler, "PR", stand, log, aktionen, true)
						stand.preußenDir = wer
						if (aktionen)
							aktionen.push({ art: 12 })
						if (log)
							fügZugEin(log, "PR", [{ art: 5, geld : stand.ags.PR.geld }])
						return true
					}
					return
				case "5":
					stand.phase = 2
					delete data.vierPhase
					stand.wechsel = true
					let vorPRUmgewandelt
					spieler.forEach( (s, i) => { // Privatbahnen schließen
						s.papiere.slice().forEach( p => {
							if (Stand.istVorpreußische(p)) {
								if (log && (p === "v2"))
									stand.preußenDir = Stand.gibDirektorNrFürBahn("v2")
								Stand.wandelUmVorPR(p, i, dran && !p.startsWith("p") && (p[1] > dran[1]), stand, log)
								vorPRUmgewandelt = true
							} else if (p.startsWith("p"))
								Stand.gibAbPapier(s.papiere, p)
						} )
					} )
					const spielerÜberLimit = Stand.prüfUndÄnderDirektor(stand.ags.PR, spieler, "PR", stand, log, aktionen)
					if (aktionen) {
						aktionen.push({ art: 13 })
						if (vorPRUmgewandelt)
							aktionen.push({ art: 14 })
					}
					return spielerÜberLimit >= 0 ? spielerÜberLimit : true
				case "6": verschrottLoks.call(stand, "3", aktionen); break
				case "6+": verschrottLoks.call(stand, "3+", aktionen)
			}
	}

	static gibAbLok(aktion, stand) {
		const wer = aktion.von || (stand === data ? data.dran[0] : stand.dran)
		let loks = (stand.vorPR[wer[1] - 1] || stand.ags[wer]).loks
		const lok = aktion.lok
		loks.splice(loks.indexOf(lok), 1)
		loks = stand.bank.loks
		const i = loks.indexOf(lok)
		if (i < 0)
			loks.push(lok)
		else
			loks.splice(i, 0, lok)
	}

	static gibAbPapier(wer, was) {
		const i = wer.indexOf(was)
		if (i >= 0)
			wer.splice(i, 1)
	}
	
	static nimmAufPapier(wer, was) {
		let i
		if (was.length > 2)
			i = wer.findIndex( p => p.startsWith(was.substr(0, 2)) )
		else {
			let j = 0
			let k
			i = wer.findIndex( p => {
				j += 1
				if (p.startsWith(was))
					k = j
				return p === was
			})
			if (i < 0)
				i = k
		}
		if (i >= 0)
			wer.splice(i, 0, was)
		else
			wer.push(was)
	}

	static prüfUndÄnderDirektor(ag, spieler, agName, stand, log, aktionen, warteMitDoppelTausch) {
		if (ag && ((ag.direktor >= 0) || data.tauschtPreußen && ((agName === "PR")))) {
			const direktorNr = data.tauschtPreußen && (agName === "PR") ? stand.preußenDir : ag.direktor // Preußendirektor könnte gewechselt haben
			const direktor = spieler[direktorNr]
			let direktorNeu = -1
			let max = 0
			for (let i = 0, e = spieler.length; i < e; ++i) {
				const anzahl = spieler[i].papiere.reduce((r, p) => {
					if (stand.tausch && (agName === "PR") && stand.tausch.includes(p))
						p = gibPRAustausch(p)
					return p.startsWith(agName) ? r + ((p.length > 2) ? 2 : 1) : r
				}, 0)
				if (anzahl > max) {
					direktorNeu = i
					max = anzahl
				} else if ((anzahl === max) && (i === direktorNr))
					direktorNeu = i
			}
			if (direktorNeu != direktorNr) {
				ag.direktor = direktorNeu
				if (log)
					fügZugEin(log, agName, [{ direktor: direktorNeu }])
				if (!warteMitDoppelTausch) {
					const neu = spieler[direktorNeu].papiere
					const doppel = agName + "2"
					const istPR = agName === "PR"
					if (!neu.some( p => (p === doppel) || (istPR && Stand.doppelPR.includes(p) && stand.tausch && stand.tausch.includes(p)) )) {
						//  Doppelaktie gegen zwei einzelne tauschen
						const alt = direktor.papiere
						const von = istPR || alt.includes(doppel) ? alt : stand.bank.pool
						const komplett = neu.filter( p => p === agName ).length > 1 // vollständig getauschte 5%er
						if (istPR && von.includes("v2")) { // Berlin-Potsdamer muss getauscht werden
							if (komplett) { // gegen komplett getauschte 5% vorPR tauschen
								const getauschte = ["v1", "v3", "v5", "v6"].filter( b => !Object.values(stand.spieler).some( s => s.papiere.includes(b) ) )
								Stand.gibAbPapier(von, "v2")
								Stand.nimmAufPapier(neu, doppel)
								let tausch = getauschte.shift()
								Stand.gibAbPapier(neu, agName)
								Stand.nimmAufPapier(von, tausch)
								stand.tausch.push(tausch)
								tausch = getauschte.shift()
								Stand.gibAbPapier(neu, agName)
								Stand.nimmAufPapier(von, tausch)
								stand.tausch.push(tausch)
							} else { // gegen nicht komplett getauschte 5% vorPR tauschen
								Stand.gibAbPapier(von, "v2")
								Stand.nimmAufPapier(neu, "v2")
								let tausch = neu.find( p => !Stand.doppelPR.includes(p) && stand.tausch.includes(p) )
								Stand.gibAbPapier(neu, tausch)
								Stand.nimmAufPapier(von, tausch)
								tausch = neu.find( p => !Stand.doppelPR.includes(p) && stand.tausch.includes(p) )
								Stand.gibAbPapier(neu, tausch)
								Stand.nimmAufPapier(von, tausch)
							}
						} else if (!istPR || komplett) { // normal tauschen
							Stand.gibAbPapier(von, doppel)
							Stand.nimmAufPapier(neu, doppel)
							Stand.gibAbPapier(neu, agName)
							Stand.nimmAufPapier(von, agName)
							Stand.gibAbPapier(neu, agName)
							Stand.nimmAufPapier(von, agName)
						} else { // gegen nicht komplett getauschte 5% vorPR tauschen
							Stand.gibAbPapier(von, doppel)
							Stand.nimmAufPapier(neu, "v2")
							stand.tausch.push("v2")
							Stand.gibAbPapier(neu, neu.find( p => !Stand.doppelPR.includes(p) && stand.tausch.includes(p) ))
							Stand.nimmAufPapier(von, agName)
							Stand.gibAbPapier(neu, neu.find( p => !Stand.doppelPR.includes(p) && stand.tausch.includes(p) ))
							Stand.nimmAufPapier(von, agName)
						}
						if (aktionen && (direktorNr === data.dran[0]))
							aktionen.push({ art: 11 })
						else if (log)
							fügZugEin(log, direktorNr, [{ art: 11 }])
						if (alt.length > gibPapierlimit(Object.keys(stand.spieler)[direktorNr], stand)) { // Zwangsverkauf
							return direktorNr
						}
					}
				}
			}
		}
	}

	/**
	 * @param {int} spielerzahl Die Anzahl der Mitspielenden
	 * @returns Betrag des Startkapitals für einen Spieler
	 */
	static gibStartGeld(spielerzahl) {
		switch (spielerzahl) {
			case 3: return 600
			case 4: return 475
			case 5: return 390
			case 6: return 340
			case 7: return 310
		}
	}

	/**
	 * Verschiebt Aktienkurs
	 * @param {Object} kurs Der Kurs
	 * @param {0|1} i Ob in x oder y Richtung
	 * @param {-1|1} wie In welche Richtung verschobem werden soll
	 * @param {Object} ags Die Ags
	 */
	static verschiebKurs(kurs, i, wie, ags) {
		let verschiebt = false
		if (i < 1) {
			if (wie > 0) { // rechts
				if (istAmPfeilHoch(kurs)) {
					if (kurs[1] < 6)
						i = 1
				} else if (kurs[0] < 15)
					verschiebt = true
			} else { // links
				if ((kurs[0] === 0) || ((kurs[1] === 5) && (kurs[0] < 3)) || ((kurs[1] === 6) && (kurs[0] < 5))) {
					if (kurs[1] > 0)
						i = 1
				} else if (kurs[0] > 0)
					verschiebt = true
			}
		}
		if (i > 0) {
			if (wie > 0) { // hoch
				if ((kurs[1] < 4) || ((kurs[0] > 1) && (kurs[1] < 5)) || ((kurs[0] > 3) && (kurs[1] < 6)))
					verschiebt = true
			} else if ((kurs[1] > 5) || ((kurs[0] < 15) && (kurs[1] > 4)) || ((kurs[0] < 12) && (kurs[1] > 3)) // runter
				       || ((kurs[0] < 9) && (kurs[1] > 2)) || ((kurs[0] < 7) && (kurs[1] > 1)) || ((kurs[0] < 5) && (kurs[1] > 0)))
					verschiebt = true
		}
		if (verschiebt) {
			let einzel
			let gleiche = 0
			for (let n in ags) {
				const ag = ags[n]
				if (ag.betrieb && (ag.kurs !== kurs) && (ag.kurs[0] === kurs[0]) && (ag.kurs[1] === kurs[1])) {
					if (!einzel)
						einzel = ag.kurs
					ag.kurs[2] -= 1
					gleiche += 1
				}
			}
			if (einzel && (gleiche === 1))
				einzel.length = 2
			kurs.length = 2
			kurs[i] += wie
			gleiche = 0
			for (let n in ags) {
				const ag = ags[n]
				if (ag.betrieb && (ag.kurs !== kurs) && (ag.kurs[0] === kurs[0]) && (ag.kurs[1] === kurs[1])) {
					if (ag.kurs.length < 3)
						ag.kurs[2] = gleiche
					gleiche += 1
				}
			}
			if (gleiche > 0)
				kurs[2] = gleiche
		}
		return verschiebt
	}

	/**
	 * Sperrt Aktien einer AG für diese Runde vom Rückkauf
	 * @param {string}  Die AG
	 * @param {Object}  Die Spielerinfo
	 */
	static sperrAGFürSpieler(ag, info) {
		const sperre = info.sperre
		if (sperre) {
			if (!sperre.includes(ag))
				sperre.push(ag)
		} else
			info.sperre = [ag]
	}

	/**
	 * @param {Array<Spieler>} spieler Die Spieler
	 * @param {int} start Nummer des Startspielers
	 * @returns {Array<string>} Liefert die Namensliste der wandelbaren vorpreußischen Gesellschaften.
	 */
	static gibWandelbareVorPR(spieler, start) {
		return spieler.slice(start).concat(spieler.slice(0, start))
		       .reduce( (r, s) => r = r.concat(s.papiere.filter( p => Stand.istVorpreußische(p) )) , [])
	}
	
	/**
	 * Lässt Spieler bankrott gehen
	 * @param {int} nr Nummer des Spielers
	 * @param {Object} stand Der Spielstand
	 * @param {Array<Züge>} log Die Züge
	 */
	static gehBankrott(nr, stand, log) {
		const spieler = Object.values(stand.spieler)
		const wer = spieler[nr]
		stand.bank.geld += wer.geld
		wer.geld = -1
		const pool = stand.bank.pool
		let anteile
		const vorpreußische = []
		while (wer.papiere.length) { // alle Papiere abgeben 
			const papier = wer.papiere.pop()
			if (stand.ags[papier.substring(0, 2)])
				Stand.nimmAufPapier(pool, papier)
			else if (stand.tausch && stand.tausch.includes(papier)) {
				stand.tausch.splice(stand.tausch.indexOf(papier), 1)
				Stand.nimmAufPapier(pool, Stand.doppelPR.includes(papier) ? "PR2" : "PR")
			} else if (["p3", "p4"].includes(papier))
				Stand.nimmAufPapier(pool, "PR2")
			else if (papier.startsWith("v"))
				vorpreußische.push(papier)
			const ag = papier.substr(0, 2)
			if (stand.ags[ag] && (stand.ags[ag].direktor === nr)) { // Direktorenwechsel vornehmen
				if (!anteile)
					anteile = []
				let direktor
				let max = 0
				spieler.forEach( (s, i) => {
					if (i !== nr) {
						if (!anteile[i])
							anteile[i] = gibAktienpackete(s.papiere, stand.ags)
						const teil = anteile[i][ag]
						if ((teil > max) || ((teil === max) && (direktor < nr))) {
							max = teil
							direktor = i
						}
					}
				} )
				stand.ags[ag].direktor = direktor || 0 // wenn niemand sonst Aktien hat, übernimmt Startspieler
				if (log)
					fügZugEin(log, ag, [{ direktor: direktor }])
			}
		}
		wer.papiere.push(...vorpreußische)
		if (nr === stand.aktienStart) { // neuen Aktienrundenstartspieler suchen
			stand.aktienStart = (nr + 1) % spieler.length
			while (spieler[stand.aktienStart].geld < 0) // Bankrotte Spieler überspringen
				stand.aktienStart = ++stand.aktienStart % spieler.length
		}
	}
	
	/**
	 * Setzt Daten für Extrazug
	 * @param {Array} extra Die Daten des Extrazuges
	 */
	static setzExtra(extra, wer, dann) {
		data.extra = extra
		sendeData(null, null, { wer, was: { extra }, dann })
	}
	
	/**
	 * Setzt Daten für Extrazugrücknahmen
	 * @param {Object} extra Die Date des Extrazuges
	 */
	static setzRück(rück) {
		if (Object.keys(rück).length)
			data.rück = rück
		else
			delete data.rück
		localStorage.setItem(STAND_ID, JSON.stringify(data))
	}
	
	/**
	 * Behandelt Extrazüge
	 * @param {Array<Aktion>} zug Die Aktionen des Zuges
	 * @param {Array<String | int>} wer Zugreihenfolge der Extrazüge
	 * @param {Boolean} intern Ob es ein interner Zug ist
	 * @param {plan} plan Der Spielplan
	 */
	static ziehExtra(zug, wer, intern, plan) {
		let extra = data.extra
		const dran = data.dran[0]
		if (wer) { // Extrazug beginnen
			data.extra = extra = [dran, ...zug]
			data.dran = wer.concat(data.dran)
			const lokZug = zug.findLast( z => z.lok )
			const typ = lokZug && lokZug.lok
			if (typ && ["4", "4+", "5"].includes(typ) && istErsteLok(typ)) // Preußische können umgewandelt werden
				data.tauschtPreußen = true
		} else { // fortsetzen
			extra.push(...zug)
			data.dran.shift()
			if (data.tauschtPreußen && (((extra[0] < 0) && (data.dran.length === 1))
			                            || ((data.dran[0] === extra[0]) && !data.dran.includes(data.dran[0], 1)))) {
				const spieler = Object.values(plan.spieler)
				if (zug[0] && zug[0].ende && (zug[0].wer === "v2")) { // Berlin-Potsdamer eröffnet Preußen
					const start = Stand.gibDirektorNrFürBahn("v2")
					const bahnen = Stand.gibWandelbareVorPR(spieler, start)
					bahnen.splice(bahnen.indexOf("v2"), 1)
					data.dran = bahnen.concat(data.dran)
					data.preußenDir = start
				} else { // Tauschrunde Preußenpapiere beendet
					if (zug[0] && zug[0].ende)
						Stand.wandelUmVorPR(zug[0].wer, Stand.gibDirektorNrFürBahn(zug[0].wer), true, plan)
					let spielerÜberLimit
					if (plan.preußenDir >= 0) {
						spielerÜberLimit = Stand.prüfUndÄnderDirektor(data.ags.PR, spieler, "PR", plan)
						if (isNaN(spielerÜberLimit)
						    && (spieler[plan.preußenDir].papiere.length > gibPapierlimit(Object.keys(plan.spieler)[plan.preußenDir], plan)))
							spielerÜberLimit = plan.preußenDir
						delete plan.preußenDir
					}
					if ((spielerÜberLimit >= 0) && !extra.some( a => a.papier && (a.von === spielerÜberLimit) )) // nicht schon abgegeben
						data.dran.unshift(spielerÜberLimit)
					else {
						const loksAbzugeben = Stand.gibBahnenMitÜberzähligenLoks(plan, data.dran[0], spieler)
						if (loksAbzugeben.filter( b => !extra.some( a => a.lok && (a.bahn === b) ) ).length) // nicht schon abgegeben
							data.dran = loksAbzugeben.concat(data.dran)
						else {
							delete data.tauschtPreußen
							plan.tauschtPreußen = false
							if (plan.tausch)
								plan.tausch.splice(0)
							if (extra[0] < 0) { // normale Tauschrunde Preußenpapiere vor neuer Runde
								if (intern) {
									sendeData(null, null, { wer: dran, was: { extra, dran: data.dran }, dann: data.dran[0] })
									Verteiler.sendeExtraZug({ zug })
								}
								zug.splice(0)
								return Stand.zieh(extra.splice(1))
							}
						}
					}
				}
			} else if (zug.length && (zug.at(-1).art === 18)) { // Bankrott
				if (intern)
					Verteiler.sendeExtraZug({ zug })
				return Stand.zieh(zug)
			}
		}
		if (intern) {
			sendeData(null, null, { wer: dran, was: { extra, dran: data.dran }, dann: data.dran[0] })
			Verteiler.sendeExtraZug(wer ? { zug, wer } : { zug })
		}
		zug.splice(0)
		beiStandGeladen(data)
	}

	static zieh(zug, intern, wann) {
		const log = []
		const dran = data ? data.dran[0] === 0 ? 0 : data.dran[0] || -1 : -1
		const spieler = data ? Object.values(data.spieler || {}) : []
		const aktionen = []
		const spielAktionen = []
		const verkaufte = new Set()
		let bankGesprengt
		if (dran !== -1)
			fügZugEin(log, dran, aktionen)
		for (let aktion of zug) {
			aktionen.push(aktion)
			const geld = aktion.geld
			if (aktion.spieler) { // von (Start-)Stand übernehmen
				if (aktion.bank) {
					if (!data)
						data = gibStartStand()
					const dran = aktion.dran
					Object.assign(data, aktion)
					data.dran = []
					if ((dran >= 0) || (dran.startsWith && dran.startsWith("v")))
						data.dran.push(dran)
					else {
						erzeugAGReihenfolge()
						if (isNaN(dran)) {
							let i = data.dran.indexOf(dran)
							if (i >= 0) {
								const ag = data.ags[dran]
								const kurs = ag.kurs
								if ((kurs[0] < 15) && !istAmPfeilHoch(kurs)) {
									const alterKurs = kurs.concat()
									++kurs[0] // uninteressant, da von oben ist immer kleiner 
									while (i > 0) {
										const vorigeAG = data.dran[i - 1]
										if (gibAktienWert(ag) >= gibAktienWert(data.ags[vorigeAG])) {
											if (confirm(`War ${BAHNEN[vorigeAG]} schon dran?`))
												break
										} else
											break
										--i
									}
									data.dran.splice(0, i)
									ag.kurs = alterKurs
								}
							}
						}
					}
					data.aktienStart = data.aktienStart >= 0 ? data.aktienStart : dran >= 0 ? -1 : 0
					fügZugEin(log, -1, [aktion])
				} else {
					data = gibStartStand()
					data.bank.geld = STARTGELD
					data.spieler = {}
					starteNeuesSpiel(aktion)
				}
			} else if (aktion.papier) { // Papier(e) ver-/kaufen
				const agName = aktion.papier.substr(0, 2)
				const ag = data.ags[agName]
				let wer = dran
				let von
				let an
				if (geld > 0) { // verkaufen
					if (aktion.von >= 0) {
						wer = aktion.von
						delete aktion.von // TODO braucht das der Zug nicht?
						fügZugEin(log, wer, aktionen.splice(aktionen.indexOf(aktion), 1))
					}
					data.bank.geld -= geld
					if (data.bank.geld < 0)
						bankGesprengt = true
					von = spieler[wer].papiere
					an = data.bank.pool
					const kurs = ag.kurs
					let p = Number.MAX_VALUE
					if (!verkaufte.has(agName) && Stand.verschiebKurs(kurs, 1, -1, data.ags))
						fügZugEin(log, agName, [{ art: 9 }])
					verkaufte.add(agName)
					if (wer === dran)
						Stand.sperrAGFürSpieler(agName, spieler[dran])
				} else if (aktion.von >= 0) { // verstaatlichen
					spieler[aktion.von].geld -= geld
					von = spieler[aktion.von].papiere
					an = spieler[dran].papiere
				} else { // kaufen
					if (aktion.von || (aktion.papier[0] === "p"))
						data.bank.geld -= geld
					else if (aktion.papier.startsWith("v"))
						data.vorPR[aktion.papier[1] - 1].geld -= geld
					else {
						if (geld)
							ag.geld -= geld
						if ((aktion.papier.length > 2) && isNaN(ag.direktor) && (aktion.papier !== "PR2")) {
							ag.direktor = dran // Direktor setzen
							if (aktion.papier === "BA2") // Preußenaktien ausgeben
								gibAktienAus("PR", spielAktionen)
						}
					}
					von = data.bank[aktion.von || "aktien"]
					an = spieler[dran].papiere
				}
				if (geld)
					spieler[wer].geld += geld
				Stand.gibAbPapier(von, aktion.papier)
				Stand.nimmAufPapier(an, aktion.papier)
				if (ag && ag.direktor >= 0)
					Stand.prüfUndÄnderDirektor(ag, spieler, agName, data, log, aktionen)
				if ((data.runde === 0) && !aktion.von && (aktion.von !== 0) && (geld < 0)) { // neue Aktienpakete ausgeben
					if (["BA", "WT", "MS"].includes(agName) && data.bank.aktien.reduce( (r, a) => r += a.startsWith(agName) ? 1 : 0 , 0) == 4) {
						switch (agName) {
							case "BA": gibAktienAus("WT", spielAktionen)
								break
							case "WT": gibAktienAus("HE", spielAktionen)
								break
							case "MS": gibAktienAus("OL", spielAktionen)
						}
					} else if (["BY", "SX"].includes(agName) && !data.bank.aktien.some( a => ["BY", "SX"].includes(a.substr(0, 2)) ))
						gibAktienAus("BA", spielAktionen)
					else if (["BA", "WT", "HE"].includes(agName) && !data.bank.aktien.some( a => ["BA", "WT", "HE"].includes(a.substr(0, 2)) ))
						gibAktienAus("MS", spielAktionen)
				}
	
			} else if (aktion.fährt) { // Ertrag für gefahrenes Geld verteilen
				data.bank.geld -= geld || 0
				if (data.bank.geld < 0)
					bankGesprengt = true
				if (dran.startsWith("v")) {
					const gewinn = geld / 2
					const an = Object.entries(data.spieler).find( e => e[1].papiere.includes(dran) )[0]
					const bahn = data.vorPR[dran[1] - 1]
					bahn.geld += gewinn
					bahn.wert += gewinn // Wertsteigerung durch Ausschüttung für Verlauf
					data.spieler[an].geld += gewinn
					aktionen.push({ rente: gewinn, an: dran })
					aktionen.push({ rente: gewinn, an: Object.keys(data.spieler).indexOf(an) })
				} else if (aktion.einbehalt || !geld) {
					if (geld)
						data.ags[dran].geld += geld
					else
						aktionen.pop()
					if (Stand.verschiebKurs(data.ags[dran].kurs, 0, -1, data.ags))
						aktionen.push({ art: 7 })
				} else {
					data.spieler[dran] = {papiere: data.bank.pool}
					for (let name in data.spieler) {
						const faktor = gibAktienpackete(data.spieler[name].papiere, data.ags)[dran] / 10
						if (faktor > 0) {
							const gewinn = Math.round(geld * faktor)
							if (data.ags[name])
								data.ags[name].geld += gewinn
							else {
								data.spieler[name].geld += gewinn
								name = Object.keys(data.spieler).indexOf(name)
							}
							aktionen.push({ rente: gewinn, an: name })
						}
					}
					delete data.spieler[dran]
					const bahn = data.ags[dran]
					bahn.wert += gewinn // Wertsteigerung durch Ausschüttung für Verlauf
					if (Stand.verschiebKurs(bahn.kurs, 0, 1, data.ags))
						aktionen.push({ art: 8 })
				}
	
			} else if (aktion.lok) {
				if (aktion.von && !aktion.geld) {
					fügZugEin(log, aktion.von, [aktion])
					Stand.gibAbLok(aktionen.splice(aktionen.indexOf(aktion), 1)[0], data)
					delete aktion.von
				} else if (aktion.geld)
					Stand.kaufLok(aktion.lok, aktion.geld, aktion.von, data, spielAktionen, log)
				else
					Stand.gibAbLok(aktion, data)
	
			} else if (aktion.art === 17) {
				Stand.gehBankrott(aktion.wer, data, log)
				fügZugEin(log, aktion.wer, aktionen.splice(aktionen.indexOf(aktion), 1))
				
			} else if (aktion.ende) { // Vorpreußen umwandeln
				const wer = aktion.wer
				aktionen.splice(aktionen.indexOf(aktion), 1)
				fügZugEin(log, wer, [{ ende: aktion.ende }])
				Stand.wandelUmVorPR(wer, spieler.findIndex( s => s.papiere.includes(wer) ),
				                    (data.extra && (data.extra[0] < 0)) || (!wer.startsWith("p") && (wer[1] > dran[1])), data, log)
				if (zug.filter( a => a.ende ).at(-1) === aktion)
					Stand.prüfUndÄnderDirektor(data.ags.PR, spieler, "PR", data, log, aktionen)
				delete aktion.wer

			} else { // Bahnhöfe und Gleise bauen
				if (geld) {
					(data.ags[dran] || data.vorPR[dran[1] - 1]).geld -= geld
					data.bank.geld += geld
				}
				if (aktion.bhf) {
					data.bhfe.push(aktion.bhf)
					if (aktion.bhf.bahn !== dran) {
						fügZugEin(log, aktion.bhf.bahn, aktionen.splice(aktionen.indexOf(aktion), 1))
					}
					if (aktion.bhf.extra === "p1") {
						Stand.gibAbPapier(spieler.find( s => s.papiere.includes("p1") ).papiere, "p1")
						fügZugEin(log, "p1", [{ ende: 1 }])
					}
					if (aktion.bhf.extra === "p6") {
						Stand.gibAbPapier(spieler.find( s => s.papiere.includes("p6") ).papiere, "p6")
						fügZugEin(log, "p6", [{ ende: 1 }])
					}
				} else {
					const i = data.gleise.findIndex( g => (g.x === aktion.gleis.x) && (g.y === aktion.gleis.y) )
					if (i >= 0)
						data.gleise.splice(i, 1)
					if ((aktion.gleis.y === 12) && [7, 8].includes(aktion.gleis.x)
						&& data.gleise.some( g => (g.y === 12) && [7, 8].includes(g.x) )) {
						Stand.gibAbPapier(spieler.find( s => s.papiere.includes("p5") ).papiere, "p5")
						fügZugEin(log, "p5", [{ ende: 1 }])
					}
					data.gleise.push(aktion.gleis)
				}
			}
		}
		
		delete data.extra
		delete data.preußenDir
		let neueRunde
		if (dran < 0) { // Ende Tauschrunde Preußenpapiere vor neuer Runde
			if (!zug[0] || !zug[0].spieler) {
				if (data.runde > 0) {
					wählNächste()
					schüttPrivatbahnenAus(spieler, log)
				} else {
					data.dran = [data.aktienStart]
					data.aktienStart = -1
				}
			}
		} else if (dran >= 0) { // nächster Spieler
			if (!zug.length && (data.aktienStart < 0))
				data.aktienStart = dran
			if (data.clemens) {
				if (dran)
					data.dran[0] = dran - 1
				else
					data.clemens = 0
			} else
				data.dran[0] = (dran + 1) % spieler.length
			while (spieler[data.dran[0]].geld < 0) { // Bankrotte Spieler überspringen
				data.dran[0] = ++data.dran[0] % spieler.length
			}
			while (spieler[data.dran[0]].passt && (data.dran[0] !== data.aktienStart)) { // Automatisch passen
				fügZugEin(log, data.dran[0], [])
				Verteiler.sendeZug([])
				if (data.aktienStart < 0)
					data.aktienStart = data.dran[0]
				data.dran[0] = ++data.dran[0] % spieler.length
			}
			if (zug.some( a => a.papier && a.geld <= 0 )) // letzter Spieler hat gekauft
				data.aktienStart = -1
			else if (data.dran[0] === data.aktienStart) { // letzter Spieler passt (kein Kauf), neue Betriebsrunde
				wählNächste()
				if (!data.dran[0].startsWith || !data.dran[0].startsWith("p"))
					neueRunde = true
			}
		} else // nächste Bahn
			neueRunde = wählNächste(dran)

		if (!data.ende && bankGesprengt) { // Spielende einleiten
			spielAktionen.push({ art: 15 })
			data.ende = true
		}
		
		if (data.ende && data.runde && (neueRunde || (data.dran[0] >= 0))) { // Spielende
			setzVerlauf()
			data.bank.loks.splice(0)
			for (let p of ["runde", "phase", "lok", "dran", "aktienStart", "ende"])
				delete data[p]
			spielAktionen.push({ art: 16 })
		} else if (data.dran[0] >= 0) {
			if (data.runde < 0) {
				if (data.bank.aktien.length === 0) { // Startrunde beenden
					spielAktionen.push({ art: 1 }, { aktien: "BY" }, { aktien: "SX" })
					for (let i = 0; i < 5; ++i)
						data.bank.aktien.push("BY")
					for (let i = 0; i < 8; ++i)
						data.bank.aktien.push("SX")
					data.runde = 0
					delete data.clemens
				}
			} else if (data.runde > 0) { // neue Aktienrunde
				setzVerlauf()
				data.aktienStart = -1
				if (data.bank.aktien.some( p => (p[0] === "p") || (p[0] === "v") || (p === "BY2") )) { // Startrunde wieder aufnehmen
					spielAktionen.push({ art: 3 })
					data.runde = -1
				} else {
					delete data.wechsel
					beginnNeueRunde(spieler, log)
					spielAktionen.push({ art: 4 })
					data.runde = 0
				}
			}
		} else if (neueRunde) { // neue Betriebsrunde
			beginnNeueRunde(spieler, log)
			if (data.runde < 1) { // Aktienrunde beendet
				spieler.forEach( s => {
					delete s.sperre // Aktienkaufsperre aufheben
					delete s.passt // Automatisch weiterpassen aufheben
				} )
				if (data.lok === "2") {
					prüfUndGibLoksAus.call(data, data.lok, spielAktionen)
					for (let i = 1; i <= 6; ++i)
						data.bhfe.push({bahn: "v" + i, x: BHF_VORLAGE["v" + i].start[0], y: BHF_VORLAGE["v" + i].start[1], n: BHF_VORLAGE["v" + i].start[2]})
				}
				if (!data.bank.aktien.some( a => ["p1", "BY2", "p5", "p6"].includes(a) )) { // alle Bayernaktion des Startpakets verkauft
					for (let n in data.ags) {
						const ag = data.ags[n]
						if (ag.direktor >= 0) {
							if (!ag.betrieb) { // AG in Betrieb gehen lassen
								if (((data.runde > -1) || (n === "BY"))
								    && (data.bank.aktien.reduce( (r, a) => r += a === n + "2" ? 2 : a === n ? 1 : 0 , 0) <= 5)) {
									ag.betrieb = true

									// Prüfen, ob andere AGs den selben Kurs haben und "drunterlegen"
									let kursgleich
									Object.values(data.ags).forEach( a => {
										if ((a !== ag) && (a.kurs[0] === ag.kurs[0]) && (a.kurs[1] === ag.kurs[1]))
											if (!kursgleich || (kursgleich.kurs[2] > a.kurs[2]))
												kursgleich = a
									} )
									if (kursgleich) {
										if (kursgleich.kurs.length < 3)
											kursgleich.kurs[2] = 0
										ag.kurs[2] = kursgleich.kurs[2] + 1
									}

									if (!data.extra && (data.dran.length > 1))
										sortierAGInSpielrunde(ag, n)
									
									if (n === "BY") { // Bayern Startkapital durch Startrunde ergänzen
										data.bank.geld -= 276
										ag.geld += 276
									} else if (n === "SX") { // Sachsen Startkapital durch Startrunde ergänzen
										data.bank.geld -= 176
										ag.geld += 176
									}

									if (n !== "BA") // Heimatbahnhof legen
										data.bhfe.push({bahn: n, x: BHF_VORLAGE[n].start[0], y: BHF_VORLAGE[n].start[1], n: BHF_VORLAGE[n].start[2]})
									fügZugEin(log, n, [{ art: 5, geld: ag.geld }])
								}
							}
							if ((data.runde > -1) && !data.bank.aktien.some( a => a.startsWith(n) ) && !data.bank.pool.some( a => a.startsWith(n) )) {
								// vollständig verkaufte Aktien erhöhen
								if (Stand.verschiebKurs(ag.kurs, 1, 1, data.ags))
									fügZugEin(log, n, [{ art: 6 }])
							}
						}
					}
				}
			}
			setzVerlauf()
			if (data.runde < 0)
				data.runde = 1
			else
				++data.runde
			fügZugEin(log, -1, spielAktionen.splice(0).concat([{ art: 2, nr: data.runde }]))
			if (!data.tauschtPreußen && (data.phase < 2))
				schüttPrivatbahnenAus(spieler, log)
		}
		if (spielAktionen.length)
			fügZugEin(log, -1, spielAktionen)
		
		if (wann) {
			data.log.push(...log)
			return
		}

		if (intern) {
			Verteiler.sendeZug({ wer: dran, was: zug, wann: gibWann() })
			if (zug.passt) {
				if (data.runde < 1)
					spieler[dran].passt = true
				delete zug.passt
			}
			const logkopie = data.log.concat(log)
			data.log = log
			sendeData(null, null, { wer: dran, was: JSON.parse(JSON.stringify(zug)), dann: data.dran[0] })
			data.log = logkopie
		}
		zug.splice(0)
		beiStandGeladen(data)
		return log
	}

	static sendeText(nachricht) {
		nachricht.wann = gibWann()
		data.log.push(nachricht)
		Verteiler.sendeText(nachricht)
		
		// schicke auch separat an den Server
		const postData = {
			token: nachricht.token,
			text: nachricht.text,
			utf8: "✓",
			authenticity_token: authenticity_token
		}
		JSON.post(postData, chatPfad, e => {
			if (!e || (e.status === 204)) {
				++data.letzteZugNr
				localStorage.setItem(STAND_ID, JSON.stringify(data))
			}
		} )
		return nachricht
	}

	static aktualisieren() {
		JSON.load(standPfad + ".json", beiStandGeladen)
	}

	static schickProblem(titel, beschreibung) {
		const log = data.log
		data.log.splice(0)
		sendeData(titel, beschreibung)
		data.log = log
	}

	static setzZurück(aktionenzahl, rück) {
		if (data.extra && (data.extra[0] === data.dran[0])) {
			data.extra.splice(aktionenzahl + 1)
			if (data.extra.length < 2)
				delete data.extra
			localStorage.setItem(STAND_ID, JSON.stringify(data))
			if (rück) {
				Verteiler.sendeZugZurück(rück)
				if (Object.keys(rück).length)
					data.rück = rück
			}
		}
		aktualisierStand(data)
	}
}