class Question < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  belongs_to :container, polymorphic: true
  belongs_to :contact
  has_many :answers, dependent: :destroy
  
  # --- VALIDATIONS ----------------------------
  
  validates :token,
        presence: true,
        uniqueness: true
  validates_presence_of :content
  
  # --- SCOPES ---------------------------------
  
  scope :published, -> { where( status: 'published' )
        .where( 'published_at < ?', Time.zone.now ) }
  
  # --- INSTANCE METHODS -----------------------
  
  def iid
    "qu_#{token}"
  end
  
  def to_param
    "#{token}"
  end
  
  # --- Predicates -----------------------------
  
  def published?
    status == 'published'
  end
  
  # --- CONSTANTS ------------------------------
  
  STATUS_OPTIONS = [
    'new',
    'published'
  ]
  
end
