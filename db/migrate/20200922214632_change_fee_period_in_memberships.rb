class ChangeFeePeriodInMemberships < ActiveRecord::Migration[5.2]
  def up
    change_column :memberships, :fee_period, :integer
  end
  
  def down
    change_column :memberships, :fee_period, :string
  end
end
