module StatisticsHelper
  
  def h_quot in_a, in_b
    ( 1.0 * in_a / in_b * 100 ).round - 100 if in_b > 0
  end
  
end
