json.extract! contentpart, :id, :token, :status, :kind, :position, :container_id, :container_type, :content, :created_at, :updated_at
json.url contentpart_url(contentpart, format: :json)
