window.addEventListener('DOMContentLoaded', () => {
	if (!JSON.load && !window.lädModule) {
		const s = document.createElement("script")
		s.src = "../lib/lad_module.js"
		document.head.append(s)
	}
})