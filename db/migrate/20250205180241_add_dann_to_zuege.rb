class AddDannToZuege < ActiveRecord::Migration[5.2]
  def change
    add_column :zuege, :dann, :string, :limit => 2
  end
end
