(function () {
	window.lädModule = true
	
	const module = {}
	const vues = {}
	const suchKommentarStart = /\/\/|\/\*/
	const suchUmbruch = /[\r\n]/
	const suchKommentarEnde = /\*\//
	const suchImport = /import (\{ ([\w, \t\n]+) \} from )?["']([^"']+)["'];?\r?\n/g
	const suchFromVue = /from ["‘]vue["‘]/
	const suchVueImport = /import (\w+) from ["']([\w\./]+\.vue)["'];?/g
	const suchVue = /const (\w+) ?= ?await importVue\("([//.\w]+\.vue)"[, \w\.]*\)\n/g
	const suchExport = /export (?:\{([^\}]+)\}|(\w+)) ?(\w+)?;?/g
	const suchExportAs = /(\w+) as (\w+)/g
	const suchVueName = /(\w+)\.vue/
	const suchImportVue = /importVue/
	const suchAsyncFunction = /const AsyncFunction = \(async function \(\) \{\}\)\.constructor\n/
	const suchImportTausch = /.*import\(.*\n/
	const suchImportTausch2 = /import\./g
	const suchNichtLeer = /[\S ]/
	const alleExporte = {}
	const abhängigkeiten = []
	const basisUrl = new URL(document.querySelector("script[type=module]").attributes.src.value, document.URL)
	const vueUrl = new URL("../../lib/vendor/vue.esm-browser.js", basisUrl)
	
	// Array um at erweitern
	if (!Array.prototype.at)
		Array.prototype.at = function (i) { return this.slice(i++, i || undefined)[0] }
	
	// Array um findLast erweitern
	if (!Array.prototype.findLast)
		Array.prototype.findLast = function (finde) {
			for (i = this.length - 1; i > -1; --i)
				if (finde(this[i]))
					return this[i]
			return
		}
	
	// ResizeObserver implementieren
	if (!window.ResizeObserver) {
		window.ResizeObserver = class ResizeObserver {
			constructor(callback) {
				this.observed = []
				this.sizes = []
				this.callback = callback
			}
			
			checkSize() {
				let changed
				for (let i = 0, l = this.observed.length; i < l; ++i) {
					const h = this.observed[i].clientHeight
					if (this.sizes[i] !== h)
						changed = true
					this.sizes[i] = h
				}
				if (changed)
					this.callback()
			}
			
			observe(el) {
				this.observed.push(el)
				this.checkSize()
				if (!this.observeIntervalId)
					this.observeIntervalId = setInterval(this.checkSize.bind(this), 100)
			}
		}
	}

	function ladDatei(url, istVue, beiGeladen) {
		// console.log("lad Datei", url.pathname)
		const dateien = istVue ? vues : module
		dateien[url] = true
		fetch(url).then( r => {
			if (r.status !== 200)
				throw `"${url}" konnte nicht geladen werden`
			return r.text()
		} ).then( text => {
			if (url.pathname.includes("esm-browser")) // vue und pinia für meinen Fox aufbereiten
				text = text.replace(/for \(const/g, "for (let")
			dateien[url] = text
			
			// Kommentare ausfiltern
			let ohneKommentar = ""
			let teil = text
			let i = teil.search(suchKommentarStart)
			while (i >= 0) {
				ohneKommentar += teil.substr(0, i)
				teil = teil.substr(i)
				if (teil[1] === "/")
					teil = teil.substr(teil.search(suchUmbruch))
				else
					teil = teil.substr(teil.search(suchKommentarEnde) + 2)
				i = teil.search(suchKommentarStart)
			}
			if (ohneKommentar)
				text = ohneKommentar
			while (suchImport.exec(text)) {
				const pfad = RegExp.$3
				if (suchNichtLeer.test(RegExp.leftContext.substr(-1)))
					continue
				const neueUrl = suchFromVue.test(text) ? vueUrl : new URL(pfad, url)
				if (abhängigkeiten.every( a => a[1] !== neueUrl ))
					abhängigkeiten.push([url, neueUrl])
				if (!module[neueUrl])
					ladDatei(neueUrl, false, beiGeladen)
			}
			while (suchVue.exec(text) || suchVueImport.exec(text)) {
				const neueUrl = new URL(RegExp.$2, url)
				if (abhängigkeiten.every( a => a[1] !== neueUrl ))
					abhängigkeiten.push([url, neueUrl])
				if (!vues[neueUrl])
					ladDatei(neueUrl, true, beiGeladen)
			}
			if (Object.values(module).concat(Object.values(vues)).every( v => typeof(v) === "string") )
				beiGeladen()
		} )
	}
	
	function tauschImport(name, pfad, url, importe, fehlImporte, später) {
		const i = importe.length
		importe.push(alleExporte[name])
		if (!alleExporte[name]) {
			const u = new URL(url, pfad)
			fehlImporte.push([name, i, u])
			if (später.some( s => (s[0] === pfad) && (s[1] === u.href) ))
				return `let ${name} // from ${url}\n`
		}
		return `const ${name} = arguments[${i}] // from ${url}\n`
	}
	
	function erstellExport(text, pfad, importe, vorlage, alleExporte) {
		// console.log("erstellExport", pfad)
		const ergebnis = Function(text + "\n//# sourceURL=" + pfad)(...importe)
		if (ergebnis) {
			if (vorlage)
				Object.values(ergebnis[0])[0].template = vorlage
			Object.assign(alleExporte, ergebnis[0])
		}
		return ergebnis
	}

	function wandelDatei(pfad, text, nachImporte, später, istVue, vorlage) {
		// console.log(pfad + " geladen")
		const importe = []
		const fehlImporte = []
		text = text.replace(suchImport, (t, nix, i, url) => { // alle Skriptimporte tauschen
			if (suchNichtLeer.test(RegExp.leftContext.substr(-1)))
				return t
			// console.log(t, nix, i)
			let tausch = ""
			if (i)
				i.split(",").forEach( n => tausch += (istVue && tausch ? "\t" : "") + tauschImport(n.trimLeft(), pfad, url, importe, fehlImporte, später) )
			return tausch
		} )
		.replace(suchVue, (t, name, url) => tauschImport(name, pfad, url, importe, fehlImporte, später) ) // alle Vueimporte tauschen
		.replace(suchVueImport, (t, name, url) => tauschImport(name, pfad, url, importe, fehlImporte, später) ) // alle Vueimporte tauschen
		if (suchImportVue.test(pfad)) {
			text = text.replace(suchAsyncFunction, "") // wegen Syntaxfehler entfernen
			       .replace(suchImportTausch, "") // import wegen Syntaxfehler entfernen
			       .replace(suchImportTausch2, "") // import wegen Syntaxfehler entfernen
		}
		// importe.forEach( i => console.log(pfad, i) )
		if (suchExport.test(text)) {
			const exporte = []
			let rest
			text = text.replace(suchExport, (t, e1, e2, e3) => {
				if (suchNichtLeer.test(RegExp.leftContext.substr(-1)))
					return t
				let result = ""
				if (e1) {
					rest = e1.replace(suchExportAs, "$2:$1")
				} else if (e2 === "default") {
					const name = suchVueName.exec(pfad)[1]
					exporte.push(name)
					result = `const ${name} = `
				} else {
					if (e3)
						exporte.push(e3)
					else
						exporte.push(e2)
					result = e3 ? e2 + " " + e3 : ""
				}
				return result
			}) + `\nreturn [{${exporte.join(", ") || rest}}]`
		}
		// console.log(pfad + ":\n" + text)
		if (fehlImporte.length) {
			// console.log("fehlt", fehlImporte.map( e => e[0] ), pfad)
			nachImporte.push([fehlImporte, importe, text, pfad, vorlage])
		} else
			erstellExport(text, pfad, importe, vorlage, alleExporte)
	}
	
	ladDatei(basisUrl, false, () => {
		// alle Dateien sind geladen
		// console.log(module, vues)
		const ketten = []
		const ringe = []
		for (let abhängigkeit of abhängigkeiten) { // Importringe finden
			const kette = [abhängigkeit[0].href]
			while (abhängigkeit) {
				if (kette.includes(abhängigkeit[1].href)) {
					ringe.push(kette)
					break
				}
				kette.push(abhängigkeit[1].href)
				abhängigkeit = abhängigkeiten.find( a => a[0].href === abhängigkeit[1].href )
			}
			if (ketten.every( k => (k.at(-2) !== kette[0]) || (k.at(-1) !== kette[1]) ))
				ketten.push(kette)
		}
		if (ringe.length) {
			console.warn("Importringe:\n" + ringe.map( r => r.join(" < ") ).join("\n"))
			for (let ring of ringe) {
				let nr
				let min = Number.MAX_VALUE
				ring.forEach( (url, i) => {
					const l = abhängigkeiten.filter( a => a[0].href === url ).length
					if (l < min) {
						min = l
						nr = i
					}
				} )
				ring.push(...ring.splice(0, nr))
			}
		}
		const nachImporte = []
		Object.entries(module).forEach( m => wandelDatei(m[0], m[1], nachImporte, ringe) )
		for (let m of Object.entries(vues)) {
			const vue = parseVue(m[1])
			wandelDatei(m[0], vue[0], nachImporte, ringe, true, vue[1])
		}
		const asyncImporte = []
		while (nachImporte.length) {
			const kandidat = nachImporte.shift()
			const importe = kandidat[1]
			const fehlImporte = kandidat[0]
			const fehltNochImporte = []
			for (let i = 0; i < fehlImporte.length; ++i) {
				const fehlt = fehlImporte[i]
				const dExport = alleExporte[fehlt[0]]
				if (dExport !== undefined) {
					importe[fehlt[1]] = dExport
					fehlImporte.splice(i--, 1)
				} else if (ringe.some( r => (kandidat[3] === r[0]) && (fehlt[2].href === r[1]) ))
					fehltNochImporte.push(...fehlImporte.splice(i--, 1))
			}
			if (fehlImporte.length)
				nachImporte.push(kandidat)
			else {
				let text = kandidat[2]
				if (fehltNochImporte.length) {
					text = text.slice(0, -1)
					fehltNochImporte.forEach( n => text += `, i => ${n[0]} = i`)
					text += "]"
				}
				const ergebnis = erstellExport(text, kandidat[3], importe, kandidat[4], alleExporte)
				if (ergebnis)
					fehltNochImporte.forEach( (n, i) => asyncImporte.push([n[0], ergebnis[i + 1]]) )
			}
		}
		asyncImporte.forEach( u => u[1](alleExporte[u[0]]) ) // fehlende Importe asynchon nachliefern
	})
})()
