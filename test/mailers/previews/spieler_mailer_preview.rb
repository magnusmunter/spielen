# Preview all emails at http://localhost:3000/rails/mailers/spieler_mailer
class SpielerMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/spieler_mailer/dubistdran
  def dubistdran
    SpielerMailer.with( email: 'test@spielen.io', name: 'Hans Tester', plan_id: 2 ).dubistdran
  end

end
