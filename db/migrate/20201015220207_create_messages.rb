class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.references :sender, polymorphic: true
      t.references :receiver, polymorphic: true
      t.text :content
      t.datetime :read_at
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
