import { createApp, computed } from "../../lib/vendor/vue.esm-browser.js"
import { createPinia, storeToRefs } from "../../lib/vendor/pinia.esm-browser.js"
import "../../lib/extendJSON.js"
import { DragDirective } from "../../lib/vue_drag_directive.js"
import { nutzStand, Stand, STARTGELD, STARTPAKET } from "./Stand.js"
import { spielNr, checkNotificationsPermission, triggerNotification, gibAktionenText, gibStreckenText,
         gibAktienWert, gibAktienpackete, gibPapierWert, gibPapierlimit, rollAutomatisch, stopAutoRollen } from "./global.js"
import { BAHNEN, LOKS, GLEIS_VORLAGE } from "../config.js"
import { Verteiler } from "./Verteiler.js"
import { importVue } from "../../lib/importVue.js"

const Plan = await importVue("../components/Plan.vue", import.meta.url)
const Logbuch = await importVue("../components/Logbuch.vue")
const Spielerinfo = await importVue("../components/info/Spielerinfo.vue")
const Boerse = await importVue("../components/boerse/Boerse.vue")
const Wertverlauf = await importVue("../components/Wertverlauf.vue")
const AgMarker = await importVue("../components/AgMarker.vue")
const Karte = await importVue("../components/Karte.vue")
const Aktie = await importVue("../components/Aktie.vue")
const Auswahl = await importVue("../components/Auswahl.vue")

console.log( "plan > init" )

function gibSpielernameFürAdmin(dran) { // DONE
	if (plan.spieler[dran])
		return dran
	if (plan.ags[dran])
		return Object.keys(plan.spieler)[plan.ags[dran].direktor]
	const eintrag = Object.entries(plan.spieler).find( s => s[1].papiere.includes(dran) )
	return eintrag ? eintrag[0] : Object.keys(plan.spieler)[plan.preußenDir]
}

function prüfUndZeigRück(rück) { // DONE
	const nr = Stand.gibSpielerNr(this.aktuellerSpieler)
	const zug = rück[nr]
	if (zug) {
		alert(`${BAHNEN[zug[0]] || Object.keys(this.spieler)[zug[0]]} nimmt Aktion${zug.length > 2 ? "en" : ""} zurück:\n`
		      + zug.slice(1).map( a => gibAktionenText(a, this.spieler) ).join("\n"))
		return nr
	}
}

const CACHE_NAME = "spiel1835" // DONE

let wartetAufAktualisierung // DONE
// let spielNr = document.URL.substring(document.URL.lastIndexOf("/") + 1) // DONE
let serviceWorkerRegistration // DONE

Stand.aktualisierStand = function (stand, letzteLogNr) { // DONE
	if (!stand) {
		plan.bank = { aktien: [], pool: [], loks: [] }
		return
	}
	
	stand = JSON.parse(JSON.stringify(stand))
	wartetAufAktualisierung = true
	for (let n in stand) {
		if (n === "dran") {
			const dran = stand.dran[0]
			plan.dran = isNaN(dran) ? dran : Object.keys(stand.spieler)[dran]
		} else if (["bank", "spieler", "ags"].includes(n))
			Object.assign(plan[n], stand[n])
		else if (["vorPR", "gleise", "bhfe", "aktionen", "neueSpur", "log"].includes(n)) {
			plan[n].splice(0)
			plan[n].push(...stand[n])
		} else if (n !== "ende")
			plan[n] = stand[n]
	}
	plan.letzteLogNr = letzteLogNr

	// bereits gemachte Aktionen wieder herstellen
	const dran = plan.extra ? plan.extra[0] : plan.dran
	const aktionen = plan.aktionen.length ? plan.aktionen : plan.extra ? plan.extra.slice(1) : []
	const verkaufte = new Set()
	for (let aktion of aktionen) {
		if (aktion.geld && !aktion.lok && isNaN(aktion.von))
			(plan.spieler[dran] || plan.ags[dran] || plan.vorPR[dran[1] - 1]).geld
			+= aktion.papier ? aktion.geld : aktion.fährt ? aktion.einbehalt ? aktion.geld : plan.ags[dran] ? 0 : aktion.geld / 2 : -aktion.geld

		if (aktion.gleis)
			plan.gleise.push(aktion.gleis)
		else if (aktion.bhf)
			plan.bhfe.push(aktion.bhf)
		else if (aktion.fährt)
			plan.einbehalt = aktion.einbehalt
		else if (aktion.lok) {
			if (aktion.geld) {
				Stand.kaufLok(aktion.lok, aktion.geld, aktion.von, plan, null, null, plan.extra && plan.extra[0])
				if (plan.tausch) { // getauschte vorpreußisch verkaufbar machen
					const direktor = plan.spieler[plan.dran]
					if (direktor && direktor.geld < 0)
						direktor.papiere = direktor.papiere.map( p => plan.tausch.includes(p) ? Stand.doppelPR.includes(p) ? "PR2" : "PR" : p )
				}
			} else
				Stand.gibAbLok(aktion, plan)
		} else if (aktion.papier) { // ver- / kaufen / verstaatlichen
			const spieler = Object.values(plan.spieler)
			const papiere = (spieler[aktion.von] || spieler[dran] || plan.spieler[dran]).papiere
			if (aktion.geld > 0) { // verkaufen
				if (aktion.von >= 0)
					spieler[aktion.von].geld += aktion.geld
				Stand.gibAbPapier(papiere, aktion.papier)
				Stand.nimmAufPapier(plan.bank.pool, aktion.papier)
				const ag = aktion.papier.substring(0, 2)
				if (!verkaufte.has(ag))
					Stand.verschiebKurs(plan.ags[ag].kurs, 1, -1, plan.ags)
				verkaufte.add(ag)
			} else { // kaufen / verstaatlichen
				if (aktion.von >= 0) {
					spieler[aktion.von].geld += aktion.geld
					Stand.gibAbPapier(spieler[aktion.von].papiere, aktion.papier)
				} else
					Stand.gibAbPapier(plan.bank[aktion.von || "aktien"], aktion.papier)
				Stand.nimmAufPapier(papiere, aktion.papier)
			}
			const ag = aktion.papier.substr(0, 2)
			Stand.prüfUndÄnderDirektor(plan.ags[ag], spieler, ag, plan)
		} else if (aktion.ende) {
			const wer = aktion.wer
			Stand.wandelUmVorPR(wer, Stand.gibDirektorNrFürBahn(wer), (plan.extra[0] < 0) || (!wer.startsWith("p") && (wer[1] > dran[1])), plan)
			if (aktionen.filter( a => a.ende ).at(-1) === aktion)
				Stand.prüfUndÄnderDirektor(plan.ags.PR, Object.values(plan.spieler), "PR", plan)
		} else if (aktion.art === 17)
			Stand.gehBankrott(aktion.wer, plan)
	}

	if (plan.admin && plan.dran)
		plan.aktuellerSpieler = stand.spielerNameFürAdmin
	

	if (!plan.info) {
		if (plan.ende // Logbuch für Endabrechnung öffnen
		    || (plan.letzteLogNr < plan.log.length)) // letzte Züge nach letztem laden markieren und Logbuch öffnen
			plan.$el.parentNode.querySelector("section:nth-of-type(2) > h1").click()
		else if (!BAHNEN[plan.dran] !== plan.offeneTabs.includes(0)) // Börse für Aktienrunde öffnen oder für Betriebsrunde schließen
			plan.$el.parentNode.querySelector("#section_boerse > h1").click()
	}
}

function istPrivatbahnPaketUnvollständig(bahn) { // DONE
	if (!plan.bank.aktien.includes(bahn)) {
		const papiere = Object.values(plan.spieler).find( s => s.papiere.includes(bahn) ).papiere
		return papiere.reduce( (r, e) => r += e ===  "BY" ? 1 : ["p1", "p5", "p6"].includes(e) ? -1 : 0 , 0) < 0
	}
}

// Plan aufbauen
const plan = createApp({
	components: {
		Plan,
		Logbuch,
		Spielerinfo,
		Boerse,
		Wertverlauf
	},
	data() { return {
		version: "", // DONE
		info: document.location.search.includes("info"), // DONE
		gleisVorlage : GLEIS_VORLAGE, // DONE
		bahnen: BAHNEN, // DONE
		einbehalt: false, // DONE
		letzteFahrt: null, // DONE
		letzteZüge: [], // DONE
		offeneTabs: [], // DONE
		fehler: null, // DONE
		zeigtProblem: false, // DONE
		zeigtInstallation: false, // DONE
		sitz: new Set(), // DONE
		stopptSpieler: false // DONE
	} },
	setup() {
		const stand = nutzStand()

		return Object.assign(storeToRefs(stand), {
			gibGleisRest: stand.gibGleisRest,
			gibBhfRest: stand.gibBhfRest,
			gibLokZahl: stand.gibLokZahl,
			gibLokrest: stand.gibLokrest,
			verteilLok: stand.verteilLok,
			reichtGeld: stand.reichtGeld,
		})
	},
	updated() { // DONE
		if (wartetAufAktualisierung) {
			wartetAufAktualisierung = false
			let fortsetzen
			if (this.extra && !this.tauschtPreußen && (this.extra.length > 1) // Extrazug abarbeiten
			    && ((this.extra[0] === this.dran) || (this.extra[0] === Stand.gibSpielerNr(this.dran)))) {
				this.aktionen = this.extra.slice(1)
				if (Array.isArray(this.extra.at(-1))) // Lokkaufanfrage
					this.aktionen.pop()
				else
					this.extra = null
				fortsetzen = true
			}
			
			if (this.rück) {
				const nr = prüfUndZeigRück.call(this, this.rück)
				if (nr >= 0) {
					delete this.rück[nr]
					Stand.setzRück(this.rück)
					delete this.rück
				}
			}
			if (this.istAktuellerSpielerDran) {
				if (this.extra) {
					if (!this.tauschtPreußen && (this.extra[0] === this.dran)) {
						const lokkaufAnfrage = this.extra.at(-1)
						if (!lokkaufAnfrage.length || (lokkaufAnfrage.length < 4)) {
							this.beiLokkaufBestätigt(lokkaufAnfrage.length ? lokkaufAnfrage : null)
							this.extra = null
						}
					} else {
						if (this.extra.some( a => a.gleis && (a.gleis.x === 2) && (a.gleis.y === 11) ))
							alert(BAHNEN["BA"] + " muss Heimatbahnhof wählen.")
						else if (this.istLokAbgabe)
							alert("Loklimit hat sich durch Phasenwechsel verkleinert.\n"
							      + BAHNEN[this.dran] + " muss überzählige Loks abgeben.")
						else if (this.tauschtPreußen) {
							const spielerNr = Stand.gibDirektorNrFürBahn(this.dran)
							const bahn = this.extra.at(-1)
							const zwangsweise = this.extra.some( a => a.lok === "4+" )
							if ((!bahn.ende || (Stand.gibDirektorNrFürBahn(bahn.wer) !== spielerNr))
							    && (Object.values(this.spieler)[spielerNr].papiere.find( p => {
							      return (!zwangsweise || (p !== "v2")) && Stand.istVorpreußische(p)
							    } ) === this.dran)) {
								if (this.dran === "v2")
									alert(`Besitzer der ${BAHNEN["v2"]} darf Preußen eröffnen, da erste 4er Lok gekauft wurde.`)
								else if (this.extra.some( a => a.ende && (a.wer === "v2") ))
									alert(`Preußenpapiere können umgewandelt werden, da Besitzer der ${BAHNEN["v2"]} Preußen eröffnet hat.`)
								else if (zwangsweise)
									alert(`Preußenpapiere können umgewandelt werden, da Besitzer der ${BAHNEN["v2"]} Preußen eröffnen musste.`)
							}
						} else if (this.spieler[this.dran] 
						           && (this.spieler[this.dran].papiere.length > gibPapierlimit(this.dran, this)))
							alert(this.dran + " hat zuviele Papiere und muss eins verkaufen.")
					}
				} else if (fortsetzen) {
					alert("Zug fortsetzen.")
					if (this.gibLokrest(this.ags[this.dran] ? this.dran : this.dran[1]) < 0)
						alert("Loklimit hat sich durch Phasenwechsel verkleinert.\nÜberzählige Loks abgeben.")
				}
				if (this.aktionen.length && this.aktionen.at(-1).fährt)
					this.$refs.plan.fahrLok()
			} else {
				if ((this.dran !== this.aktuellerSpieler) && this.sitz.has(this.direktor || this.dran))
					this.stopptSpieler = true
				if (this.extra) {
					if ((this.dran === this.aktuellerSpieler) && (this.spieler[this.dran].papiere.length > gibPapierlimit(this.dran, this)))
						alert(this.dran + " hat zuviele Papiere und muss eins verkaufen.")
					else {
						const lokkaufAnfrage = this.extra.at(-1)
						if (lokkaufAnfrage.length
						    && (Stand.gibDirektorNrFürBahn(lokkaufAnfrage[2]) === Stand.gibSpielerNr(this.aktuellerSpieler))) {
							if (lokkaufAnfrage.length > 3)
								this.beiBestätigLokkauf(lokkaufAnfrage)
							else // TODO prüfen
								Stand.kaufLok(...lokkaufAnfrage, this)
						}
					}
				} else if (fortsetzen && (this.dran === this.aktuellerSpieler))
					alert("Zug fortsetzen.")
			}
		}
	},
	provide() { return { plan: computed( () => this.$refs.plan ) } }, // DONE
	computed: {
		direktor() { // DONE
			const bahn = this.dran
			if (BAHNEN[bahn]) {
				if (this.ags[bahn]) {
					const aktion = this.aktionen.at(-1)
					if (aktion && (aktion.art === 17)) // ehemaliger Direktor ist bankrott
						return Object.keys(this.spieler)[aktion.wer]
					return Object.keys(this.spieler)[this.ags[bahn].direktor]
				}
				const eintrag = Object.entries(this.spieler).find( s => s[1].papiere.includes(bahn) )
				return eintrag ? eintrag[0] : Object.keys(this.spieler)[this.preußenDir]
			}
		},
		originalDirektor() { // DONE
			if (this.preußenDir >= 0)
				return Object.keys(this.spieler)[this.preußenDir]
			const bahn = this.extra[0]
			if (BAHNEN[bahn])
				return this.ags[bahn] ? Object.keys(this.spieler)[this.ags[bahn].direktor]
				                      : Object.entries(this.spieler).find( s => s[1].papiere.includes(bahn) )[0]
		},
		istAktuellerSpielerDran() { // DONE
			const data = this.spieler[this.aktuellerSpieler]
			if (!data)
				return
			const ag = this.ags[this.dran]
			if (ag) {
				const aktion = this.aktionen.at(-1)
				if (aktion && (aktion.art === 17)) // ehemaliger Direktor ist bankrott
					return aktion.wer === Stand.gibSpielerNr(this.aktuellerSpieler)
				const nr = Stand.gibSpielerNr(this.aktuellerSpieler)
				return (ag.direktor === nr) || ((this.dran === "PR") && (this.preußenDir === nr))
			}
			return data.papiere.includes(this.dran)
		},
		/**
		 * Liefert alle Aktionen. die für Bahnen anzuzeigen sind
		 */
		bahnAktionen() { // DONE
			return (!this.tauschtPreußen || this.ags[this.dran])
			       && this.aktionen.filter( a => (!a.bhf || (a.bhf.bahn === this.dran))
			                                     && (!a.lok || (a.lok && (this.extra || a.geld || !a.von)))
			                                     && !a.papier && !a.art && !a.ende)
		},
		fahrGeld() { // DONE
			const aktion = this.aktionen.find( a => a.fährt )
			return aktion && aktion.geld
		},
		zugUnvollständig() { // DONE
			if (BAHNEN[this.dran] && (this.gibLokrest(this.ags[this.dran] ? this.dran : this.dran[1]) < 0))
				return true // überzählige Loks nicht abgegeben
			if (this.extra) {
				const aktion = this.extra.at(-1)
				const gleis = aktion.gleis
				if (gleis && (gleis.x === 2) && (gleis.y === 11) && (this.dran === "BA") && !this.aktionen.some( a => a.bhf ))
					return true // Baden hat noch keinen Heimatbahnhof gebaut
				if (Array.isArray(aktion) && (aktion.length !== 3)) // Lokkauf unbestätigt
					return true
			} else {
				if (this.$refs.plan && this.$refs.plan.auswählen) // Auswahl unbestätigt
					return true
				const ag = this.ags[this.dran]
				if (ag) {
					if (ag.loks.length) {
						const lokzahl = ag.loks.length - this.aktionen.reduce( (r, a) => r += a.lok ? 1 : 0 , 0)
						if (this.fahrstrecken.length < Math.min(this.fahrbareStrecken.length, lokzahl))
							return true // nicht alle Loks gefahren
					} else if (!this.aktionen.length || !this.aktionen.at(-1).lok) // Plichtlok nicht gekauft
						return true
				}
			}
			return false
		},
		aufnahmeUnvollständig() { // DONE
			const aktien = this.bank.aktien
			if (this.runde === -2) {
				const pakete = gibAktienpackete(aktien, this.ags)
				if ((pakete["BY"] > 5) || aktien.includes("BY2")) {
					if (Object.entries(pakete).some( e => e[0] === "BY" ? false : (e[0] === "SX") && !aktien.includes("SX2") ? e[1] < 8 : e[1] < 10 ))
						return true // andere Aktien als vom Startpaket verteilt
					if (aktien.includes("p2")) {
						if (!aktien.includes("SX2"))
							return true
					} else if (!Object.values(this.spieler).find( s => s.papiere.includes("p2") ).papiere.includes("SX2"))
						return true
					return istPrivatbahnPaketUnvollständig("p1") || istPrivatbahnPaketUnvollständig("p5") || istPrivatbahnPaketUnvollständig("p6")
					       || (aktien.reduce( (r, a) => r -= a === "BY" ? 1 : 0 , 8) > aktien.reduce( (r, a) => r -= ["p1", "p5", "p6"].includes(a) ? 1 : 0 , 3))
				}
				return aktien.includes("v2") && (pakete["PR"] > 9.5)
			}
			return (this.runde === -4) && ((this.standRunde < 0) || this.kursloseAGs.length)
		},
		installierbar() { return navigator.serviceWorker }, // DONE
		kursloseAGs() { return Object.keys(this.ags).filter( n => this.ags[n].betrieb && !this.ags[n].kurs ) }, // DONE
		ende() { return this.log.some( z => z.was && z.was.length && (z.was[0].art === 16) ) } // DONE
	},
	watch: {
		dran(dran) { // DONE
			let letzteFahrt = this.letzteFahrt
			if (letzteFahrt) // alte letzte Fahrt löschen
				this.letzteFahrt = letzteFahrt = null
			if (!this.extra && !this.tauschtPreußen) { // letzte Fahrt markieren
				const log = this.log
				for (let i = log.length - 1; !letzteFahrt && (i >= 0); --i) {
					const zug = log[i]
					letzteFahrt = (zug.wer === dran) && zug.was.find( a => a.fährt )
				}
				if (letzteFahrt)
					this.letzteFahrt = letzteFahrt.fährt
			}
		},
		einbehalt(einbehalt) { // DONE
			const dran = this.dran
			const ag = this.ags[dran]
			if (ag && this.fahrGeld) {
				let geld = einbehalt ? this.fahrGeld : -this.fahrGeld
				ag.geld += geld
				if (this.bank.pool.includes(dran))
					ag.geld -= geld * gibAktienpackete(this.bank.pool, this.ags)[dran] / 10
			}
		}
	},
	methods: {
		gibGleisRestStil(vorlage) { // DONE
			switch (vorlage.bild) {
				case 0: return `left: ${vorlage.x + 48}px; top: ${vorlage.y + 20}px`
				case 1: return `left: ${.7273 * vorlage.x + 32}px; top: ${.7273 * vorlage.y + 394}px`
				case 2: return `left: ${.8678 * vorlage.x + 40}px; top: ${.8678 * vorlage.y + 748}px`
			}
		},
		beiFahr(geld) { // DONE
			if (this.dran.startsWith("v"))
				this.vorPR[this.dran[1] - 1].geld += geld / 2
			else if (!this.einbehalt)
				this.ags[this.dran].geld += geld * (gibAktienpackete(this.bank.pool, this.ags)[this.dran] || 0) / 10
		},
		gibAktionenText: gibAktionenText, // DONE
		gibStreckenText(strecke) { return gibStreckenText.call(this.$refs.plan, strecke) }, // DONE
		löschFahrt() { // DONE
			for (let strecke of this.fahrstrecken)
				this.$refs.plan.löschSpur(strecke)
			this.$refs.plan.setzSpurZurück()
		},
		löschSpur(i) { // DONE
			const fahrt = this.aktionen.find( a => a.fährt )
			const strecke = fahrt.fährt.splice(i, 1)[0]
			let geld = strecke[4]
			fahrt.geld -= geld
			if (this.dran.startsWith("v"))
				this.vorPR[this.dran[1] - 1].geld -= geld / 2
			else {
				if (!this.einbehalt)
					geld *= gibAktienpackete(this.bank.pool, this.ags)[this.dran] / 10
				if (geld)
					this.ags[this.dran].geld -= geld
			}
			this.$refs.plan.löschSpur(strecke)
			if (!fahrt.fährt.length)
				this.aktionen.pop()
		},
		löschLetzteAktion() { // DONE
			const rück = this.rück || {}
			let dran
			while (this.aktionen.length) { // Spieler von Extrazügen benachrichtigen
				let wen
				const letzte = this.aktionen.at(-1)
				if (letzte.von) {
					if (letzte.lok) { // von Bahn gekaufte Lok / Lokzwangsabgabe
						wen = Stand.gibDirektorNrFürBahn(letzte.von)
						if (Object.keys(this.spieler)[wen] === this.aktuellerSpieler) {
							wen = -1 // niemand benachrichtigen, da selber
							break
						}
					} else if (letzte.papier && (letzte.geld > 0)) // Aktienzwangsverkauf
						wen = letzte.von
					else
						break
				} else if (letzte.bhf && (letzte.bhf.bahn !== this.dran)) // Heimatbahnhof Baden
					wen = Stand.gibDirektorNrFürBahn(letzte.bhf.bahn)
				else if (letzte.ende) {
					wen = Stand.gibDirektorNrFürBahn(letzte.wer)
					this.tauschtPreußen = false
					this.tausch = null
				} else
					break
				if (dran === undefined)
					dran = BAHNEN[this.dran] ? this.dran : Stand.gibSpielerNr(this.dran)
				if (!rück[wen] && (wen !== dran) && (wen !== Stand.gibDirektorNrFürBahn(this.dran)))
					rück[wen] = [dran]
				if (letzte.lok && letzte.geld) // von Bahn gekaufte Lok
					break
				this.aktionen.pop()
			}
			if (this.neueSpur)
				this.$refs.plan.setzSpurZurück(this.neueSpur.typ, this.neueSpur.lok)
			const aktion = this.aktionen.pop()
			Object.values(rück).forEach( v => v.push(aktion) )
 			Stand.setzZurück(this.aktionen.length, rück)
			if (aktion.gleis || aktion.bhf) {
				if (aktion.gleis)
					Verteiler.sendeGleisWeg()
				if (aktion.bhf)
					Verteiler.sendeBhfWeg()
				this.$refs.plan.aktualisierNetz()
			}
		},
		sendeZug() { // DONE
			const zug = this.aktionen
			
			// haben wir schon die Erlaubnis
			// Notifications zu senden?
			checkNotificationsPermission()
			
			this.tauschtPreußen = false
			if (this.spieler[this.dran] && this.aktionen.length && (this.spieler[this.dran].geld < this.aktionen.at(-1).geld)) {
				// nach Aktienzwangsverkauf / Bankrott?
				this.dran = this.extra.shift()
				if (this.extra.at(-1) instanceof Function) {
					const danach = this.extra.pop()
					this.aktionen.unshift(...this.extra)
					danach()
				} else {
					this.aktionen.unshift(...this.extra)
					this.extra = null
				}
			} else if (this.extra && ((this.extra[0] < 0) || (this.extra.length > 1) && !Array.isArray(this.extra.at(-1)))) {
				this.extra = null
				this.letzteZüge = Stand.ziehExtra(zug, null, true, this) || [] // Preußentausch zwischen Runden abgeschlossen
			} else {
				this.extra = null
				if (zug.length && (zug.at(-1).art !== 17)) {
					if (this.fahrGeld) {
						if (this.einbehalt) {
							zug.find( a => a.fährt ).einbehalt = true
							this.einbehalt = false
						}
						this.löschFahrt()
					} else if (this.ags[this.dran] && !zug.some( a => a.fährt )) {
						const i = zug.findIndex( a => a.lok )
						if (i < 0)
							zug.push({ fährt: true })
						else
							zug.splice(i, 0, { fährt: true })
					}
				}
				if (this.passt) {
					zug.passt = true
					this.passt = false
				}
				this.letzteZüge = Stand.zieh(zug, true).slice(1)
			}
		},
		sendeText(nachricht) { // DONE
			// haben wir schon die Erlaubnis
			// Notifications zu senden?
			checkNotificationsPermission()
			
			// zu den eigenen Aktionen dazu
			this.letzteLogNr = this.log.push(Stand.sendeText({
				token: this.spieler[ this.aktuellerSpieler ].token,
				text: nachricht
			}))
		},
		aktualisieren() { // DONE
			// haben wir schon die Erlaubnis
			// Notifications zu senden?
			checkNotificationsPermission()
			
 			Stand.aktualisieren()
		},
		toggleTab( nr, tab ) { // DONE
			// haben wir schon die Erlaubnis
			// Notifications zu senden?
			checkNotificationsPermission()
			
			const zugForm = document.querySelector( '#sende_zug' )
			const zugFormHeight = zugForm.offsetHeight
			
			if ( this.offeneTabs.includes( nr ) ) {
				tab.classList.remove( 'open' )
				this.offeneTabs.splice( this.offeneTabs.indexOf( nr ), 1 )
			} else {
				tab.classList.add( 'open' )
				const tabContent = tab.querySelector( '.tab_content' )
				if ( tabContent ) {
				  tabContent.setAttribute( 'style', 'padding-top: ' + ( zugFormHeight + 10 ) + 'px;' )
				}
				const eingabe = tab.querySelector(".new_chat_post textarea")
				if (eingabe)
					eingabe.focus()
				this.offeneTabs.push( nr )
			}
		},
		beiKlickTab(event) { // DONE
			const tab = event.target.parentElement
			const nr = Array.prototype.findIndex.call(this.$el.parentNode.querySelectorAll("section"), s => s == tab)
			this.toggleTab( nr, tab )
		},
		beiKlickBtn( event ) { // DONE
			const btn = event.target
			const tab_sel = btn.dataset.tab_sel
			const tab = document.querySelector( tab_sel )
			const nr = Array.prototype.findIndex.call(this.$el.parentNode.querySelectorAll("section"), s => s == tab)
			this.toggleTab( nr, tab )
		},
		beiBestätigLokkauf(data) { // DONE
			const spieler = this.spieler[this.aktuellerSpieler]
			if (spieler) {
				const von = data[2]
				if ((von.startsWith("v") && spieler.papiere.some( p => p === von ))
					|| (this.ags[von].direktor === Stand.gibSpielerNr(this.aktuellerSpieler))) {
					const typ = data[0]
					const extra = this.extra || [this.dran, [...data, true]]
					if (confirm(`Darf ${BAHNEN[this.dran]} ${typ + (typ.endsWith("+") ? typ[0] : "")}er Lok für ${data[1]}M von ${BAHNEN[von]} kaufen?`)) {
						extra.at(-1).pop()
						Verteiler.sendeLokkaufBestätigt(data)
					} else {
						extra.at(-1).splice(0)
						Verteiler.sendeLokkaufBestätigt()
					}
					Stand.setzExtra(extra, extra[1][2], extra[0])
				}
			}
		},
		beiLokkaufBestätigt(data) { // DONE
			const spieler = this.spieler[this.aktuellerSpieler]
			if (spieler) {
				if ((this.dran.startsWith("v") && spieler.papiere.some( p => p === this.dran))
					|| (this.ags[this.dran] && (this.ags[this.dran].direktor === Stand.gibSpielerNr(this.aktuellerSpieler)))) {
					this.extra.pop()
					if (data) {
						alert("Zug fortsetzen.")
						this.kaufLok(...data)
					} else
						alert("Lokkauf wurde abgelehnt.")
				}
			}
		},
		kaufLok(typ, geld, von) { // DONE
			if (!geld)
				geld = LOKS[typ].geld
			const dran = this.dran
			const ag = this.ags[dran]
			const zuschießen = ag && !ag.loks.length && (geld > ag.geld)
			if ((!zuschießen && this.reichtGeld(geld))
			    || (zuschießen && confirm((BAHNEN[dran]) + " hat nicht genug Geld.\nPrivates Geld zuschießen?"))) {
				const verkauft = zuschießen && (this.spieler[this.direktor].geld < geld - ag.geld) // Aktienzwangsverkauf / Bankrott?
				if (!verkauft || confirm(this.direktor + " hat nicht genug Geld.\nAktien verkaufen?")) {
					if (this.fahrGeld && this.einbehalt)
						this.aktionen.find( a => a.fährt ).einbehalt = true
					const aktion = {lok: typ, geld: geld}
					if (von)
						aktion.von = von
					this.aktionen.push(aktion)
					
					const danach = () => {
						const wer = []
						if (typ == 4) {
							alert(`Besitzer der ${BAHNEN["v2"]} darf Preußen eröffnen.`)
							wer.push("v2")
						} else if (typ === "4+") {
							const vorPR = Stand.gibWandelbareVorPR(Object.values(this.spieler), Stand.gibDirektorNrFürBahn("v2"))
							const index = vorPR.indexOf("v2")
							if (index >= 0)
								vorPR.splice(index, 1)
							alert(`${Object.keys(this.spieler)[Stand.gibDirektorNrFürBahn(vorPR[0])]} darf Preußenpapiere umwandeln,`
							      + ` da Besitzer der ${BAHNEN["v2"]} Preußen eröffnen musste.`)
							wer.push(...vorPR)
						} else if (Number.isInteger(extra)) {
							alert(Object.keys(this.spieler)[extra] + " hat zuviele Papiere und muss eins verkaufen.")
							wer.push(extra)
						} else {
							const loksAbzugeben = Stand.gibBahnenMitÜberzähligenLoks(this, dran, Object.values(this.spieler))
							if (loksAbzugeben.length) {
								alert(`${BAHNEN[loksAbzugeben[0]]} gibt überzählige Loks ab.`)
								wer.push(...loksAbzugeben)
							} else if (this.gibLokrest(this.ags[dran] ? dran : dran[1]) < 0) {
								alert("Überzählige Loks abgeben.")
								return
							} else
								return
						}
						this.löschFahrt()
						Stand.ziehExtra(this.aktionen, wer, true)
					}
					const extra = Stand.kaufLok(typ, geld, von, this)
					if (verkauft) {
						Stand.gibDirektorNrFürBahn(dran)
						this.extra = [dran].concat(this.aktionen.splice(0))
						if (extra || (extra === 0)) // Sonderzüge durch Phasenwechsel
							this.extra.push(danach)
						this.dran = this.direktor
					} else if (extra || (extra === 0)) // Sonderzüge durch Phasenwechsel
						danach()
				}
			}
		},
		kaufPapier(typ, von) { // DONE nach nutzStand umgezogen
			const dran = this.dran
			const info = this.spieler[dran]
			const kauft = von !== dran
			const bahn = typ.substr(0, 2)
			const zwangsweise = info.geld < 0
			const prüfBankrott = zwangsweise && (() => {
				if (!this.extra.blockt)
					this.extra.blockt = []
				this.extra.blockt.push(typ)
				if (!this.extra.blockt.reduce( (r, p) => {
					const i = r.indexOf(p)
					if (i >= 0)
						r.splice(i, 1)
					return r
				} , info.papiere.filter( p => this.ags[p.substring(0,2)] )).length) {
					this.aktionen.splice(0, this.aktionen.length, ...this.extra.slice(1), { art: 17, wer: Stand.gibSpielerNr(dran) })
					alert(dran + " ist Bankrott")
					this.sendeZug()
				}
			})
			if (kauft) {
				if (info.sperre && info.sperre.some( s => typ.startsWith(s) ))
					return alert(dran + " kann diese Runde verkaufte Aktien nicht wieder kaufen")
				else if (info.papiere.length >= gibPapierlimit(dran, this))
					return alert(dran + " hat sein Papierlimit erreicht")
			} else { // verkauft
				Stand.sperrAGFürSpieler(bahn, info)
				let teil = typ.length > 2 ? 2 : 1
				if (typ.startsWith("PR"))
					teil /= 2
				if ((typ.length > 2) && (this.ags[bahn].direktor === Stand.gibSpielerNr(dran))
				    && Object.values(this.spieler).every( s => (s === info) || (gibAktienpackete(s.papiere, this.ags)[bahn] || 0) < teil )) {
					alert(dran + " darf Direktoraktie nicht verkaufen,\nda sie sonst niemand übernehmen kann")
					if (zwangsweise)
						prüfBankrott()
					return
				}
				const pool = gibAktienpackete(this.bank.pool, this.ags)[bahn]
				if (pool && (teil + pool > 5)) {
					alert(dran + " darf diese Aktie nicht verkaufen,\nda höchstens die Hälfte der AG am Markt sein darf")
					if (zwangsweise)
						prüfBankrott()
					return
				}
				if (zwangsweise && this.extra && typ.startsWith(this.extra[0])) {
					const anteil = gibAktienpackete(info.papiere, this.ags)[bahn]
					if (Object.values(this.spieler).some( s => (s !== info) && (anteil - teil < gibAktienpackete(s.papiere, this.ags)[bahn]) )) {
						alert(dran + " darf seinen Direktorposten nicht aufgeben")
						prüfBankrott()
						return
					}
				}
			}
			
			let wert
			if (von) {
				wert = gibAktienWert(this.ags[bahn])
				if (typ.startsWith("PR"))
					wert /= 2
				if (typ.length > 2)
					wert *= 2
			} else
				wert = gibPapierWert(typ)
			if (von && kauft && (von !== "pool")) // verstaatlichen
				wert *= 1.5
			if (kauft)
				wert *= -1
			if (!kauft || this.reichtGeld(-wert)) {
				info.geld += wert
				if (wert > 0) { // verkaufen
					Stand.gibAbPapier(info.papiere, typ)
					Stand.nimmAufPapier(this.bank.pool, typ)
				} else if (von && (von !== "pool")) { // verstaatlichen
					Stand.gibAbPapier(this.spieler[von].papiere, typ)
					Stand.nimmAufPapier(info.papiere, typ)
				} else { // kaufen
					const papiere = this.bank[von || "aktien"]
					Stand.gibAbPapier(papiere, typ)
					Stand.nimmAufPapier(info.papiere, typ)
				}
				const aktion = { papier: typ, geld: wert }
				if (this.extra) // Zwangsverkauf durch Limitüberschreitung oder Aktienzwangsverkauf / Bankrott?
					aktion.von = Stand.gibSpielerNr(this.dran)
				else if (von && kauft)
					aktion.von = this.spieler[von] ? Stand.gibSpielerNr(von) : von
				this.aktionen.push(aktion)
				if (["p1", "p5", "p6"].includes(typ)) {
					typ = "BY"
					Stand.nimmAufPapier(info.papiere, typ)
					this.aktionen.push({ papier: typ })
				} else if (typ === "p2") {
					typ = "SX2"
					Stand.nimmAufPapier(info.papiere, typ)
					this.aktionen.push({ papier: typ })
				}
				Stand.prüfUndÄnderDirektor(this.ags[bahn], Object.values(this.spieler), bahn, this)
				if (info.geld < 0)
					prüfBankrott()
			}
		},
		
		/**
		 * Geht in der Spielstandsaufnahme einen Schritt weiter
		 * Erster ist Papiere verteilen
		 */
		gehSpielaufnahmeWeiter() { // DONE
			if (gibAktienpackete(this.bank.aktien, this.ags)["BY"] > 5) // Startpaket nicht komplett verteilt
				Stand.speicherStand(this.bank.geld, STARTPAKET, null, null, this.vorPR, null, this.spieler, 0, -1,
				                    Number(prompt("Nummer der Person, die dran ist") || 0),
				                    Number(prompt("Nummer der Person, die zuletzt gepasst hat") || -1), "2")
			else {
				switch (this.runde) {
					case -2: // Loks verteilen
						const ags = Object.keys(this.ags).filter( (ag, i, ags) => {
							if ((gibAktienpackete(this.bank.aktien, this.ags)[ag] || 0) <= 5) {
								this.ags[ag].betrieb = true
								this.ags[ag].kurs = null
							}
							if (i < 1)
								return false
							return (i < 2) || this.ags[ags[i - 1]].betrieb
						} )
						if (this.ags.BY.betrieb && !Object.values(this.spieler).some( s => s.papiere.includes("v2") )) {
							this.ags.PR.betrieb = true
							this.ags.PR.kurs = null
						}
						if (this.ags.BA.direktor >= 0)
							ags.push("PR")
						this.bank.aktien = this.bank.aktien.filter( p => ags.includes(p.slice(0, 2)) )
						for (let typ of ["2", "2+", "3", "3+", "4", "4+", "5", "5+", "6", "6+"])
							for (let i = 0, e = LOKS[typ].zahl; i < e; ++i)
								this.bank.loks.push(typ)
						break
					case -3: // Runde und Aktienkurse setzen
						let lok = "2"
						let zahl = 0
						const loks = []
						for (let l of this.bank.loks) {
							if (l != lok) {
								lok = l
								if (zahl && (this.bank.loks.filter( l => l == lok ).length == this.gibLokZahl(lok)))
									break
								zahl = 0
							}
							loks.push(l)
							++zahl
						}
						this.bank.loks = loks
						this.lok = lok
						this.dran = "admin"
						break
					case -4: // abschließen
						let aktion
						let geld = this.bank.geld
						if (!this.standRunde && !this.gleise.length && this.vorPR.every( v => !v.loks.length )
						    && (gibAktienpackete(this.bank.aktien, this.ags)["BY"] > 5)) { // Startversteigerung unvollständig
							const spielerData = Object.values(this.spieler)
							for (let spieler of spielerData) {
								spieler.geld = Stand.gibStartGeld(spielerData.length)
								geld -= spieler.geld
								for (let p of spieler.papiere) {
									if (p !== "BY") {
										const wert = gibPapierWert(p)
										spieler.geld -= wert
										if (p.startsWith("v"))
											this.vorPR[Number(p[1]) - 1].geld = wert
										else
											geld += wert
									}
								}
							}
							aktion = { bank: { geld, aktien: STARTPAKET, pool: this.bank.pool, loks: this.bank.loks },
									vorPR: this.vorPR, ags: this.ags, spieler: this.spieler, phase: this.phase, runde: -1,
									dran: Number(prompt("Nummer der Person, die dran ist")),
									aktienStart: Number(prompt("Nummer der Person, die zuletzt gepasst hat") || -1),
									lok: this.lok }
						} else {
							Object.keys(this.spieler).forEach( (name, spielerNr) => {
								let wert = Number(prompt("Vermögen von " + name) || 0)
								geld -= wert
								const spieler = this.spieler[name]
								spieler.geld = wert
								for (let i = 1; i <= 6; ++i)
									if (spieler.papiere.includes("v" + i)) {
										wert = Number(prompt("Bertiebsvermögen " + BAHNEN["v" + i]) || 0)
										geld -= wert
										this.vorPR[i - 1].geld = wert
									}
								for (let ag in this.ags)
									if (this.ags[ag].direktor == spielerNr) {
										wert = Number(prompt("Bertiebsvermögen " + BAHNEN[ag]) || 0)
										geld -= wert
										this.ags[ag].geld = wert
									}
							} )
							aktion = { bank: { geld, aktien: this.bank.aktien, pool: this.bank.pool, loks: this.bank.loks },
									vorPR: this.vorPR, ags: this.ags, spieler: this.spieler, phase: this.phase, runde: this.standRunde,
									dran: this.standRunde ? prompt("Kürzel der Bahn, die dran ist") || -1
															: Number(prompt("Nummer der Person, die dran ist")),
									aktienStart: Number(prompt("Nummer der Person, die zuletzt gepasst hat")),
									lok: this.lok, gleise: this.gleise, bhfe: this.bhfe }
						}
						Stand.zieh([JSON.parse(JSON.stringify(aktion))], true)
						return
				}
				--this.runde
			}
		},
		
		wandelUm() { // DONE
			const aktion = { ende: 2 }
			if (this.extra)
				aktion.wer = this.dran
			this.aktionen.push(aktion)
			this.sendeZug()
		},
		behalten() { // DONE
			this.sendeZug()
		},
		schickProblem(event) { // DONE
			Stand.schickProblem(event.target.titel.value, event.target.beschreibung.value)
			this.zeigtProblem = false
		},
		brichAbProblem() { // DONE
			this.zeigtProblem = false
		},
		installier() { // DONE
			if (!serviceWorkerRegistration)
				navigator.serviceWorker.register("../serviceWorker.js")
		},
		deinstallier() { // DONE
			serviceWorkerRegistration.unregister()
			caches.delete(CACHE_NAME)
			this.zeigtInstallation = false
		},
		aktualisier() { // DONE
			caches.delete(CACHE_NAME)
			document.location.reload()
		},
		weiterSpielen() { // DONE
			this.stopptSpieler = false
			TogetherJS.require("peers").Self.update({ name: this.direktor || this.dran })
		},
		gibVerlaufstabelle: Stand.gibVerlaufstabelle, // DONE
		rollAutomatisch, // DONE
		stopAutoRollen // DONE
	}
})
.use(createPinia())
.directive("drag", DragDirective)
.component("AgMarker", AgMarker)
.component("Karte", Karte)
.component("Aktie", Aktie)
.component("Auswahl", Auswahl)
.mount("#plan")

// serviceWorker Version für lokale app DONE
if (plan.installierbar) {
	navigator.serviceWorker.getRegistration().then( reg => {
		if (reg) {
			serviceWorkerRegistration = reg
			fetch("../update.json").then( r => r.json() )
				.then( j => plan.version = j.version )
				.catch( e => console.debug(e) )
		}
	} )
	.catch( e => console.debug(e) )
}

Verteiler.beiSpielerWechsel = function (name) { // DONE
	console.log("beiSpielerWechsel", name)
	if (!plan.aktuellerSpieler) { // neu geladen, Name aus URL holen und setzen
		const urlName = document.location.hash.substring(1)
		if (urlName) {
			const self = TogetherJS.require("peers").Self
			if (self.name !== urlName) {
				self.update({ name: urlName })
				return
			}
		}
	}
	if (!plan.aktuellerSpieler || (name == -1) || plan.spieler[name]) {
		console.log("Spieler setzen", name)
		if (name == -1)
			plan.admin = true
		else {
			plan.admin = false
			plan.aktuellerSpieler = name
			plan.sitz.add(name)
		}
		if (plan.dran) {
			if (plan.admin)
				plan.aktuellerSpieler = stand.spielerNameFürAdmin
			wartetAufAktualisierung = true // Feld aktualisieren
		} else
			Stand.aktualisieren() // Spielstand einlesen
	}
}

// eigene Nachrichten bei TogetherJS einrichten
Verteiler.beiZug = function (zug) { // DONE
	if ((zug.wer === plan.dran) || (Object.keys(plan.spieler)[zug.wer] === plan.dran))
		plan.letzteZüge = Stand.zieh(zug.was)
	else
		console.warn("Zugspieler stimmt nicht", zug.wer, plan.dran)
	
	// erzeuge Notification
	if (plan.istAktuellerSpielerDran)
		triggerNotification( 'Du bist wieder dran!' )
}

/**
 * Reagiert auf Extrazug vom Verteiler
 * @param {Array<Aktion>} data Die Aktionen des Zuges
 */
Verteiler.beiExtraZug = function(data) { // DONE
	plan.extra = null
	plan.letzteZüge = Stand.ziehExtra(data.zug, data.wer, false, plan) || []
	
	// erzeuge Notification
	if (plan.istAktuellerSpielerDran || (Object.keys(plan.spieler)[Stand.gibDirektorNrFürBahn(plan.dran)] === plan.aktuellerSpieler))
		triggerNotification( 'Du bist wieder dran!' )
}

Verteiler.beiZugZurück = function (data) { // DONE
	prüfUndZeigRück.call(plan, data)
}

Verteiler.beiText = function (data) { // DONE
	plan.log.push(data)
	plan.letzteZüge = [data]
	
	// erzeuge Notification
	const spieler = Object.entries( plan.spieler ).find( e => e[1].token && e[1].token === data.token )
	triggerNotification( spieler[0] + ' schreibt:', data.text )
}

Verteiler.beiBestätigLokkauf = plan.beiBestätigLokkauf // DONE

Verteiler.beiLokkaufBestätigt = plan.beiLokkaufBestätigt // DONE

// window.onerror = function(msg, url, line, col, error) { DONE
	// // plan.fehler = `Fehler: ${msg}\nurl: ${url}\nline: ${line}${col ? "\ncolumn: " + col : ""}${error ? "\nerror: " + error : ""}`
	// alert(`Fehler: ${msg}\nurl: ${url}\nline: ${line}${col ? "\ncolumn: " + col : ""}${error ? "\nerror: " + error : ""}`)
	// return true
// }