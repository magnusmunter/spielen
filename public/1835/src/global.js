import { BAHNEN, FERNVERBINDUNGEN, gibStadtBei } from "../config.js"

const pfad = document.location.pathname
export const spielNr = (Number(pfad.substring(pfad.lastIndexOf("/") + 1)) || 1)
export const bilderPfad = "../1835/img/"
export const standPfad = "../stands/" + spielNr
export const chatPfad = "../streams/" + spielNr + '/posts'

export function checkNotificationsPermission() {
	// function to actually ask the permissions

	function handlePermission( permission ) {
		// log on the console
		console.log( "Permission for Notifications asked..." )
		if ((Notification.permission === 'denied') || (Notification.permission === 'default'))
			console.log( "No permission received." )
		else
			console.log( "Yes! Permission received." )
	}

	// Let's check if the browser supports notifications
	if ( "Notification" in window ) {
		try {
			Notification.requestPermission().then( permission => handlePermission( permission ) )
		} catch( e ) {
			Notification.requestPermission( permission => handlePermission( permission ) )
		}
	} else
		console.log("This browser does not support notifications.")
}

// Notification durch den Browser ans OS
export function triggerNotification( inTitle, inBody ) {
  if ( Notification.permission === 'granted' ) {
	const options = { icon: '/assets/logos/Spielen_Logo_q192.png' }
	if ( inBody )
	  options.body = inBody
	new Notification( inTitle, options )
  }
}

export function gibAktionenText(aktion, spieler) {
	let text
	switch (aktion.art) {
		case 1: return "beendet Startpaketverkauf"
		case 2: return "beginnt neue Betriebsrunde " + aktion.nr
		case 3: return "setzt Startpaketverkauf fort"
		case 4: return "beginnt neue Aktienrunde"
		case 5: return `nimmt Betrieb auf mit Startkapital ${aktion.geld}M`
		case 6: return "ist komplett verkauft und steigt"
		case 7: return "Kurs fällt"
		case 8: return "Kurs steigt"
		case 9: return "fällt durch Verkauf"
		case 10: return `verschrottet ${aktion.typ}${aktion.typ.length > 1 ? aktion.typ[0] : ""}er Loks`
		case 11: return "bekommt zwei einzelne gegen Doppelaktie"
		case 12: return `wandelt ${BAHNEN.v2} in ${BAHNEN.PR} um`
		case 13: return "schließt Privatbahnen"
		case 14: return `wandelt Vorpreußische Bahnen in ${BAHNEN.PR} um`
		case 15: return "erklärt Bank für gesprengt und leitet letzte Spielrunde ein"
		case 16: return "beendet das Spiel"
		case 17: return "ist Bankrott"
		default: if (aktion.spieler)
			return aktion.bank ? "übernimmt Tischspielstand" : "beginnt Startpaketverkauf"
		else if (aktion.papier) {
			const papier = aktion.papier
			const istBahn = papier.startsWith("p") || papier.startsWith("v")
			text = aktion.geld > 0 ? "verkauft" : aktion.von >= 0 ? "verstaatlicht" : "kauft"
			text += ` die ${istBahn ? "Gesellschaft" : "Aktie"} "${BAHNEN[papier.substr(0, 2)]}`
			if (!istBahn)
				text += ` ${papier.startsWith("PR") ? papier.endsWith("2") ? "10" : "5" : papier.endsWith("2") ? "20" : "10"}%`
			if (aktion.direktor || (papier == "BY2") || (papier == "SX2"))
				text += " Direktor"
			text += '"'
			if ((aktion.von >= 0) && (aktion.geld < 0))
				text += ` von ${spieler[aktion.von]}`
			if (aktion.geld)
				return text + ` für ${Math.abs(aktion.geld)}M`
		} else if (aktion.gleis) {
			const gleis = aktion.gleis
			const platz = String.fromCharCode(gleis.y + 65) + ((gleis.x + 1) * 2 - ((gleis.y % 2) ? 0 : 1))
			text = "baut Gleis " + gleis.i
			const stadt = gibStadtBei(gleis.x, gleis.y)
			text += stadt ? ` bei ${stadt} (${platz})` : " auf " + platz
			if (aktion.geld)
				text += `, Kosten: ${aktion.geld}M`
			if (gleis.extra)
				return text + ", Sonderrecht: " + BAHNEN[gleis.extra]
		} else if (aktion.bhf) {
			const bhf = aktion.bhf
			text = "baut Bahnhof in " + gibStadtBei(bhf.x, bhf.y)
			if (aktion.geld)
				text += `, Kosten: ${aktion.geld}M`
			if (bhf.extra)
				return text + ", Sonderrecht: " + BAHNEN[bhf.extra]
		} else if (aktion.fährt) {
			text = `fährt ein ${aktion.geld}M`
			if (aktion.einbehalt)
				return text + " wird einbehalten"
		} else if (aktion.rente)
			return `schüttet an ${BAHNEN[aktion.an] || spieler[aktion.an] || aktion.an} aus ${aktion.rente}M`
		else if (aktion.aktien)
			return `gibt ${BAHNEN[aktion.aktien]} Aktien aus`
		else if (aktion.loks)
			return `gibt ${aktion.loks}er Loks aus`
		else if (aktion.lok) {
			const lok = aktion.lok
			if (aktion.geld) {
				text = `kauft ${lok + (lok.endsWith("+") ? lok[0] : "")}er Lok `
				if (aktion.von)
					text += `von ${BAHNEN[aktion.von]} `
				return text + `für ${aktion.geld}M`
			}
			return `gibt überzählige ${lok + (lok.length > 1 ? lok : "")}er Lok ab`
		} else if (aktion.direktor >= 0)
			return `bekommt ${spieler[aktion.direktor]} als neuen Direktor`
		else if (aktion.ende)
			return (aktion.ende > 1) ? `geht in ${BAHNEN.PR} auf` : "schließt"
		else if (aktion.geld)
			return `schießt zu ${aktion.geld}M`
	}
	return text
}

function gibHaltText(x, y, von) {
	const stadt = gibStadtBei(x, y)
	if (stadt)
		return " " + stadt
	if (FERNVERBINDUNGEN[x + "," + y])
		return " " + FERNVERBINDUNGEN[x + "," + y]
	if (this.gibWertBei(x, y, von)) // this ist hier durch Aufruf Plan
		return " +"
	return ""
}

export function gibStreckenText(strecke) {
	const geld = strecke.pop()
	const lok = strecke.shift()
	let text = lok + (lok.endsWith("+") ? lok[0] : "") + "er Lok fährt"
	const verlauf = strecke[2].split("")
	let von
	let vonPos
	let pos = [strecke[0], strecke[1]]
	while (pos) {
		if ((pos[0] !== 6) || (pos[1] !== 2)
		    || (((von != 1) || (verlauf[0] != 2)) && ((von != 5) || (verlauf[0] != 4)))) // Schwerinumfahrung
			text += gibHaltText.call(this, pos[0], pos[1], vonPos) // this ist hier Plan, wird in plan gebunden
		vonPos = pos
		von = verlauf.shift()
		if (von === "+")
			von = verlauf.shift()
		pos = von >= 0 ? this.gibNachbarPos(pos[0], pos[1], von) : null // this ist hier Plan, wird in plan gebunden
	}
	return `${text} ${geld}M`
}

export function gibAktienWert(ag) {
	let wert
	const i = ag.kurs[0]
	const j = ag.kurs[1]
	if (j < 4) {
		wert = 54
		let o = 12
		for (let x = 0; x < j; ++x) {
			o -= 2
			wert += o
		}
		for (let x = 0; x < i; ++x) {
			wert += o
			o += (x < 4 - j) ? -2 : 2
		}
	} else {
		wert = 82
		let o = 4
		for (let x = 4; x < j; ++x) {
			o += 2
			wert += o - 4
		}
		for (let x = 0; x < i; ++x) {
			wert += o
			o += 2
		}
	}
	return wert
}

export function gibAktienpackete(papiere, ags) {
	let letzte
	return papiere.reduce( (r, p) => {
		const ag = p.substr(0, 2)
		if (ags[ag]) {
			if (!r[ag])
				r[ag] = 0
			const teil = p.length > 2 ? 2 : 1
			r[ag] += (ag === "PR") ? teil / 2 : teil
		}
		return r
	} , {})
}

/**
 * @returns Den Wert des Papiers
 * @param {String} art Das Kürzel des Papiers
 */
export function gibPapierWert(art) {
	let wert
	switch (art.slice(0, 2)) {
		case "p1": return 100
		case "p2": return 190
		case "p3": return 130
		case "p4": return 160
		case "p5": return 120
		case "p6": return 150
		case "v2": return 170
		case "v4": return 160
		case "PR": wert = 154; break
		case "BY": wert = 92; break
		case "SX": wert = 88; break
		case "BA":
		case "WT":
		case "HE": wert = 84; break
		default: wert = 80
	}
	if (art.startsWith("PR"))
		wert /= 2
	if (art.length > 2)
		wert *= 2
	return wert
}

export function gibPapierlimit(spieler, data) {
	let limit
	switch (Object.keys(data.spieler).length) {
		case 3: limit = 19; break
		case 4: limit = 15; break
		case 5: limit = 12; break
		case 6: limit = 11; break
		case 7: limit = 9
	}
	Object.entries(gibAktienpackete(data.spieler[spieler].papiere, data.ags)).forEach( ag => (ag[1] >= 8) && ++limit )
	return limit
}

/**
 * Rollt ein HTML Element automatisch, wenn der Zeiger am Rand ist
 * @param {HTMLElemnt} element Das zu rollende Element
 * @param {MouseEvent} event Das event
 * @param {boolean} horizontal Ob horizontal gerollt werden soll
 * @param {boolean} vertikal Ob vertikal gerollt werden soll
 */
export function rollAutomatisch(element, event, horizontal, vertikal) {
	const boundingRect = element.getBoundingClientRect()
	let rollt
	let rollX = 0
	let rollY = 0
	if (horizontal) {
		const links = boundingRect.x + 100
		const rechts = boundingRect.right - 100
		if (event.clientX < links) {
			if (element.rollId)
				clearInterval(element.rollId)
			rollt = true
			rollX = event.clientX - links
		} else if (event.clientX > rechts) {
			if (element.rollId)
				clearInterval(element.rollId)
			rollt = true
			rollX = event.clientX - rechts
		}
	}
	if (vertikal) {
		const oben = boundingRect.y + 100
		const unten = boundingRect.bottom - 100
		if (event.clientY < oben) {
			if (element.rollId)
				clearInterval(element.rollId)
			rollt = true
			rollY = event.clientY - oben
		} else if (event.clientY > unten) {
			if (element.rollId)
				clearInterval(element.rollId)
			rollt = true
			rollY = event.clientY - unten
		}
	}
	if (rollt)
		element.rollId = setInterval(() => element.scrollBy(rollX, rollY), 20)
	else if (element.rollId) {
		clearInterval(element.rollId)
		delete element.rollId
	}
}

/**
 * Beendet automatisches Rollen für ein HTML Element
 * @param {HTMLElement} element Das rollende Element
 * @see rollAutomatisch
 */
export function stopAutoRollen(element) {
	if (element.rollId) {
		clearInterval(element.rollId)
		delete element.rollId
	}
}