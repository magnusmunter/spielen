module AccountHelper
  
  def h_account_editable?( in_account )
    !current_account.blank? and
        in_account == current_account
  end
  
end
