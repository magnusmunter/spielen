# Preview all emails at http://localhost:3000/rails/mailers/orders_mailer
class OrdersMailerPreview < ActionMailer::Preview
  
  # Preview this email at http://localhost:3000/rails/mailers/orders_mailer/confirmation_mail
  def confirmation_mail
    OrdersMailer.with( order: Order.last ).confirmation_mail
  end
  
  # Preview this email at http://localhost:3000/rails/mailers/orders_mailer/confirmation_mail
  def new_order_notification_mail
    OrdersMailer.with( order: Order.last ).new_order_notification_mail
  end
  
end
