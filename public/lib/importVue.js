let id = -1

function gibId() {
	return (++id).toString(26).split("")
	             .map(d => String.fromCharCode(isNaN(d) ? d.charCodeAt(0) + 10 : parseInt(d) + 97)).join("")
}

const AsyncFunction = (async function () {}).constructor
const suchAttribut = /:|@|(?:v-)|(?:ref)/
const suchAttributOhne = /(?:\W):([\wäöüßÄÖÜ\-:]+)(=?)/g
const suchSelbstschliessTags = /<(\/)?([A-Z][\w-]*)([^\/>]*)(\/)?/g
const suchAttribute = /\w+[A-ZÄÖÜ]\w*(?==")/g
const suchDefineProps = /(const props = )?defineProps\(([^\)]+)\)/
const suchDefineEmits = /(const emit = )?defineEmits\(([^\)]+)\)/
const suchDefineExpose = /defineExpose\(([^\)]+)\)/
const suchBezeichner = /([\wäöüßÄÖÜ]+)/g
const suchVueImport = /import (\w+) from ["']([\w\./]+\.vue)["'];?/g
const suchImport = /import (\{ ([\w, \t\n]+) \} from )?["']([^"']+)["'];?\r?\n/g
const suchParams = /\n\t(const|function) \{? ?([\wäöüßÄÖÜ, ]+)? ?\}?/g
const suchParamsImText = /\{\{([^\}]+)\}\}/g
const suchStil = /\n\t([^\n\t\{]*)([,\{])/g
const suchTag = /(<[\w-]+)/g
const suchExport = /export default \{/

function hatElementParam(element, regExp, param) {
	const attributes = element.attributes
	for (let i = 0, e = attributes.length; i < e; ++i)
		if ((attributes[i].name.search(suchAttribut) === 0) && regExp.test(attributes[i].value))
			return true
	if (element.firstElementChild && hatElementParam(element.firstElementChild, regExp, param))
		return true
	if (element.nextElementSibling && hatElementParam(element.nextElementSibling, regExp, param))
		return true
}

function gibKleinMitStrich(text) {
	return text.replace(/([a-z]?)([A-Z])/g, (a, b, c) => (b ? b + "-" : "") + c.toLowerCase() )
}

function parseVue(text) {
	const div = document.createElement("div")
	div.innerHTML = text
	const skripte = div.querySelectorAll("script")
	const style = div.querySelector("style")
	let template = text.slice(text.indexOf("<template>") + 10, text.lastIndexOf("</template>")).trim()
	                   .replace(suchAttributOhne, (t, a, b) => b ? t : `\t:${a}="${a}"` )
	                   .replace(suchSelbstschliessTags, (a, b, c, d, e) => {
	                   	c = gibKleinMitStrich(c)
	                   	d = d.replace(suchAttribute, t => t === "viewBox" ? t : gibKleinMitStrich(t) )
	                   	return e ? `<${c + d.trimRight()}></${c}` : `<${b || ""}${c + d}`
	                   } )
	div.innerHTML = div.querySelector("template").innerHTML // template hat keine Knoten
	const componentTemplates = div.querySelectorAll("script[type='text/x-template']")
	for (let i = 0, count = componentTemplates ? componentTemplates.length : 0; i < count; ++i) {
		document.head.appendChild(componentTemplates[i])
	}
	let skript = ""
	for (let s of Array.from(skripte)) {
		if (s.hasAttribute("setup")) { // <script setup> in options API umwandeln
			const skriptText = s.innerText
			let i = skriptText.lastIndexOf("import")
			i = skriptText.indexOf("\n", i) + 1
			const setup = skriptText.substr(i).replace(suchDefineProps, "").replace(suchDefineEmits, "").replace(suchDefineExpose, "").trim()
			skript = skriptText.substr(0, i) + skript.replace(suchImport, "\n")
			skript += "\n\texport default {\n"
			let props = []
			if (suchDefineProps.test(skriptText.substr(i))) {
				props = JSON.parse(RegExp.$2)
				skript += `\t\tprops: ${RegExp.$2},\n`
			}
			if (suchDefineEmits.test(skriptText.substr(i)))
				skript += `\t\temits: ${RegExp.$2},\n`
			const params = []
			if (suchDefineExpose.test(skriptText.substr(i))) {
				const exposeText = RegExp.$1
				const expose = []
				while (suchBezeichner.exec(exposeText)) {
					expose.push(RegExp.$1)
					if (!props.includes(RegExp.$1))
						params.push(RegExp.$1)
				}
				skript += `\t\texpose: ["${expose.join('", "')}"],\n`
			} else
				skript += "\t\texpose: [],\n"
			
			// Komponenten zugänglich machen
			const components = []
			while (suchVueImport.exec(skript))
				components.push(RegExp.$1)
			if (components.length)
				skript += "\t\tcomponents: {\n"
			             + `\t\t\t${components.join(",\n\t\t\t")}\n`
			             + "\t\t},\n"
			
			// oberste Parameter zugänglich machen
			div.innerHTML = template.replace(/template/g, "p") // template hat keine Knoten
			while (suchImport.exec(skriptText) || suchParams.exec(skriptText)) {
				const alleParams = RegExp.$2.split(",")
				for (let i = 0, e = alleParams.length; i < e; ++i) {
					const param = alleParams[i].trim()
					if (!params.includes(param)) {
						const regExp = RegExp().compile("(^|\\b)" + param + "($|\\b)")
						if (hatElementParam(div.firstElementChild, regExp, param))
							params.push(param)
						else {
							const text = div.innerText || div.textContent
							// console.log("such", param, text)
							while (suchParamsImText.exec(text)) {
								const teil = RegExp.$1
								// console.log(teil)
								if (regExp.test(teil)) {
									params.push(param)
									suchParamsImText.lastIndex = 0
									break
								}
							}
						}
					}
				}
			}
			if (params.length)
				skript += "\t\tsetup(props, { attrs, slots, emit, expose }) {\n"
							 + "\t\t\t" + setup.replace(/\t([^\t])/g, "\t\t\t$1") + "\n\n"
				          + `\t\t\treturn { ${params.join(", ")} }\n`
				          + "\t\t}\n"
			
			skript += "\t}"
		} else
			skript = s.innerText
	}
	if (style) {
		if (style.hasAttribute("scoped")) {
			style.removeAttribute("scoped")
			const attr = "data-v-" + gibId()
			style.innerText = style.innerText.replace(suchStil, (t, zeile, ende) => "\n\t" + zeile.trimRight() + `[${attr}]` + ende )
			template = template.replace(suchTag, `$1 ${attr}`)
		}
		document.head.appendChild(style)
	}
	return [skript, template]
}
if (window.lädModule) // für lad_module "exportieren"
	window.parseVue = parseVue

let basisUrl
let vueUrl
export function importVue(url, basis) {
	if (!basisUrl) {
		basisUrl = basis
		vueUrl = new URL("../../lib/vendor/vue.esm-browser.js", basis)
	}
	if (!basis)
		basis = basisUrl
	url = new URL(url, basis)
	return new Promise( resolve => fetch(url).then( r => r.text() ).then( text => {
		const importe = [] // tausch Importe
		const vue = parseVue(text)
		if (suchVueImport.test(vue[0])) {
			let importText = 'import { importVue } from "./'
			const index = import.meta.url.split("").findIndex( (c, i) => c != url.href[i] )
			for (let i = url.href.substring(index).split("/").length - 1; i > 0; --i)
				importText += "../"
			vue[0] = importText + import.meta.url.substring(index) + '"\n' + vue[0]
		}
		const skript = vue[0].trim().replace(suchVueImport, `const $1 = await importVue("$2", "${url}")`)
		               .replace(suchImport, (t, nix, i, f) => {
			importe.push(import(f === "vue" ? vueUrl : new URL(f, url)))
			return i.split(",").reduce( (r, n) => {
				n = n.trimLeft()
				return r + (r ? "\t" : "") + `const ${n} = arguments[${importe.length - 1}].${n} // from ${f}\n`
			} , "")
		} ).replace(suchExport, `return {\n\t\ttemplate: arguments[${importe.length}],`)
		Promise.all(importe)
			.then( i => resolve(AsyncFunction("\t" + skript + "\n\n\t//# sourceURL=" + url)(...i.concat(vue[1]))) )
			.catch( e => {
				e.message += " in " + url.pathname
				throw e
			} )
	} ) )
}