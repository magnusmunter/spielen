class CreateNewsletters < ActiveRecord::Migration[5.2]
  def change
    create_table :newsletters do |t|
      t.string :token
      t.string :status
      t.text :content
      t.datetime :send_at

      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
