import { createApp } from "./../../lib/vendor/vue.esm-browser.js"

createApp({
	data() { return { stand: null } },
	methods: {
		aendern (event) {
			event.preventDefault()
			try {
				const form = this.$el
				const text = this.$el.text.value
				form.text.remove()
				if (!form.zuege.value)
					form.zuege.remove()
				const stand = JSON.parse(text)
				delete stand.zuege
				form.stand.value = JSON.stringify(stand)
				form.submit()
			} catch(e) {
				alert(e)
			}
		}
	}
}).mount("div")