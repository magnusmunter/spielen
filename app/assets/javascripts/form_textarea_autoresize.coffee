# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# ----------------------------------------------

($ document).on 'turbolinks:load', ->
  console.log '> script @ form_textarea_resize.coffee'
  
  form_textarea_resize = ( el = null ) ->
    if el? and el.tagName == 'TEXTAREA'
      $ich = ($ el)
    else
      $ich = ($ this)
    
    console.log "-- form_textarea_resize - el:#{el} this:#{this} prop 'id':#{$ich.prop 'id'} @ form_textarea_resize.coffee"
    $ich.height(50)
    new_height = $ich.get(0).scrollHeight
    console.log "-- new_height:#{new_height}"
    $ich.height(new_height)
  
  
  # --- Initialisierung ------------------------
  
  $main = ($ 'main')
  unless $main.data 'init_form_textarea_resize'
    console.log '-- init'
    $main.on 'input.form_textarea_resize',
        'textarea',
        form_textarea_resize
    $main.data 'init_form_textarea_resize', true
  

