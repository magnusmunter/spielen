require 'test_helper'

class SpielerMailerTest < ActionMailer::TestCase
  test "dubistdran" do
    mail = SpielerMailer.dubistdran
    assert_equal "Dubistdran", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
