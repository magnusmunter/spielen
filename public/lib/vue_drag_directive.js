const body = document.body

let noPointer = true
let downType = "mousedown"
let moveType = "mousemove"
let upType = "mouseup"
try {
	noPointer = !PointerEvent
	downType = "pointerdown"
	moveType = "pointermove"
	upType = "pointerup"
} catch (e) {}
let draggables = []
let droppables = []
let dirty

function transformTouchToMouseEvent(event) {
	event.preventDefault()
	if ((event.touches.length > 1) || ((event.type == "touchend") && (event.touches.length > 0)))
		return

	const newEvent = document.createEvent("MouseEvents")
	const touch = event.changedTouches[0]
	let target = event.target
	let type

	switch (event.type) {
		case "touchstart": type = "mousedown"; break
		case "touchmove":
			target = document.elementFromPoint(touch.clientX, touch.clientY)
			type = "mousemove"
			break
		case "touchend": type = "mouseup"
	}

	newEvent.initMouseEvent(type, true, true, window, 0, touch.screenX, touch.screenY, touch.clientX, touch.clientY,
	                        event.ctrlKey, event.altKey, event.shiftKey, event.metaKey, 0, null)
	newEvent.originalType = event.type
	target.dispatchEvent(newEvent)
}

export const DragDirective = {
	mounted(el, binding) {
		if (dirty) {
			console.log("DragDirective mounted", el, binding)
			draggables = draggables.filter( d => {
				if (body.contains(d[0])) {
					d[1].targets = d[1].targets.filter( t => body.contains(t[0]) )
					return true
				}
			} )
			droppables = droppables.filter( d => body.contains(d[0]) )
			dirty = false
		}
		const data = binding.value.data
		if (data) {
			draggables.push([el, binding])
			binding.targets = []
			for (let d of droppables) {
				const accepts = d[1].value.accepts
				if (accepts && accepts(data))
					binding.targets.push(d)
			}
		}
		const accepts = binding.value.accepts
		if (accepts) {
			droppables.push([el, binding])
			for (let d of draggables)
				if (accepts(d[1].value.data))
					d[1].targets.push([el, binding])
		}
		
		let startTarget
		let startX
		let startY
		let startA
		let lastA
		let turns = 0
		let lastTarget
		let overTarget

		function deleteOverTarget() {
			const leave = overTarget[1].value.leave
			if (leave)
				leave(data)
			overTarget = null
		}
		
		function onMove(event) {
			if (binding.value.drag) {
				const pointer = event.changedTouches ? event.changedTouches[0] : event
				const x = pointer.pageX - startX
				const y = pointer.pageY - startY
				let a = x ? Math.atan(y / x) : (y > 0) ? Math.PI / 2 : 1.5 * Math.PI
				if (x < 0)
					a += Math.PI
				else if ((x > 0) && (y < 0))
					a += 2 * Math.PI
				if (isNaN(startA)) {
					if (Math.sqrt(x * x + y * y) > 50) {
						startA = lastA = a
						turns = 0
					}
				} else {
					a -= startA
					if (a <= 0)
						a += 2 * Math.PI
					if (a > Math.PI)
						a -= 2 * Math.PI
					const sign = Math.sign(lastA)
					if ((Math.sign(a) !== sign) && (Math.abs(a - lastA) > Math.PI))
						turns += sign
					lastA = a
					event.angle = a + 2 * Math.PI * turns
				}
				binding.value.drag(Object.assign(event, { startX, startY, dragX: x, dragY: y }))
			}
			if (binding.targets) {
				const target = event.pointerType === "touch" ? document.elementFromPoint(event.clientX, event.clientY) : event.target
				if (target !== lastTarget) {
					lastTarget = target
					if (overTarget && !overTarget[0].contains(target))
						deleteOverTarget()
					let dropTarget
					for (let t of binding.targets)
						if (t[0].contains(target) && (!dropTarget || dropTarget[0].contains(t[0])))
							dropTarget = t
					if (dropTarget && (!overTarget || (dropTarget[0] !== overTarget[0]))) {
						const enter = dropTarget[1].value.enter
						if (enter) {
							overTarget = dropTarget
							enter(data)
						}
					}
				}
				for (let dragTarget of binding.targets)
					if (dragTarget[0].contains(target)) {
						const over = dragTarget[1].value.over
						if (over)
							over(data, dragTarget[0], event)
					}
			}
		}

		function onUp(event) {
			window.removeEventListener(moveType, onMove)
			window.removeEventListener(upType, onUp)
			if (noPointer) {
				window.removeEventListener("touchmove", transformTouchToMouseEvent, { passive: false })
				window.removeEventListener("touchend", transformTouchToMouseEvent)
			}
			startA = undefined
			turns = 0
			if (binding.value.end) {
				event.startTarget = startTarget
				binding.value.end(event)
			}
			if (binding.targets)
				for (let target of binding.targets) {
					const end = target[1].value.end
					if (end)
						end(data, target[0], event)
					if ((target === overTarget)
					    || target[0].contains(event.pointerType === "touch" ? document.elementFromPoint(event.clientX, event.clientY) : event.target)) {
						if (overTarget)
							deleteOverTarget()
						const drop = target[1].value.drop
						if (drop)
							drop(data)
					}
				}
			if (((event.pointerType === "touch") && (document.elementFromPoint(event.clientX, event.clientY) === startTarget))
			    || ((event.originalType === "touchend") && (event.target === startTarget))) {
				const newEvent = document.createEvent("MouseEvents")
				newEvent.initMouseEvent("click", true, true, window, 0, event.screenX, event.screenY, event.clientX, event.clientY,
				                        event.ctrlKey, event.altKey, event.shiftKey, event.metaKey, 0, null)
				startTarget.dispatchEvent(newEvent)
			}
		}

		if (binding.value.drag) {
			el.addEventListener(downType, event => {
				event.preventDefault()
				startTarget = event.target
				startX = event.pageX
				startY = event.pageY
				window.addEventListener(moveType, onMove)
				window.addEventListener(upType, onUp)
				if (noPointer) {
					window.addEventListener("touchmove", transformTouchToMouseEvent, { passive: false })
					window.addEventListener("touchend", transformTouchToMouseEvent)
				}
				if (binding.value.start)
					binding.value.start(event)
				if (binding.targets)
					for (let target of binding.targets) {
						const hint = target[1].value.hint
						if (hint)
							hint(binding.value.data)
					}
			})
			if (noPointer)
				el.addEventListener("touchstart", transformTouchToMouseEvent)
			else
				el.addEventListener("touchstart", e => e.preventDefault() )
		}
	},
	unmounted(el, binding) {
		dirty = true
	}
}