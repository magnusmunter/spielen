class CreateStreams < ActiveRecord::Migration[5.2]
  def change
    create_table :streams do |t|
      t.string :token
      t.string :status
      t.string :title
      t.references :owner, foreign_key: { to_table: :accounts }
      t.boolean :public
      t.integer :max_participants
      t.boolean :only_invites
      t.string :status_required
      t.integer :fade_away_days

      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
