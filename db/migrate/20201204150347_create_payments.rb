class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.string :status
      t.references :membership, foreign_key: true
      t.decimal :amount, precision: 10, scale: 2
      t.datetime :paid_at

      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
