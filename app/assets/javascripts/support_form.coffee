# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# ----------------------------------------------

($ document).on 'turbolinks:load', ->
  console.log '> script @ support_form.coffee'
  
  toggle_submit = ->
    $ich = ($ this)
    console.log "-- toggle_submit - #{$ich.tagName}.#{$ich.attr 'class'}##{$ich.attr 'id'} @ support_form.coffee"
    
    $actions = $ich.closest( 'form' ).find( '.actions' )
    if $ich.val().length isnt 0
      $actions.removeClass 'initially_hidden'
    else
      $actions.addClass 'initially_hidden'
    
  
  
  # --- Initialisierung ------------------------
  
  $main = ($ 'main')
  unless $main.data 'init_support_form'
    console.log '-- init'
    $main.on 'input.forms',
        '#support_form #amount',
        toggle_submit
    $main.data 'init_support_form', true
  

