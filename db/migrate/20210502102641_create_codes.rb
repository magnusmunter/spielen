class CreateCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :codes do |t|
      t.string :input
      t.string :url

      t.timestamps
    end
  end
end
