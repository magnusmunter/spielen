require 'test_helper'

class OrdersMailerTest < ActionMailer::TestCase
  test "confirmation_mail" do
    mail = OrdersMailer.confirmation_mail
    assert_equal "Confirmation mail", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
