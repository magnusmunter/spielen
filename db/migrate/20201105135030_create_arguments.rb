class CreateArguments < ActiveRecord::Migration[5.2]
  def change
    create_table :arguments do |t|
      t.string :token
      t.string :status
      t.references :account, foreign_key: true
      t.references :proposal, foreign_key: true
      t.string :title
      t.text :description

      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
