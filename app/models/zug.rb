class Zug < ApplicationRecord
  belongs_to :stand, foreign_key: 'spiel_id'
  
  # --- SCOPES ---------------------------------
  
  scope :ab, -> ( nr ) {
        where "nr > ?", ( nr || 0 ) }
  
end
