class Order < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  # --- RELATIONS ------------------------------
  
  belongs_to :contact, optional: true
  has_many :orderquants, dependent: :destroy
  
  accepts_nested_attributes_for :contact, :orderquants
  
  # --- VALIDATION -----------------------------
  
  validates :delivery_street, :delivery_housenr, :delivery_plz, :delivery_city, presence: true, if: Proc.new { |o| o.step.to_i == 3 }
  validates :invoice_street, :invoice_housenr, :invoice_plz, :invoice_city, presence: true, if: Proc.new { |o| o.step.to_i == 4 and o.invoice_differs? }
  validates :read_privacy, :read_terms, acceptance: true, if: Proc.new { |o| o.step.to_i == 5 }
  
  validate :validate_everything
  
  def validate_everything
    logger.debug "> validate_everything @ order -- step: #{step}"
    case step.to_i
      when 1
        logger.debug "-- product_count: #{product_count}"
        errors.add( :orderquant, 'Sie müssen mindestens einen Artikel auswählen' ) if product_count < 1
        
      when 3
        errors.add( :delivery_org, 'muss ausgefüllt werden' ) if customer_kind != 'private' and delivery_org.blank? and delivery_name.blank?
      when 4
        errors.add( :invoice_org, 'muss ausgefüllt werden' ) if invoice_differs? and invoice_org.blank? and invoice_name.blank?
        
    end
    
    logger.debug "-- errors: #{errors.size}"
  end
  
  # --- INSTANCE METHODS -----------------------
  
  def product_count
    orderquants.inject( 0 ) { |sum,o| sum + o.quantity.to_i }
  end
  
  def product_sum
    orderquants.inject( 0 ) { |sum,o| sum + ( o.quantity.to_i * o.reward.price ) }
  end
  
  def categories
    category_names = orderquants.collect { |o| ( o.reward.category.blank? ? '' : o.reward.category ) }.uniq
    result = category_names.collect do |c|
      { name: c,
          orderquants: orderquants.select { |o|
              o.reward.category == c or
                  ( o.reward.category.blank? and c.blank? ) } }
    end
    return result
  end
  
  def secret
    id_hash [ token, created_at.to_s ].join
  end
  
  # --- Text Generation
  
  def invoice_org_text
    return invoice_org unless invoice_address_blank?
    delivery_org
  end
  def invoice_name_text
    return invoice_name unless invoice_address_blank?
    delivery_name
  end
  def delivery_destination_text
    return delivery_org unless delivery_org.blank?
    return delivery_name unless delivery_name.blank?
    return contact.name_text if contact
    'unbekannt'
  end
  def invoice_street_housenr_text
    return [ invoice_street, invoice_housenr ].join( ' ' ) unless invoice_address_blank?
    [ delivery_street, delivery_housenr ].join( ' ' )
  end
  def invoice_plz_city_text
    return [ invoice_plz, invoice_city ].join( ' ' ) unless invoice_address_blank?
    [ delivery_plz, delivery_city ].join( ' ' )
  end
  def invoice_country_text
    return invoice_country unless invoice_address_blank?
    delivery_country
  end
  
  
  # --- INSTANCE PREDICATES --------------------
  
  def unconfirmed?
    %w( preliminary new ).include? status
  end
  
  def invoice_address_blank?
    invoice_org.blank? and
        invoice_name.blank? and
        invoice_street.blank? and
        invoice_housenr.blank? and
        invoice_plz.blank? and
        invoice_city.blank? and
        invoice_country.blank?
  end
  
  def first_order_of_day?
    prev_order = Order.
        where( [ 'created_at < ?', created_at ] ).
        order( created_at: :desc ).
        first
    return ( prev_order and
        prev_order.created_at.at_midnight < created_at.at_midnight )
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.t_select( in_key )
    const_get( in_key.upcase ).collect { |o| [ self.human_attribute_name( "#{in_key}.#{o}" ), o ] }
  end
  
  # --- CONSTANTS ------------------------------
  
  STATUS = %w( preliminary new open done )
  
  CUSTOMER_KIND = %w( private organisation )
  
  CUSTOMER_LOCATION = %w( germany world )
  
end
