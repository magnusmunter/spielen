json.extract! article, :id, :token, :status, :title, :published_at, :description, :created_at, :updated_at
json.url article_url(article, format: :json)
