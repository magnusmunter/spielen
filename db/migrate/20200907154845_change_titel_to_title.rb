class ChangeTitelToTitle < ActiveRecord::Migration[5.2]
  def change
    rename_column :events, :titel, :title
    rename_column :articles, :titel, :title
  end
end
