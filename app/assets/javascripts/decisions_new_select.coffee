# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# ----------------------------------------------

($ document).on 'turbolinks:load', ->
  console.log '> script @ decisions_new_select.coffee'
  
  select_switch = ->
    $ich = ($ this)
    console.log "-- select_switch - ##{$ich.attr 'id'} @ decisions_new_select.coffee"
    sel = $ich.data 'sel'
    console.log "-- sel:#{sel}, val:#{$ich.val()}"
    if $ich.val() is 'selection'
      ($ sel).fadeIn 200
    else
      ($ sel).fadeOut 200
    
    return false # hat_ziel
  
  
  # --- Initialisierung ------------------------
  
  $select = ($ 'select#decision_variant')
  unless $select.data 'init_decision_variant'
    console.log '-- init init_decision_variant'
    $select.on 'input.decision_variant',
        '',
        select_switch
    $select.data 'init_decision_variant', true
  

