class Post < ApplicationRecord
  belongs_to :contact
  belongs_to :stream
  has_one :contentpart, as: :container, dependent: :destroy
  
  
  def text_content
    contentpart.blank? ? 'kein Text' : contentpart.content
  end
  
end
